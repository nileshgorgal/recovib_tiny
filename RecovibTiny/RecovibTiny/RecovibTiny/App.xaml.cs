﻿using Xamarin.Essentials;
using Xamarin.Forms;

namespace RecovibTiny
{
	public partial class App : Application
	{
		public App()
		{
			InitializeComponent();

			//MainPage = new Pages.MainPage(){ BindingContext = new ViewModels.MainViewModel() };
			MainPage = new Pages.MainPage();

		}
		protected override void OnStart()
		{
			CheckAndRequestPermissionAsync();
		}

		protected override void OnSleep()
		{
		}

		protected override void OnResume()
		{
			CheckAndRequestPermissionAsync();
		}

		private async void CheckAndRequestPermissionAsync()
		{
			var status = await Permissions.CheckStatusAsync<Permissions.StorageRead>();
			if (status != PermissionStatus.Granted)
			{
				await Permissions.RequestAsync<Permissions.StorageRead>();
			}

			status = await Permissions.CheckStatusAsync<Permissions.StorageWrite>();
			if (status != PermissionStatus.Granted)
			{
				await Permissions.RequestAsync<Permissions.StorageWrite>();
			}

		}
	}
}
