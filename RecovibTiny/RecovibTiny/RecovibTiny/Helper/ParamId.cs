﻿namespace RecovibTiny.Helper
{
	public class ParamId
	{
		public static readonly byte
			   SN_ID = 0x01,

			   LAST_CHECK_ID = 0x02,

			   AX_GAIN_2G_ID = 0x03,
			   AY_GAIN_2G_ID = 0x04,
			   AZ_GAIN_2G_ID = 0x05,

			   AX_OFFSET_2G_ID = 0x06,
			   AY_OFFSET_2G_ID = 0x07,
			   AZ_OFFSET_2G_ID = 0x08,

			   AX_GAIN_6G_ID = 0x09,
			   AY_GAIN_6G_ID = 0x0A,
			   AZ_GAIN_6G_ID = 0x0B,

			   AX_OFFSET_6G_ID = 0x0C,
			   AY_OFFSET_6G_ID = 0x0D,
			   AZ_OFFSET_6G_ID = 0x0E,

			   CALIB_STATE_ID = 0x0F,
			   CALIB_DATE_ID = 0x10,
			   USE_DATE_ID = 0x11,
			   REMINDER_DATE_ID = 0x12,

			   FS_ID = 0x13,
			   DT_ARM_ID = 0x14,
			   DT_START_ID = 0x15,
			   DT_STOP_ID = 0x16,
			   SENSITIVITY_ID = 0x17,

			   A_COFF_ID = 0x18,
			   B_COFF_ID = 0x19,

			   UPDATE_REQ_ID = 0x1A,
			   TIME_TUNING_PERIOD_ID = 0x1B,
			   START_TICK_ID = 0x1C,
			   STOP_TICK_ID = 0x1D;


	}
}