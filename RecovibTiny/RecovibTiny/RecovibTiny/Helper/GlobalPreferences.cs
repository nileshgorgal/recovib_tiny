﻿using Xamarin.Essentials;

namespace RecovibTiny.Helper
{
	public class GlobalPreferences
	{
		private const string ANDROID11_PATHKEY = "android11_pathkey";

		public static string Android11_Path
		{
			get => Preferences.Get(ANDROID11_PATHKEY, string.Empty);
			set => Preferences.Set(ANDROID11_PATHKEY, value);
		}


		//Input Possibilities
		public enum INPUT_CHOICE
		{
			REAL_VALUES, RECORDED_VALUES
		}
		private const string INPUT_CHOICE_KEY = "input_choice";
		public static INPUT_CHOICE _InputChoice
		{
			get => (INPUT_CHOICE)Preferences.Get(INPUT_CHOICE_KEY, 0);
			set => Preferences.Set(INPUT_CHOICE_KEY, (int)value);
		}

		private static readonly string SERIAL_NUMBER_KEY = "_sn";
		public static string Serial_Number
		{
			get => Preferences.Get(SERIAL_NUMBER_KEY, "false");
			set => Preferences.Set(SERIAL_NUMBER_KEY, value);
		}

		private static readonly string SIGNAL_PROC_HIDE_KEY = "signal_proc_hide";

		public static bool Signal_Proc_Hide
		{
			get => Preferences.Get(SIGNAL_PROC_HIDE_KEY, false);
			set => Preferences.Set(SIGNAL_PROC_HIDE_KEY, value);
		}
		private static readonly string FFT_ANA_HIDE_KEY = "fft_ana_hide";

		public static bool Fft_Ana_Hide
		{
			get => Preferences.Get(FFT_ANA_HIDE_KEY, false);
			set => Preferences.Set(FFT_ANA_HIDE_KEY, value);
		}

		private static readonly string g_Key_6 = "6g";
		public static bool g_6
		{
			get => Preferences.Get(g_Key_6, false);
			set => Preferences.Set(g_Key_6, value);
		}
		private static readonly string g_key_2 = "2g";
		public static bool g_2
		{
			get => Preferences.Get(g_key_2, false);
			set => Preferences.Set(g_key_2, value);
		}
		private static readonly string gunits_key = "gunits";
		public static bool gunits
		{
			get => Preferences.Get(gunits_key, false);
			set => Preferences.Set(gunits_key, value);
		}
		private static readonly string ms2units_key = "ms2units";
		public static bool ms2units
		{
			get => Preferences.Get(ms2units_key, false);
			set => Preferences.Set(ms2units_key, value);
		}
		private static readonly string Ax_cb_key = "Ax_cb";
		public static bool Ax_cb
		{
			get => Preferences.Get(Ax_cb_key, false);
			set => Preferences.Set(Ax_cb_key, value);
		}
		private static readonly string Ay_cb_key = "Ay_cb";
		public static bool Ay_cb
		{
			get => Preferences.Get(Ay_cb_key, false);
			set => Preferences.Set(Ay_cb_key, value);
		}
		private static readonly string Az_cb_key = "Az_cb";
		public static bool Az_cb
		{
			get => Preferences.Get(Az_cb_key, false);
			set => Preferences.Set(Az_cb_key, value);
		}
		private static readonly string acc_radio_key = "acc_radio";
		public static bool acc_radio
		{
			get => Preferences.Get(acc_radio_key, false);
			set => Preferences.Set(acc_radio_key, value);
		}
		private static readonly string hpfilterCb_key = "hpfilterCb";
		public static bool hpfilterCb
		{
			get => Preferences.Get(hpfilterCb_key, false);
			set => Preferences.Set(hpfilterCb_key, value);
		}
		private static readonly string fc_hp_key = "fc_hp";
		public static double fc_hp
		{
			get => Preferences.Get(fc_hp_key, 0.0);
			set => Preferences.Set(fc_hp_key, value);
		}

		private static readonly string lpfilterCb_key = "lpfilterCb";
		public static bool lpfilterCb
		{
			get => Preferences.Get(lpfilterCb_key, false);
			set => Preferences.Set(lpfilterCb_key, value);
		}
		private static readonly string fc_lp_key = "fc_lp";
		public static double fc_lp
		{
			get => Preferences.Get(fc_lp_key, 0.0);

			set => Preferences.Set(fc_lp_key, value);
		}

		private static readonly string vel_radio_key = "vel_radio";
		public static bool vel_radio
		{
			get => Preferences.Get(vel_radio_key, false);

			set => Preferences.Set(vel_radio_key, value);
		}
		private static readonly string fc_vel_key = "fc_vel";
		public static double fc_vel
		{
			get => Preferences.Get(fc_vel_key, 0.0);

			set => Preferences.Set(fc_vel_key, value);
		}

		private static readonly string disp_radio_key = "disp_radio";
		public static bool disp_radio
		{
			get => Preferences.Get(disp_radio_key, false);

			set => Preferences.Set(disp_radio_key, value);
		}

		private static readonly string fc_dis_key = "fc_dis";
		public static double fc_dis
		{
			get => Preferences.Get(fc_dis_key, 0.0);

			set => Preferences.Set(fc_dis_key, value);
		}

		private static readonly string a_s_q_p_radio_key = "a_s_q_p_radio";
		public static bool a_s_q_p_radio
		{
			get => Preferences.Get(a_s_q_p_radio_key, false);

			set => Preferences.Set(a_s_q_p_radio_key, value);
		}
		private static readonly string a_s_rms_radio_key = "a_s_rms_radio";
		public static bool a_s_rms_radio
		{
			get => Preferences.Get(a_s_rms_radio_key, false);

			set => Preferences.Set(a_s_rms_radio_key, value);
		}
		private static readonly string p_s_radio_key = "p_s_radio";
		public static bool p_s_radio
		{
			get => Preferences.Get(p_s_radio_key, false);

			set => Preferences.Set(p_s_radio_key, value);
		}
		private static readonly string p_s_d_radio_key = "p_s_d_radio";
		public static bool p_s_d_radio
		{
			get => Preferences.Get(p_s_d_radio_key, false);

			set => Preferences.Set(p_s_d_radio_key, value);
		}

		private static readonly string a_s_d_radio_key = "a_s_d_radio";
		public static bool a_s_d_radio
		{
			get => Preferences.Get(a_s_d_radio_key, false);

			set => Preferences.Set(a_s_d_radio_key, value);
		}
		private static readonly string rectangular_radio_key = "rectangular_radio";
		public static bool rectangular_radio
		{
			get => Preferences.Get(rectangular_radio_key, false);

			set => Preferences.Set(rectangular_radio_key, value);
		}
		private static readonly string hanning_radio_key = "hanning_radio";
		public static bool hanning_radio
		{
			get => Preferences.Get(hanning_radio_key, false);

			set => Preferences.Set(hanning_radio_key, value);
		}
		private static readonly string flattop_radio_key = "flattop_radio";
		public static bool flattop_radio
		{
			get => Preferences.Get(flattop_radio_key, false);

			set => Preferences.Set(flattop_radio_key, value);
		}
		private static readonly string no_scaling_key = "no_scaling";
		public static bool no_scaling
		{
			get => Preferences.Get(no_scaling_key, false);

			set => Preferences.Set(no_scaling_key, value);
		}
		private static readonly string chart_scaling_key = "chart_scaling";
		public static bool chart_scaling
		{
			get => Preferences.Get(chart_scaling_key, false);

			set => Preferences.Set(chart_scaling_key, value);
		}
		private static readonly string avginput_key = "avginput";
		public static int avginput
		{
			get => Preferences.Get(avginput_key, 0);

			set => Preferences.Set(avginput_key, value);
		}
		private static readonly string fftinput_key = "fftinput";
		public static int fftinput
		{
			get => Preferences.Get(fftinput_key, 0);

			set => Preferences.Set(fftinput_key, value);
		}

		private static readonly string editTau_key = "editTau";
		public static float EditTau
		{
			get => Preferences.Get(editTau_key, 0f);

			set => Preferences.Set(editTau_key, value);
		}
		private static readonly string editPeriodShow_key = "editPeriodShow";
		public static int EditPeriodShow
		{
			get => Preferences.Get(editPeriodShow_key, 0);

			set => Preferences.Set(editPeriodShow_key, value);
		}
		private static readonly string editPeriodKeep_key = "editPeriodKeep";
		public static int EditPeriodKeep
		{
			get => Preferences.Get(editPeriodKeep_key, 0);

			set => Preferences.Set(editPeriodKeep_key, value);
		}
		private static readonly string editPeriodSkipNext_key = "editPeriodSkipNext";
		public static int EditPeriodSkipNext
		{
			get => Preferences.Get(editPeriodSkipNext_key, 0);

			set => Preferences.Set(editPeriodSkipNext_key, value);
		}

		private static readonly string f_switch_key = "f_switch";
		public static bool F_switch
		{
			get => Preferences.Get(f_switch_key, false);

			set => Preferences.Set(f_switch_key, value);
		}

		private static readonly string T_switch_key = "t_switch";
		public static bool T_switch
		{
			get => Preferences.Get(T_switch_key, false);

			set => Preferences.Set(T_switch_key, value);
		}
		private static readonly string data_scaling_key = "data_scaling";
		public static bool data_scaling
		{
			get => Preferences.Get(data_scaling_key, false);

			set => Preferences.Set(data_scaling_key, value);
		}
		private static readonly string recordlimitinput_key = "recordlimitinput";
		public static int recordlimitinput
		{
			get => Preferences.Get(recordlimitinput_key, 0);
			set => Preferences.Set(recordlimitinput_key, value);
		}


	}
}
