﻿namespace RecovibTiny.Helper
{
	public class ParamType
	{
		public static readonly byte
			SN8 = 0x01,
			SN8_SIZE = 0x08,
			DT = 0x02,
			DT_SIZE = 0x0E,
			FLOAT32 = 0x03,
			FLOAT32_SIZE = 0x04,
			INT8 = 0x04,
			INT8_SIZE = 0x01,
			INT16 = 0x05,
			INT16_SIZE = 0x02,
			UINT16 = 0x06,
			UINT16_SIZE = 0x02,
			UINT32 = 0x07,
			UINT32_SIZE = 0x04;
	}
}