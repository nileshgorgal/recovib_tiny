﻿namespace RecovibTiny.Helper
{
	public class Constant
	{
		public static int VendorId = 8263;
		public static int ProductId = 2379;
		internal const string ZERO = "0";

		public enum LargeArrayType
		{

			BIT,
			BYTE,
			SHORT,
			INT,
			LONG,
			FLOAT,
			DOUBLE,
			STRING
		}
		/*  //Filter Possibilities
          public enum FILTER_CHOICE
          {
              HP, LP, HP_LP, NO_FILTER
          }
          //Input Possibilities
          public enum INPUT_CHOICE
          {
              REAL_VALUES, RECORDED_VALUES
          }
          //Viewer Device Possibilities
          public enum MODE_CHOICE
          {
              ACCELERATION_MODE, VELOCITY_MODE, DISPLACEMENT_MODE
          }
          public enum WINDOW_CHOICE
          {
              RECTANGULAR_WINDOW, HANNING_WINDOW, FLAT_TOP_WINDOW
          }
          public enum FREQUENTIAL_CHOICE
          {
              AMPLITUDE_SPECTRUM_PEAK, AMPLITUDE_SPECTRUM_RMS, POWER_SPECTRUM, POWER_SPECTRAL_DENSITY, AMPLITUDE_SPECTRAL_DENSITY
          }*/
	}
}