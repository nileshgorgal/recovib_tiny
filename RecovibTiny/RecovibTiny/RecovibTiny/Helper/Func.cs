﻿namespace RecovibTiny.Helper
{
	public class Func
	{
		public static readonly byte
			EVENT_EXT_DUMMY = 0x00,         // 1
			EVENT_EXT_GET_STATE = 0x01,     // 2
			EVENT_EXT_ARM = 0x02,           // 3
			EVENT_EXT_UNARM = 0x03,         // 4
			EVENT_EXT_PRODUCTION = 0x04,    // 5
			EVENT_EXT_UNPRODUCTION = 0x05,  // 6
			EVENT_EXT_SET_INFOA = 0x06,     // 7
			EVENT_EXT_GET_INFOA = 0x07,     // 8
			EVENT_EXT_SET_INFOB = 0x08,     // 9
			EVENT_EXT_GET_INFOB = 0x09,     // 10
			EVENT_INT_USB_ON = 0x0A,        // 11
			EVENT_INT_USB_OFF = 0x0B,       // 12
			EVENT_INT_USB_ERROR = 0x0C,     // 13
			EVENT_INT_TIMER_1S_TICK = 0x0D, // 14
			EVENT_INT_TIMER_ELAPSED = 0x0E, // 15
			EVENT_INT_STORE_START = 0x0F,   // 16
			EVENT_INT_STORE_STOP = 0x10,    // 17
			EVENT_INT_PGOOD_ON = 0x11,      // 18
			EVENT_INT_PGOOD_OFF = 0x12,     // 19
			EVENT_INT_BATTERY_LOW = 0x13,   // 20
			EVENT_EXT_SET_INFOC = 0x14,     // 21
			EVENT_EXT_GET_INFOC = 0x15,     // 22
			EVENT_EXT_USB_OFF_SIM = 0x16,   // 23
			EVENT_EXT_RST_REQ = 0x17,       // 24
			EVENT_EXT_SET_INFOD = 0x18,     // 25
			EVENT_EXT_GET_INFOD = 0x19,     // 26
			EVENT_EXT_SET_PARAM = 0x1A,     // 27
			EVENT_EXT_GET_PARAM = 0x1B,     // 28
			EVENT_EXT_CFG = 0x1C,           // 29
			EVENT_EXT_UNCFG = 0x1D,         // 30
			EVENT_EXT_GET_ACQ_TICK = 0x1E;   // 31
	}
}