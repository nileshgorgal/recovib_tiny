﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace RecovibTiny.Helper
{
	public class StringToColorConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			string valueAsString = value.ToString();
			switch (valueAsString)
			{
				case ("#176fc1"): { return Color.FromHex("#176fc1"); }
				case ("c6c6c7"): { return Color.FromHex("#c6c6c7"); }
				default:
					{
						return Color.FromHex(value.ToString());
					}
			}
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) { return null; }
	}
}
