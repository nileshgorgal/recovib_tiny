﻿using RecovibTiny.Helper;
using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RecovibTiny.CustomControl
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CustomIncrementalControl : ContentView
	{
		public event EventHandler IncrementalValueChanged;
		public CustomIncrementalControl()
		{
			InitializeComponent();
		}

		public string IncrementalValue
		{
			get { return (string)GetValue(IncrementalValueProperty); }
			set { SetValue(IncrementalValueProperty, value); }
		}

		public static readonly BindableProperty IncrementalValueProperty =
					BindableProperty.Create(nameof(IncrementalValue), typeof(string), typeof(CustomIncrementalControl), default, BindingMode.TwoWay, propertyChanged: (sender, oldValue, newValue) =>
					{
						((CustomIncrementalControl)sender).IncrementalValueChanged?.Invoke(sender, null);
					});

		private void btnSub_Clicked(object sender, EventArgs e)
		{
			int tempValue = 0;
			if (int.TryParse(IncrementalValue, out tempValue))
			{
				if (tempValue > 1)
				{
					IncrementalValue = (tempValue - 1).ToString();
				}
				else
				{
					IncrementalValue = Constant.ZERO;
				}
			}
			else
			{
				IncrementalValue = Constant.ZERO;
			}
		}

		private void btnAdd_Clicked(object sender, EventArgs e)
		{
			int tempValue = 0;
			int.TryParse(IncrementalValue, out tempValue);
			IncrementalValue = (tempValue + 1).ToString();
		}
	}
}