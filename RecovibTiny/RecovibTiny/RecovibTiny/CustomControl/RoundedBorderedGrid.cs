﻿using Xamarin.Forms;

namespace RecovibTiny.CustomControl
{
	public class RoundedBorderedGrid : Grid
	{
		public int BorderWidth { get; set; }
		public Color BorderColor { get; set; }
		public float CornerRadius { get; set; }
	}
}
