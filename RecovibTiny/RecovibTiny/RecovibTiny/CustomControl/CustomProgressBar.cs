﻿using Xamarin.Forms;

namespace RecovibTiny.CustomControl
{
	public class CustomProgressBar : ProgressBar
	{

		public float CurrentProgress
		{
			get { return (float)GetValue(CurrentProgressProperty); }
			set { SetValue(CurrentProgressProperty, value); }
		}

		public static readonly BindableProperty CurrentProgressProperty =
					BindableProperty.Create(nameof(CurrentProgress), typeof(float), typeof(CustomProgressBar), default, propertyChanged: (sender, oldValue, newValue) =>
					{
						if (newValue is float val)
						{
							val = val / 100;
							Device.BeginInvokeOnMainThread(() => ((CustomProgressBar)sender).ProgressTo(val, 250, Easing.Linear));
						}
					});


	}
}
