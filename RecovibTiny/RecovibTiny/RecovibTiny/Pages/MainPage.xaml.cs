﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RecovibTiny.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
		}

		private void TapGestureRecognizer_Tapped(object sender, EventArgs e)
		{

		}
		protected override bool OnBackButtonPressed()
		{
			MessagingCenter.Unsubscribe<object>(this, "Connect");
			return true;
		}


	}
}