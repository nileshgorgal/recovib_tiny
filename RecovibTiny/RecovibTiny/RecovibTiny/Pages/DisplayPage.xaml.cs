﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RecovibTiny.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DisplayPage : ContentPage
	{
		int IncresedValue = 1;
		const int HoursValue = 1;
		const int MinsValue = 2;
		const int SecValue = 3;
		public DisplayPage()
		{
			InitializeComponent();
		}

		private void AddValue_Clicked(object sender, EventArgs e)
		{
			switch (IncresedValue)
			{
				case HoursValue:
					{
						int hours = int.Parse(hoursText.Text);
						if (hours != 12)
						{
							hoursText.Text = (hours + 1).ToString();
						}
					}
					break;
				case MinsValue:
					{
						int minute = int.Parse(MiniteText.Text);
						if (minute != 58)
						{
							MiniteText.Text = (minute + 1).ToString();
						}
					}
					break;
				case SecValue:
					{
						int sec = int.Parse(SecText.Text);
						if (sec != 58)
						{
							SecText.Text = (sec + 1).ToString();
						}
					}
					break;
			}
		}

		private void SubValue_Clicked(object sender, EventArgs e)
		{
			switch (IncresedValue)
			{
				case HoursValue:
					{
						int hours = int.Parse(hoursText.Text);
						if (hours != 0)
						{
							hoursText.Text = (hours - 1).ToString();
						}
					}
					break;
				case MinsValue:
					{
						int minute = int.Parse(MiniteText.Text);
						if (minute != 0)
						{
							MiniteText.Text = (minute - 1).ToString();
						}
					}
					break;
				case SecValue:
					{
						int sec = int.Parse(SecText.Text);
						if (sec != 0)
						{
							SecText.Text = (sec - 1).ToString();
						}
					}
					break;
			}
		}

		private void Hours_Clicked(object sender, EventArgs e)
		{
			IncresedValue = HoursValue;
		}
		private void Minute_Clicked(object sender, EventArgs e)
		{
			IncresedValue = MinsValue;
		}
		private void Sec_Clicked(object sender, EventArgs e)
		{
			IncresedValue = SecValue;
		}

		private bool isManual = false;
		private void Manual_Clicked(object sender, EventArgs e)
		{
			if (isManual)
			{
				isManual = false;
				HourFrame1.IsVisible = true;
				MinFrame1.IsVisible = true;
				SecFrame1.IsVisible = true;
				HourFrame.IsVisible = false;
				MinFrame.IsVisible = false;
				SecFrame.IsVisible = false;
				IncrementView.IsVisible = false;
			}
			else
			{
				isManual = true;
				HourFrame.IsVisible = true;
				MinFrame.IsVisible = true;
				SecFrame.IsVisible = true;
				HourFrame1.IsVisible = false;
				MinFrame1.IsVisible = false;
				SecFrame1.IsVisible = false;
				IncrementView.IsVisible = true;
			}
		}

		private void Done_Clicked(object sender, EventArgs e)
		{
			String time = "";
			if (isManual)
			{
				if (int.Parse(hoursText1.Text) > 12)
				{
					DisplayAlert("Alert", "Please enter hours 12 or below", "OK");
					return;
				}
				else if (int.Parse(MiniteText1.Text) > 58)
				{
					DisplayAlert("Alert", "Please enter minute 58 or below", "OK");
					return;
				}
				else if (int.Parse(SecText1.Text) > 58)
				{
					DisplayAlert("Alert", "Please enter second 58 or below", "OK");
					return;
				}
				time = hoursText1.Text + ":" + MiniteText1.Text + ":" + SecText1.Text;
				MessagingCenter.Send<System.Object, string>(this, "TimePicker", time);
			}
			else
			{
				time = hoursText.Text + ":" + MiniteText.Text + ":" + SecText.Text;
				MessagingCenter.Send<System.Object, string>(this, "TimePicker", time);
			}
		}
		private void CANCEL_Clicked(object sender, EventArgs e)
		{

		}
	}
}