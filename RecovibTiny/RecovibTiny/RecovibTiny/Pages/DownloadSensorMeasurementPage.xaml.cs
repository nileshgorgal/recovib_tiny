﻿
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RecovibTiny.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DownloadSensorMeasurementPage : ContentPage
	{
		public DownloadSensorMeasurementPage()
		{
			InitializeComponent();
		}
	}
}