﻿using Rg.Plugins.Popup.Services;
using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Plugin.DateTimePicker;

namespace RecovibTiny.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MeasurementSetupPage : ContentPage
	{
		public MeasurementSetupPage()
		{
			InitializeComponent();
		}

		private void CustomDatePicker_DateSelected(object sender, DateChangedEventArgs e)
		{
			MeasurementSetupViewModel.ClickedValue = 3;
			PopupNavigation.Instance.PushAsync(new ContentViews.CustomTimePicker(), true);
		}

		private async void TapGestureRecognizer_TappedAsync(object sender, EventArgs e)
		{
			DialogSettings settings = new DialogSettings();
			settings.SelectedDate = DateTime.Now.AddDays(-1);
			settings.MinimumDate = DateTime.Now.AddDays(-10);
			settings.MaximumDate = DateTime.Now.AddDays(10);
			settings.SelectedHour = DateTime.Now.Hour;
			settings.SelectedMinute = DateTime.Now.Minute;
			var result = await new DateTimePickerDialog().Show(this, settings);
			if (result != null)
				await DisplayAlert("Result", "Selected: " + result.Value.ToString("dd MMM yyyy HH:mm"), "Cancel");
		}

		private void CustomDatePicker_DateSelected_1(object sender, DateChangedEventArgs e)
		{
			MeasurementSetupViewModel.ClickedValue = 4;
			PopupNavigation.Instance.PushAsync(new ContentViews.CustomTimePicker(), true);
		}
	}
}