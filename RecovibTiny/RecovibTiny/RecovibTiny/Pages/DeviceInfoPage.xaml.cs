﻿
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RecovibTiny.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DeviceInfoPage : ContentPage
	{
		public DeviceInfoPage()
		{
			InitializeComponent();
		}
	}
}