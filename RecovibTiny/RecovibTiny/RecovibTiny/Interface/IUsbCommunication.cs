﻿namespace RecovibTiny.Interface
{
	public interface IUsbCommunication
	{
		bool IsDeviceConnected();
	}
}
