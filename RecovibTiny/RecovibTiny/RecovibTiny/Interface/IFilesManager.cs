﻿using System.Threading.Tasks;

namespace RecovibTiny.Interface
{
	public interface IFilesManager
	{
		Task SaveFile(string fileName, byte[] content);
	}
}
