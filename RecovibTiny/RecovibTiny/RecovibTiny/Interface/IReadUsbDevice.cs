﻿using RecovibTiny.Model;
using System;
using System.Collections.Generic;

namespace RecovibTiny.Interface
{
	public interface IReadUsbDevice
	{
		List<DeviceInfo> getBatteryLevel();
		string GetSN();

		#region SetArm	
		void InitArmSetup(byte pSensitivity, DateTime startAt, DateTime stopAt);
		byte[] ComputeDateTime(DateTime toCompute);
		void Set_Method(byte Event, byte idFunction, byte typeFunction, byte[] bufferSet);
		void SetArm(int start_tick, int stop_tick);
		void SetSensitivity(byte sensitivity);
		void Set_Method_event(byte[] bufferSet);
		byte[] ReceiveData();
		#endregion SetArm

		#region Download

		//Currently used
		void GetConvParamFromHIDCOM(String SD_id);
		#endregion Download
	}
}
