﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RecovibTiny.Interface
{
	public interface IArmSetup
	{
		void InitArmSetup(byte pSensitivity, DateTime startAt, DateTime stopAt, DateTime durationNow);
	}
}
