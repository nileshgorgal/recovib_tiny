﻿namespace RecovibTiny.Interface
{
	public interface IGetPermission
	{
		bool GetDeviceConnected();
	}
}
