﻿namespace RecovibTiny.Interface
{
	public interface IDatePicker
	{
		string GetSelectedDate();
	}
}
