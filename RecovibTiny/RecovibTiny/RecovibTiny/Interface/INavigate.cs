﻿namespace RecovibTiny.Interface
{
	public interface INavigate
	{
		void StartNativeIntentOrActivity();
	}
}
