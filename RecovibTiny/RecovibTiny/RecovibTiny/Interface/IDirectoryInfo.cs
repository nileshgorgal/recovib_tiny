﻿namespace RecovibTiny.Interface
{
	public interface IDirectoryInfo
	{
		bool ComapareVersion();
		void GetAndroid11Permission();
		void CopyMeasureBinFile();
	}
}
