﻿using RecovibTiny.Interface;
using System;
using System.IO;
using System.Linq;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace RecovibTiny.ViewModels
{
	public class DownloadSensorMeasurementViewModel : BaseViewModel
	{
		private int _PercentComplete;
		public int PercentComplete
		{
			get => _PercentComplete;
			set => SetProperty(ref _PercentComplete, value);
		}

		private bool _SetDownloadView = false;
		public bool SetDownloadView
		{
			get => _SetDownloadView;
			set => SetProperty(ref _SetDownloadView, value);
		}
		private bool _IsDisConnected = true;
		public bool IsDisConnected
		{
			get => _IsDisConnected;
			set => SetProperty(ref _IsDisConnected, value);
		}

		private String _InfoText = "USB drive mounting ongoing ...";
		public String InfoText
		{
			get => _InfoText;
			set => SetProperty(ref _InfoText, value);
		}
		public DownloadSensorMeasurementViewModel()
		{
			try
			{
				CheckAndRequestPermissionAsync();
				MessagingCenter.Subscribe<object, int>(this, "downloadProgress", (sender, arg) =>
				{
					PercentComplete = arg;
					SetDownloadView = true;
					IsDisConnected = false;
					if (PercentComplete == 100)
					{
						InfoText = "Download Complete..";
						IsDisConnected = true;
						SetDownloadView = false;
					}
				});
				if (DependencyService.Get<IDirectoryInfo>(DependencyFetchTarget.GlobalInstance).ComapareVersion())
				{
					/*String Android11_Path = GlobalPreferences.Android11_Path;
					if (Android11_Path != null && !Android11_Path.Equals(string.Empty))
					{
						Uri uri = new Uri(Android11_Path);
						DependencyService.Get<IDirectoryInfo>(DependencyFetchTarget.GlobalInstance).CopyMeasureBinFile();

					}
					else
					{*/
					DependencyService.Get<IDirectoryInfo>(DependencyFetchTarget.GlobalInstance).GetAndroid11Permission();
					//}
				}
				else
				{
					CheckSD();
				}
			}
			catch (Exception ex)
			{
				string e = ex.Message;
			}
		}

		public string Measure_bin_exists()
		{
			DirectoryInfo directories = new DirectoryInfo("/storage/");
			if (directories.Exists)
			{
				var files = Directory.EnumerateFiles("/storage/", "*.*", SearchOption.AllDirectories).Where(s => s.Contains(".BIN"));
				foreach (string f in files)
				{
					if (File.Exists(f))
					{
						//String[] lst = f.Split('/');
						return f;
					}
				}
			}
			else
			{

			}

			return "";
		}
		String SD_id;
		public void CheckSD()
		{
			try
			{
				string sd_path;
				if ((sd_path = Measure_bin_exists()) != null)
				{
					SD_id = sd_path;
					DependencyService.Get<IReadUsbDevice>(DependencyFetchTarget.GlobalInstance).GetConvParamFromHIDCOM(SD_id);
				}
				else
				{
					//	handler_SD.postDelayed(runnable_SD, 500);
				}
			}
			catch (Exception ex)
			{
				string error = ex.Message;
			}
		}

		private async void CheckAndRequestPermissionAsync()
		{
			var status = await Permissions.CheckStatusAsync<Permissions.StorageRead>();
			if (status != PermissionStatus.Granted)
			{
				await Permissions.RequestAsync<Permissions.StorageRead>();
			}

			status = await Permissions.CheckStatusAsync<Permissions.StorageWrite>();
			if (status != PermissionStatus.Granted)
			{
				await Permissions.RequestAsync<Permissions.StorageWrite>();
			}
		}
	}
}
