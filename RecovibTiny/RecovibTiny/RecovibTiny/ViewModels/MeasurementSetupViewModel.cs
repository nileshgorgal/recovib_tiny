﻿using RecovibTiny.Helper;
using RecovibTiny.Interface;
using Rg.Plugins.Popup.Services;
using System;
using System.Windows.Input;
using Xamarin.Forms;

namespace RecovibTiny.ViewModels
{
	public class MeasurementSetupViewModel : BaseViewModel
	{
		#region Command
		public ICommand PrevCommand { protected set; get; }
		public ICommand DoneCommand { protected set; get; }
		public ICommand StartAtEditCommand { protected set; get; }
		public ICommand StopAtEditCommand { protected set; get; }
		public ICommand StartInEditCommand { protected set; get; }
		public ICommand DurationEditCommand { protected set; get; }
		#endregion Command

		#region Properties
		private DateTime _StartAtEdit = DateTime.Now.AddSeconds(30);
		public DateTime StartAtEdit
		{
			get => _StartAtEdit;
			set => SetProperty(ref _StartAtEdit, value);
		}

		private DateTime _StopAtEdit = DateTime.Now.AddSeconds(30);
		public DateTime StopAtEdit
		{
			get => _StopAtEdit;
			set => SetProperty(ref _StopAtEdit, value);
		}

		private bool _StartTimeRadio = false;
		public bool StartTimeRadio
		{
			get => _StartTimeRadio;
			set => SetProperty(ref _StartTimeRadio, value);
		}

		private TimeSpan _StartAtEditTime = new TimeSpan(0, 0, 30);
		public TimeSpan StartAtEditTime
		{
			get => _StartAtEditTime;
			set => SetProperty(ref _StartAtEditTime, value);
		}

		private TimeSpan _StopAtEditTime = new TimeSpan(0, 0, 30);
		public TimeSpan StopAtEditTime
		{
			get => _StopAtEditTime;
			set => SetProperty(ref _StopAtEditTime, value);
		}

		private TimeSpan _StartInEdit = new TimeSpan(0, 0, 30);
		public TimeSpan StartInEdit
		{
			get => _StartInEdit;
			set => SetProperty(ref _StartInEdit, value);
		}

		private TimeSpan _DurationEdit = new TimeSpan(0, 0, 30);
		public TimeSpan DurationEdit
		{
			get => _DurationEdit;
			set => SetProperty(ref _DurationEdit, value);
		}

		private bool _StartDelayRadio = true;
		public bool StartDelayRadio
		{
			get => _StartDelayRadio;
			set => SetProperty(ref _StartDelayRadio, value);
		}

		private bool _Radio2gChecked = true;
		public bool Radio2gChecked
		{
			get => _Radio2gChecked;
			set => SetProperty(ref _Radio2gChecked, value);
		}

		private bool _Radio6gChecked = false;
		public bool Radio6gChecked
		{
			get => _Radio6gChecked;
			set => SetProperty(ref _Radio6gChecked, value);
		}

		private bool _Radio2gVisible;
		public bool Radio2gVisible
		{
			get => _Radio2gVisible;
			set => SetProperty(ref _Radio2gVisible, value);
		}
		private bool _Radio6gVisible;
		public bool Radio6gVisible
		{
			get => _Radio6gVisible;
			set => SetProperty(ref _Radio6gVisible, value);
		}

		private String _Radio2g = "±2g";
		public String Radio2g
		{
			get => _Radio2g;
			set => SetProperty(ref _Radio2g, value);
		}

		private String _Radio6g = "±6g";
		public String Radio6g
		{
			get => _Radio6g;
			set => SetProperty(ref _Radio6g, value);
		}
		#endregion Properties

		#region Variables
		public int ClickedValue = 1;
		private const int StartInEditClicked = 1;
		private const int DurationEditClicked = 2;
		public const int StartAtEditClicked = 3;
		public const int StopAtEditClicked = 4;
		#endregion Variables
		public MeasurementSetupViewModel()
		{
			Initialize();
		}

		private void Initialize()
		{
			PrevCommand = new Command(PrevCommandExecute);
			DoneCommand = new Command(DoneCommandExecute);
			StartAtEditCommand = new Command(StartAtEditCommandExecuteAsync);
			StopAtEditCommand = new Command(StopAtEditCommandExecute);
			StartInEditCommand = new Command(StartInEditCommandExecute);
			DurationEditCommand = new Command(DurationEditCommandExecute);

			SetRange();
			SetDateData();
			MessagingCenter.Subscribe<object, string>(this, "TimePicker", (sender, arg) =>
			{
				String[] time = arg.Split(':');
				int hour = int.Parse(time[0]);
				int mins = int.Parse(time[1]);
				int sec = int.Parse(time[2]);

				if (ClickedValue == StartInEditClicked)
				{
					StartInEdit = new TimeSpan(hour, mins, sec);
				}
				else if (ClickedValue == DurationEditClicked)
				{
					DurationEdit = new TimeSpan(hour, mins, sec);
				}
				else if (ClickedValue == StartAtEditClicked)
				{
					StartAtEditTime = new TimeSpan(hour, mins, sec);
				}
				else if (ClickedValue == StopAtEditClicked)
				{
					StopAtEditTime = new TimeSpan(hour, mins, sec);
				}
			});
		}


		private void SetDateData()
		{
			DateTime startAtInit = DateTime.Now.AddSeconds(30);
			DateTime stopAtInit = startAtInit.AddSeconds(30);
			startAtEditString = startAtInit.ToString("yyyy-MM-dd HH:mm:ss");
			stopAtEditString = stopAtInit.ToString("yyyy-MM-dd HH:mm:ss");
		}

		private void SetRange()
		{
			String _sn = "";
			//RANGE INITIAL CONFIG
			try
			{
				_sn = DependencyService.Get<IReadUsbDevice>(DependencyFetchTarget.GlobalInstance).GetSN();
			}
			catch (Exception e)
			{
				String message = e.Message;
			}
			int i = GetRange(_sn);
			switch (i)
			{
				case 0:
					//2/6g ±2g 

					Radio2gChecked = true;
					Radio2gVisible = true;
					Radio6gVisible = true;
					break;
				case 1:
					//15g ±6g

					Radio2gChecked = true;
					Radio2gVisible = true;
					Radio6gVisible = false;
					Radio2g = "±15g";
					break;
				case 2:
					//200g

					Radio2gChecked = true;
					Radio2gVisible = true;
					Radio6gVisible = false;
					Radio2g = "±200g";
					break;
				default:
					/*toastMessage(getString(R.string.bad_sn), false);*/
					break;

			}
		}

		private void DoneCommandExecute()
		{
			if (DependencyService.Get<Interface.IUsbCommunication>(DependencyFetchTarget.GlobalInstance).IsDeviceConnected())
			{
				SetArm();
			}
			else
			{
				DependencyService.Get<Interface.IGetPermission>(DependencyFetchTarget.GlobalInstance).GetDeviceConnected();
			}
		}

		private void PrevCommandExecute()
		{
			PushPage(new Pages.DeviceInfoPage() { BindingContext = new DeviceInfoViewModel() });
		}

		public int GetRange(String serial)
		{
			if (serial != null && serial.Length > 2)
			{
				if (serial.Substring(2, 2).Equals("30"))
				{
					return 0;
				}
				else if (serial.Substring(2, 2).Equals("31"))
				{
					return 1;
				}
				else if (serial.Substring(2, 2).Equals("32"))
				{
					return 2;
				}
				else
				{
					return 3;
				}
			}
			else
			{
				return 3;
			}
		}

		private void StartAtEditCommandExecuteAsync()
		{
			ClickedValue = StartAtEditClicked;
			PopupNavigation.Instance.PushAsync(new ContentViews.CustomTimePicker(), true);
		}
		private void StopAtEditCommandExecute()
		{
			ClickedValue = StopAtEditClicked;
			PopupNavigation.Instance.PushAsync(new ContentViews.CustomTimePicker(), true);
		}
		private void StartInEditCommandExecute()
		{
			ClickedValue = StartInEditClicked;
			PopupNavigation.Instance.PushAsync(new ContentViews.CustomTimePicker(), true);
		}
		private void DurationEditCommandExecute()
		{
			ClickedValue = DurationEditClicked;
			PopupNavigation.Instance.PushAsync(new ContentViews.CustomTimePicker(), true);
		}
		private DateTime startAt;
		private DateTime stopAt;
		public string startAtEditString;
		public string stopAtEditString;
		private void SetArm()
		{
			/*byte[] dateBuffer = new byte[14];
			byte[] bufferRcvd;
			DateTime durationNow;

			if (Radio2gChecked)
			{
				DependencyService.Get<IReadUsbDevice>(DependencyFetchTarget.GlobalInstance).SetSensitivity(0x02);
			}
			else
			{
				DependencyService.Get<IReadUsbDevice>(DependencyFetchTarget.GlobalInstance).SetSensitivity(0x06);
			}

			durationNow = DateTime.Now;
			if (StartDelayRadio)
			{
				startAt = durationNow.AddSeconds(StartInEdit.TotalSeconds);
				stopAt = startAt.AddSeconds(DurationEdit.TotalSeconds);
			}
			else
			{
				string dateStart = StartAtEdit.Year + "-" + StartAtEdit.Month.ToString("00") + "-" + StartAtEdit.Day.ToString("00"); //+1 for the month 0 based
				startAtEditString = dateStart + " " + StartAtEditTime.ToString();

				string dateStop = StopAtEdit.Year + "-" + StopAtEdit.Month.ToString("00") + "-" + StopAtEdit.Day.ToString("00"); //+1 for the month 0 based
				stopAtEditString = dateStop + " " + StopAtEditTime.ToString();

				startAt = DateTime.ParseExact(startAtEditString, "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
				stopAt = DateTime.ParseExact(stopAtEditString, "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
			}

			var dateNow = new DateTimeOffset(DateTime.Now);
			var startDate = new DateTimeOffset(startAt);
			var stopDate = new DateTimeOffset(stopAt); 

			//dateBuffer = ComputeDateTime(DateTime.Now);
			dateBuffer = ComputeDateTime(dateNow);
			DependencyService.Get<IReadUsbDevice>(DependencyFetchTarget.GlobalInstance).Set_Method((byte)Func.EVENT_EXT_SET_PARAM, ParamId.DT_ARM_ID, ParamType.DT, dateBuffer);
			dateBuffer = null;
			bufferRcvd = DependencyService.Get<IReadUsbDevice>(DependencyFetchTarget.GlobalInstance).ReceiveData();
			bufferRcvd = null;

			dateBuffer = ComputeDateTime(startDate);
			DependencyService.Get<IReadUsbDevice>(DependencyFetchTarget.GlobalInstance).Set_Method((byte)Func.EVENT_EXT_SET_PARAM, ParamId.DT_START_ID, ParamType.DT, dateBuffer);
			dateBuffer = null;
			bufferRcvd = DependencyService.Get<IReadUsbDevice>(DependencyFetchTarget.GlobalInstance).ReceiveData();
			bufferRcvd = null;

			dateBuffer = ComputeDateTime(stopDate);
			DependencyService.Get<IReadUsbDevice>(DependencyFetchTarget.GlobalInstance).Set_Method((byte)Func.EVENT_EXT_SET_PARAM, ParamId.DT_STOP_ID, ParamType.DT, dateBuffer);
			dateBuffer = null;
			bufferRcvd = DependencyService.Get<IReadUsbDevice>(DependencyFetchTarget.GlobalInstance).ReceiveData();
			bufferRcvd = null;

			long ms_to_start = new DateTimeOffset(startAt.ToUniversalTime()).ToUnixTimeMilliseconds() - new DateTimeOffset(durationNow.ToUniversalTime()).ToUnixTimeMilliseconds();
			long ms_to_stop = new DateTimeOffset(stopAt.ToUniversalTime()).ToUnixTimeMilliseconds() - new DateTimeOffset(durationNow.ToUniversalTime()).ToUnixTimeMilliseconds();
			if (!(ms_to_start >= ms_to_stop) && !(ms_to_start <= 0) && !(ms_to_stop <= 0))
			{
				int ticks_to_start = (int)ms_to_start / 1000;
				int ticks_to_stop = (int)ms_to_stop / 1000;

				ticks_to_start *= 1024;
				ticks_to_stop *= 1024;

				DependencyService.Get<IReadUsbDevice>(DependencyFetchTarget.GlobalInstance).SetArm(ticks_to_start, ticks_to_stop - 1);
				bufferRcvd = DependencyService.Get<IReadUsbDevice>(DependencyFetchTarget.GlobalInstance).ReceiveData();
				bufferRcvd = null;
				DisplayMessage("Successful", "RECOVIB Tiny armed", "OK");
				PushPage(new Pages.MainPage());
			}
			else
			{
				DisplayException("Bad timing configuration..!");
			}*/
			try
			{
				byte Sensitivity = Radio2gChecked ? (byte)0x02 : (byte)0x06;
				DateTime durationNow = DateTime.Now;
				if (StartDelayRadio)
				{
					startAt = durationNow.AddSeconds(StartInEdit.TotalSeconds);
					stopAt = startAt.AddSeconds(DurationEdit.TotalSeconds);
				}
				else
				{
					string dateStart = StartAtEdit.Year + "-" + StartAtEdit.Month.ToString("00") + "-" + StartAtEdit.Day.ToString("00"); //+1 for the month 0 based
					startAtEditString = dateStart + " " + StartAtEditTime.ToString();

					string dateStop = StopAtEdit.Year + "-" + StopAtEdit.Month.ToString("00") + "-" + StopAtEdit.Day.ToString("00"); //+1 for the month 0 based
					stopAtEditString = dateStop + " " + StopAtEditTime.ToString();

					startAt = DateTime.ParseExact(startAtEditString, "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
					stopAt = DateTime.ParseExact(stopAtEditString, "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
				} 

				//DependencyService.Get<IReadUsbDevice>(DependencyFetchTarget.GlobalInstance).InitArmSetup(Sensitivity,startAt, stopAt); 
				lock (this)
				{
					DependencyService.Get<IArmSetup>(DependencyFetchTarget.GlobalInstance).InitArmSetup(Sensitivity, startAt, stopAt, durationNow);
				}
				DisplayMessage("Successful", "RECOVIB Tiny armed", "OK");
				PushPage(new Pages.MainPage());
			}
			catch (Exception ex)
			{
				string error = ex.Message;
			}
		}

		public byte[] ComputeDateTime(DateTimeOffset toCompute)
		{
			DateTimeOffset dt_compute = toCompute;

			int year, month, day, hour, minute, second, millisecond;

			year = dt_compute.Year;
			month = dt_compute.Month;
			day = dt_compute.Day;
			hour = dt_compute.Hour;
			minute = dt_compute.Minute;
			second = dt_compute.Second;
			millisecond = dt_compute.Millisecond;

			//convert the date
			byte[] data = new byte[14];
			data[0] = (byte)(year & 0xFF);
			data[1] = (byte)((year >> 8) & 0xFF);

			data[2] = (byte)(month & 0xFF);
			data[3] = (byte)((month >> 8) & 0xFF);

			data[4] = (byte)(day & 0xFF);
			data[5] = (byte)((day >> 8) & 0xFF);

			data[6] = (byte)(hour & 0xFF);
			data[7] = (byte)((hour >> 8) & 0xFF);

			data[8] = (byte)(minute & 0xFF);
			data[9] = (byte)((minute >> 8) & 0xFF);

			data[10] = (byte)(second & 0xFF);
			data[11] = (byte)((second >> 8) & 0xFF);

			data[12] = (byte)(millisecond & 0xFF);
			data[13] = (byte)((millisecond >> 8) & 0xFF);
			return data;

		}
	}
}
