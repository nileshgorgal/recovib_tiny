﻿using RecovibTiny.Interface;
using RecovibTiny.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Input;
using Xamarin.Forms;

namespace RecovibTiny.ViewModels
{
	public class DeviceInfoViewModel : BaseViewModel
	{
		#region properties
		public ICommand PrevCommand { protected set; get; }
		public ICommand NextCommand { protected set; get; }

		private String _SerialNumber;
		public string SerialNumber
		{
			get => _SerialNumber;
			set => SetProperty(ref _SerialNumber, value);
		}

		private String _FirmwareVersion;
		public string FirmwareVersion
		{
			get => _FirmwareVersion;
			set => SetProperty(ref _FirmwareVersion, value);
		}
		private String _BatteryLevel;
		public string BatteryLevel
		{
			get => _BatteryLevel;
			set => SetProperty(ref _BatteryLevel, value);
		}
		private String _LastCalibration;
		public string LastCalibration
		{
			get => _LastCalibration;
			set => SetProperty(ref _LastCalibration, value);
		}

		#endregion properties
		public DeviceInfoViewModel()
		{
			Initialize();
			List<DeviceInfo> deviceInfos = DependencyService.Get<IReadUsbDevice>(DependencyFetchTarget.GlobalInstance).getBatteryLevel();
			if (deviceInfos != null)
			{
				foreach (DeviceInfo device in deviceInfos)
				{
					_BatteryLevel = (device._batteryLevel / 100).ToString("P", CultureInfo.InvariantCulture);
					SerialNumber = device._SerialNumber;
					_FirmwareVersion = device._FirmwareVersion;
					_LastCalibration = device._CalibDate;
				}
			}
		}

		private void Initialize()
		{
			PrevCommand = new Command(PrevCommandExecute);
			NextCommand = new Command(NextCommandExecute);
		}

		private void NextCommandExecute()
		{
			PushPage(new Pages.MeasurementSetupPage());// { BindingContext = new MeasurementSetupViewModel() });
		}

		private void PrevCommandExecute()
		{
			PushPage(new Pages.MainPage()); //{ BindingContext = new MainViewModel() });
		}
	}
}
