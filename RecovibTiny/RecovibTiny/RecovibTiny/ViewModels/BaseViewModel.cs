﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace RecovibTiny.ViewModels
{
	public class BaseViewModel : INotifyPropertyChanged
	{
		#region Navigations

		public void PushPage(Xamarin.Forms.ContentPage masterPage)
		{
			if (Xamarin.Forms.Application.Current.MainPage.Navigation.NavigationStack.Count > 0)
			{
				App.Current.MainPage.Navigation.PushAsync(masterPage, false);
			}
			else
			{
				App.Current.MainPage = new Xamarin.Forms.NavigationPage(masterPage);
			}
		}

		public void SetMainPage(Xamarin.Forms.ContentPage masterPage)
		{
			App.Current.MainPage = new Xamarin.Forms.NavigationPage(masterPage);
		}

		public void PushPage(Xamarin.Forms.ContentPage page, bool animate = false)
		{
			if (Xamarin.Forms.Application.Current.MainPage.Navigation.NavigationStack.Count > 0)
			{
				App.Current.MainPage.Navigation.PushAsync(page, animate);
			}
			else
			{
				App.Current.MainPage = new Xamarin.Forms.NavigationPage(page);
			}
		}

		public void PopPage()
		{
			if (App.Current.MainPage.Navigation.NavigationStack.Count > 1)
			{
				App.Current.MainPage.Navigation.PopAsync(false);
			}
		}


		public void PopRootPage()
		{
			if (App.Current.MainPage.Navigation.NavigationStack.Count > 1)
			{
				App.Current.MainPage.Navigation.PopToRootAsync(false);
			}
		}

		#endregion Navigations

		#region Dialog
		public async void DisplayException(string message)
		{
			await App.Current.MainPage.DisplayAlert("Error", message, "OK");
		}
		public void DisplayMessage(string title, string message, string button)
		{
			App.Current.MainPage.DisplayAlert(title, message, button);
		}
		public Task<bool> DisplayMessageAsync(string title, string message, string accept, string cancel)
		{
			return App.Current.MainPage.DisplayAlert(title, message, accept, cancel);
		}
		public Task DisplayMessageAsync(string title, string message, string button)
		{
			return App.Current.MainPage.DisplayAlert(title, message, button);
		}
		#endregion Dialog

		protected void SetProperty<T>(ref T referencedVariable, T newValue, [CallerMemberName] string propertyName = "")
		{
			if (!object.Equals(referencedVariable, newValue))
			{
				referencedVariable = newValue;
				RaisePropertyChanged(propertyName);
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;

		protected void RaisePropertyChanged([CallerMemberName] string propertyName = "")
		{
			if (!string.IsNullOrEmpty(propertyName))
			{
				PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
}
