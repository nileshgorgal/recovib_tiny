﻿using RecovibTiny.Interface;
using Rg.Plugins.Popup.Services;
using System;
using System.Windows.Input;
using Xamarin.Forms;

namespace RecovibTiny.ViewModels
{
	public class MainViewModel : BaseViewModel
	{
		#region Properties
		public ICommand NextCommand { protected set; get; }
		public ICommand HelpCommand { protected set; get; }

		private bool _IsConnected = false;
		public bool IsConnected
		{
			get => _IsConnected;
			set => SetProperty(ref _IsConnected, value);
		}
		private bool _SensorMeasurementsSetup = false;
		public bool SensorMeasurementsSetup
		{
			get => _SensorMeasurementsSetup;
			set => SetProperty(ref _SensorMeasurementsSetup, value);
		}

		private bool _DownloadSensorMeasurements = false;
		public bool DownloadSensorMeasurements
		{
			get => _DownloadSensorMeasurements;
			set => SetProperty(ref _DownloadSensorMeasurements, value);
		}

		private bool _SensorMeasurementsAnalysis = true;
		public bool SensorMeasurementsAnalysis
		{
			get => _SensorMeasurementsAnalysis;
			set => SetProperty(ref _SensorMeasurementsAnalysis, value);
		}

		private string _ConnectionStatus;
		public string ConnectionStatus
		{
			get => _ConnectionStatus;
			set => SetProperty(ref _ConnectionStatus, value);
		}

		private string _ConnStatusColor;
		public string ConnStatusColor
		{
			get => _ConnStatusColor;
			set => SetProperty(ref _ConnStatusColor, value);
		}

		#endregion
		public MainViewModel()
		{
			Initialize();
		}

		#region Methods
		private void Initialize()
		{
			NextCommand = new Command(NextCommandExecute);
			HelpCommand = new Command(HelpCommandExecuteAsync);
			MessagingCenter.Subscribe<object, string>(this, "Connect", (sender, arg) =>
			{
				if (arg.Equals("true"))
				{
					ConnectionStatus = "RecovibTiny Connected";
					ConnStatusColor = "#176fc1";
					IsConnected = true;
				}
				else
				{
					ConnectionStatus = "Please connect a RecovibTiny";
					ConnStatusColor = "#c6c6c7";
					IsConnected = false;
				}
			});
			bool isConnected = DependencyService.Get<IGetPermission>(DependencyFetchTarget.GlobalInstance).GetDeviceConnected();
			if (isConnected)
			{
				ConnectionStatus = "RecovibTiny Connected";
				ConnStatusColor = "#176fc1";
				IsConnected = true;
			}
			else
			{
				ConnectionStatus = "Please connect a RecovibTiny";
				ConnStatusColor = "#c6c6c7";
				IsConnected = false;
			}
		}

		private void NextCommandExecute()
		{
			bool isConnected = DependencyService.Get<IGetPermission>(DependencyFetchTarget.GlobalInstance).GetDeviceConnected();
			if (isConnected)
			{
				SetMainPage(new Pages.MainPage());
				if (SensorMeasurementsSetup)
				{
					PushPage(new Pages.DeviceInfoPage() { BindingContext = new DeviceInfoViewModel() });
				}
				else if (DownloadSensorMeasurements)
				{
					PushPage(new Pages.DownloadSensorMeasurementPage()); //{ BindingContext = new DownloadSensorMeasurementViewModel() });
				}
				else
				{
					//PushPage(new Pages.DisplayPage());
					DependencyService.Get<INavigate>(DependencyFetchTarget.GlobalInstance).StartNativeIntentOrActivity();
				}
			}
			else
			{
				DependencyService.Get<INavigate>(DependencyFetchTarget.GlobalInstance).StartNativeIntentOrActivity();
			}
		}

		private void HelpCommandExecuteAsync()
		{
			try
			{
				PopupNavigation.Instance.PushAsync(new ContentViews.CustomPopup(), true);
			}
			catch (Exception ex)
			{
				string exMessage = ex.Message;
			}
		}
		#endregion

	}
}
