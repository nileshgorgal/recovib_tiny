﻿using RecovibTiny.Interface;
using RecovibTiny.Model;
using System;
using System.Collections.Generic;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RecovibTiny
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class HomePage : ContentPage
	{
		public HomePage()
		{
			InitializeComponent();
			bool isConnected = DependencyService.Get<IUsbCommunication>(DependencyFetchTarget.GlobalInstance).IsDeviceConnected();
			lbltext.Text = isConnected == true ? "true" : "false";
		}

		private void TapGestureRecognizer_Tapped(object sender, EventArgs e)
		{
			lblBatteryLevel.Text = "Demo";
			List<DeviceInfo> deviceInfos = new List<DeviceInfo>();
			deviceInfos = DependencyService.Get<IReadUsbDevice>(DependencyFetchTarget.GlobalInstance).getBatteryLevel();

			foreach (DeviceInfo device in deviceInfos)
			{
				String batteryLevel_str = String.Format("%.0f%%", device._batteryLevel);
				lblBatteryLevel.Text = device._batteryLevel.ToString() + "\n FirmVersion : " + device._FirmwareVersion + "\n CalibDate : " + device._CalibDate + "\n Serial No :" + device._SerialNumber;
			}
		}

		private void Button_Clicked(object sender, EventArgs e)
		{
			bool isConnected = DependencyService.Get<IGetPermission>(DependencyFetchTarget.GlobalInstance).GetDeviceConnected();
			lbltext.Text = isConnected == true ? "true" : "false";
		}

		private void Method()
		{
		}
	}
}