﻿namespace RecovibTiny.Model
{
	public class DeviceInfo
	{
		public double _batteryLevel { get; set; }
		public string _SerialNumber { get; set; }
		public string _FirmwareVersion { get; set; }
		public string _CalibDate { get; set; }
	}
}
