﻿using RecovibTiny.Interface;
using Rg.Plugins.Popup.Pages;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RecovibTiny.ContentViews
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CustomPopup : PopupPage
	{
		public CustomPopup()
		{
			InitializeComponent();
		}

		private void Sync_buttonClicked(object sender, System.EventArgs e)
		{
			sync_button.IsEnabled = false;
			sync_button.Opacity = 0.3f;
			launch_button.IsEnabled = true;
			launch_button.Opacity = 1f;
		}

		private void Launch_buttonClicked(object sender, System.EventArgs e)
		{
			DependencyService.Get<IRestartApp>(DependencyFetchTarget.GlobalInstance).RestartApp();
		}
	}
}