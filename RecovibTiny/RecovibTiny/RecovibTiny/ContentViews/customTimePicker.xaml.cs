﻿using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RecovibTiny.ContentViews
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CustomTimePicker
	{
		private int IncresedValue = 3;
		private const int HoursValue = 1;
		private const int MinsValue = 2;
		private const int SecValue = 3;
		public CustomTimePicker()
		{
			InitializeComponent();
			HourFrame.BackgroundColor = Color.White;
			MinFrame.BackgroundColor = Color.White;
			SecFrame.BackgroundColor = Color.AliceBlue;
		}
		private void AddValue_Clicked(object sender, EventArgs e)
		{
			switch (IncresedValue)
			{
				case HoursValue:
					{
						int hours = int.Parse(hoursText.Text);
						if (hours != 12)
						{
							hoursText.Text = (hours + 1).ToString();
						}
					}
					break;
				case MinsValue:
					{
						int minute = int.Parse(MiniteText.Text);
						if (minute != 58)
						{
							MiniteText.Text = (minute + 1).ToString();
						}
					}
					break;
				case SecValue:
					{
						int sec = int.Parse(SecText.Text);
						if (sec != 58)
						{
							SecText.Text = (sec + 1).ToString();
						}
					}
					break;
			}
		}

		private void SubValue_Clicked(object sender, EventArgs e)
		{
			switch (IncresedValue)
			{
				case HoursValue:
					{
						int hours = int.Parse(hoursText.Text);
						if (hours != 0)
						{
							hoursText.Text = (hours - 1).ToString();
						}
					}
					break;
				case MinsValue:
					{
						int minute = int.Parse(MiniteText.Text);
						if (minute != 0)
						{
							MiniteText.Text = (minute - 1).ToString();
						}
					}
					break;
				case SecValue:
					{
						int sec = int.Parse(SecText.Text);
						if (sec != 0)
						{
							SecText.Text = (sec - 1).ToString();
						}
					}
					break;
			}
		}

		private void Hours_Clicked(object sender, EventArgs e)
		{
			HourFrame.BackgroundColor = Color.AliceBlue;
			MinFrame.BackgroundColor = Color.White;
			SecFrame.BackgroundColor = Color.White;
			IncresedValue = HoursValue;
		}
		private void Minute_Clicked(object sender, EventArgs e)
		{
			HourFrame.BackgroundColor = Color.White;
			MinFrame.BackgroundColor = Color.AliceBlue;
			SecFrame.BackgroundColor = Color.White;
			IncresedValue = MinsValue;
		}
		private void Sec_Clicked(object sender, EventArgs e)
		{
			HourFrame.BackgroundColor = Color.White;
			MinFrame.BackgroundColor = Color.White;
			SecFrame.BackgroundColor = Color.AliceBlue;
			IncresedValue = SecValue;
		}

		private void Manual_Clicked(object sender, EventArgs e)
		{
			if (Manual.Text.Equals("Select"))
			{
				Manual.Text = "Edit";
				HourFrame1.IsVisible = true;
				MinFrame1.IsVisible = true;
				SecFrame1.IsVisible = true;
				HourFrame.IsVisible = false;
				MinFrame.IsVisible = false;
				SecFrame.IsVisible = false;
				IncrementView.IsVisible = false;
			}
			else
			{
				Manual.Text = "Select";
				HourFrame.IsVisible = true;
				MinFrame.IsVisible = true;
				SecFrame.IsVisible = true;
				HourFrame1.IsVisible = false;
				MinFrame1.IsVisible = false;
				SecFrame1.IsVisible = false;
				IncrementView.IsVisible = true;
			}
		}

		private void Done_Clicked(object sender, EventArgs e)
		{
			String time = "";
			if (Manual.Text.Equals("Edit"))
			{
				if (int.Parse(hoursText1.Text) > 12)
				{
					DisplayAlert("Alert", "Please enter hours 12 or below", "OK");
					return;
				}
				else if (int.Parse(MiniteText1.Text) > 58)
				{
					DisplayAlert("Alert", "Please enter minute 58 or below", "OK");
					return;
				}
				else if (int.Parse(SecText1.Text) > 58)
				{
					DisplayAlert("Alert", "Please enter second 58 or below", "OK");
					return;
				}
				time = hoursText1.Text + ":" + MiniteText1.Text + ":" + SecText1.Text;
				MessagingCenter.Send<System.Object, string>(this, "TimePicker", time);
			}
			else
			{
				time = hoursText.Text + ":" + MiniteText.Text + ":" + SecText.Text;
				MessagingCenter.Send<System.Object, string>(this, "TimePicker", time);
			}

			PopupNavigation.Instance.PopAsync();
		}
		private void CANCEL_Clicked(object sender, EventArgs e)
		{
			PopupNavigation.Instance.PopAsync();
		}
	}
}