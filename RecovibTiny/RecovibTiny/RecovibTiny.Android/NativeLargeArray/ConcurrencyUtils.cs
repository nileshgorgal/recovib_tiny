﻿namespace RecovibTiny.Droid.NativeLargeArray
{
	class ConcurrencyUtils
	{
		///*private static ExecutorService THREAD_POOL = Executors.NewCachedThreadPool(new CustomThreadFactory(new CustomExceptionHandler()));

		//private static long THREADS_BEGIN_N_1D_FFT_2THREADS = 8192;

		//private static long THREADS_BEGIN_N_1D_FFT_4THREADS = 65536;

		//private static long THREADS_BEGIN_N_2D = 65536;

		//private static long LARGE_ARAYS_BEGIN_N = (1 << 28);

		//private static long THREADS_BEGIN_N_3D = 65536;

		//private static int NTHREADS = prevPow2(getNumberOfProcessors());

		//private ConcurrencyUtils()
		//{

		//}

		//private static class CustomExceptionHandler : Thread.IUncaughtExceptionHandler
		//{
		//	public static IntPtr Handle => throw new NotImplementedException();

		//	public static int JniIdentityHashCode => throw new NotImplementedException();

		//	public static JniObjectReference PeerReference => throw new NotImplementedException();

		//	public static JniPeerMembers JniPeerMembers => throw new NotImplementedException();

		//	public static JniManagedPeerStates JniManagedPeerState => throw new NotImplementedException();

		//	public static void Dispose()
		//	{
		//		throw new NotImplementedException();
		//	}

		//	public static void Disposed()
		//	{
		//		throw new NotImplementedException();
		//	}

		//	public static void DisposeUnlessReferenced()
		//	{
		//		throw new NotImplementedException();
		//	}

		//	public static void Finalized()
		//	{
		//		throw new NotImplementedException();
		//	}

		//	public static void SetJniIdentityHashCode(int value)
		//	{
		//		throw new NotImplementedException();
		//	}

		//	public static void SetJniManagedPeerState(JniManagedPeerStates value)
		//	{
		//		throw new NotImplementedException();
		//	}

		//	public static void SetPeerReference(JniObjectReference reference)
		//	{
		//		throw new NotImplementedException();
		//	}

		//	public static void uncaughtException(Thread t, Throwable e)
		//	{
		//		e.PrintStackTrace();
		//	}

		//	public static void UncaughtException(Thread t, Throwable e)
		//	{
		//		throw new NotImplementedException();
		//	}

		//	public static void UnregisterFromRuntime()
		//	{
		//		throw new NotImplementedException();
		//	}
		//}

		//private static class CustomThreadFactory : IThreadFactory
		//{

		//	private static IThreadFactory defaultFactory = Executors.defaultThreadFactory();

		//	public static IntPtr Handle => throw new NotImplementedException();

		//	public static int JniIdentityHashCode => throw new NotImplementedException();

		//	public static JniObjectReference PeerReference => throw new NotImplementedException();

		//	public static JniPeerMembers JniPeerMembers => throw new NotImplementedException();

		//	public static JniManagedPeerStates JniManagedPeerState => throw new NotImplementedException();

		//	private static Thread.IUncaughtExceptionHandler handler;

		//	private CustomThreadFactory(Thread.IUncaughtExceptionHandler handler)
		//	{
		//		this.handler = handler;
		//	}

		//	public static Thread NewThread(Runnable r)
		//	{
		//		Thread t = defaultFactory.NewThread(r);
		//		t.setUncaughtExceptionHandler(handler);
		//		return t;
		//	}

		//	public static Thread NewThread(IRunnable r)
		//	{
		//		throw new NotImplementedException();
		//	}

		//	public static void SetJniIdentityHashCode(int value)
		//	{
		//		throw new NotImplementedException();
		//	}

		//	public static void SetPeerReference(JniObjectReference reference)
		//	{
		//		throw new NotImplementedException();
		//	}

		//	public static void SetJniManagedPeerState(JniManagedPeerStates value)
		//	{
		//		throw new NotImplementedException();
		//	}

		//	public static void UnregisterFromRuntime()
		//	{
		//		throw new NotImplementedException();
		//	}

		//	public static void DisposeUnlessReferenced()
		//	{
		//		throw new NotImplementedException();
		//	}

		//	public static void Disposed()
		//	{
		//		throw new NotImplementedException();
		//	}

		//	public static void Finalized()
		//	{
		//		throw new NotImplementedException();
		//	}

		//	public static void Dispose()
		//	{
		//		throw new NotImplementedException();
		//	}
		//}
		///***/
		// * Returns the number of available processors.
		// *
		// * @return number of available processors
		// *//*
		//public static int getNumberOfProcessors()
		//{
		//	return Java.Lang.Runtime.GetRuntime().AvailableProcessors();
		//}

		///***/
		// * Returns the current number of threads.
		// *
		// * @return the current number of threads.
		// *//*
		//public static int getNumberOfThreads()
		//{
		//	return NTHREADS;
		//}

		///***/
		// * Sets the number of threads. If n is not a power-of-two number, then the
		// * number of threads is set to the closest power-of-two number less than n.
		// *
		// * @param n number of threads
		// *//*
		//public static void setNumberOfThreads(int n)
		//{
		//	NTHREADS = prevPow2(n);
		//}

		///***/
		// * Returns the minimal size of 1D data for which two threads are used.
		// *
		// * @return the minimal size of 1D data for which two threads are used
		// *//*
		//public static long getThreadsBeginN_1D_FFT_2Threads()
		//{
		//	return THREADS_BEGIN_N_1D_FFT_2THREADS;
		//}

		///***/
		// * Returns the minimal size of 1D data for which four threads are used.
		// *
		// * @return the minimal size of 1D data for which four threads are used
		// *//*
		//public static long getThreadsBeginN_1D_FFT_4Threads()
		//{
		//	return THREADS_BEGIN_N_1D_FFT_4THREADS;
		//}

		///***/
		// * Returns the minimal size of 2D data for which threads are used.
		// *
		// * @return the minimal size of 2D data for which threads are used
		// *//*
		//public static long getThreadsBeginN_2D()
		//{
		//	return THREADS_BEGIN_N_2D;
		//}

		///***/
		// * Returns the minimal size of 3D data for which threads are used.
		// *
		// * @return the minimal size of 3D data for which threads are used
		// *//*
		//public static long getThreadsBeginN_3D()
		//{
		//	return THREADS_BEGIN_N_3D;
		//}

		///***/
		// * Returns the minimal size for which JLargeArrays are used.
		// *
		// * @return the minimal size for which JLargeArrays are used
		// *//*
		//public static long getLargeArraysBeginN()
		//{
		//	return LARGE_ARAYS_BEGIN_N;
		//}

		///***/
		// * Sets the minimal size of 1D data for which two threads are used.
		// *
		// * @param n the minimal size of 1D data for which two threads are used
		// *//*
		//public static void setThreadsBeginN_1D_FFT_2Threads(long n)
		//{
		//	if (n < 1024)
		//	{
		//		THREADS_BEGIN_N_1D_FFT_2THREADS = 1024;
		//	}
		//	else
		//	{
		//		THREADS_BEGIN_N_1D_FFT_2THREADS = n;
		//	}
		//}

		///***/
		// * Sets the minimal size of 1D data for which four threads are used.
		// *
		// * @param n the minimal size of 1D data for which four threads are used
		// *//*
		//public static void setThreadsBeginN_1D_FFT_4Threads(long n)
		//{
		//	if (n < 1024)
		//	{
		//		THREADS_BEGIN_N_1D_FFT_4THREADS = 1024;
		//	}
		//	else
		//	{
		//		THREADS_BEGIN_N_1D_FFT_4THREADS = n;
		//	}
		//}

		///***/
		// * Sets the minimal size of 2D data for which threads are used.
		// *
		// * @param n the minimal size of 2D data for which threads are used
		// *//*
		//public static void setThreadsBeginN_2D(long n)
		//{
		//	if (n < 4096)
		//	{
		//		THREADS_BEGIN_N_2D = 4096;
		//	}
		//	else
		//	{
		//		THREADS_BEGIN_N_2D = n;
		//	}
		//}

		///***/
		// * Sets the minimal size of 3D data for which threads are used.
		// *
		// * @param n the minimal size of 3D data for which threads are used
		// *//*
		//public static void setThreadsBeginN_3D(long n)
		//{
		//	THREADS_BEGIN_N_3D = n;
		//}

		///***/
		// * Resets the minimal size of 1D data for which two and four threads are
		// * used.
		// *//*
		//public static void resetThreadsBeginN_FFT()
		//{
		//	THREADS_BEGIN_N_1D_FFT_2THREADS = 8192;
		//	THREADS_BEGIN_N_1D_FFT_4THREADS = 65536;
		//}

		///***/
		// * Resets the minimal size of 2D and 3D data for which threads are used.
		// *//*
		//public static void resetThreadsBeginN()
		//{
		//	THREADS_BEGIN_N_2D = 65536;
		//	THREADS_BEGIN_N_3D = 65536;
		//}

		///***/
		// * Sets the minimal size for which JLargeArrays are used.
		// *
		// * @param n the maximal size for which JLargeArrays are used
		// *//*
		//public static void setLargeArraysBeginN(long n)
		//{
		//	if (n < 1)
		//	{
		//		LARGE_ARAYS_BEGIN_N = 1;
		//	}
		//	else if (n > (1 << 28))
		//	{
		//		LARGE_ARAYS_BEGIN_N = (1 << 28);
		//	}
		//	else
		//	{
		//		LARGE_ARAYS_BEGIN_N = n;
		//	}
		//}

		///***/
		// * Returns the closest power-of-two number greater than or equal to x.
		// *
		// * @param x input value
		// * @return the closest power-of-two number greater than or equal to x
		// *//*
		//public static int nextPow2(int x)
		//{
		//	if (x < 1)
		//	{
		//		throw new IllegalArgumentException("x must be greater or equal 1");
		//	}
		//	if ((x & (x - 1)) == 0)
		//	{
		//		return x; // x is already a power-of-two number 
		//	}
		//	x |= x >> 1;
		//	x |= (x >> 2);
		//	x |= (x >> 4);
		//	x |= (x >> 8);
		//	x |= (x >> 16);
		//	return x + 1;
		//}

		///***/
		// * Returns the closest power-of-two number greater than or equal to x.
		// *
		// * @param x input value
		// * @return the closest power-of-two number greater than or equal to x
		// *//*
		//public static long nextPow2(long x)
		//{
		//	if (x < 1)
		//	{
		//		throw new IllegalArgumentException("x must be greater or equal 1");
		//	}
		//	if ((x & (x - 1l)) == 0)
		//	{
		//		return x; // x is already a power-of-two number 
		//	}
		//	x |= (x >> (int) 1l);
		//	x |= (x >> (int)2l);
		//	x |= (x >> (int)4l);
		//	x |= (x >> (int)8l);
		//	x |= (x >> (int)16l);
		//	x |= (x >> (int)32l);
		//	return x + 1l;
		//}

		///***/
		// * Returns the closest power-of-two number less than or equal to x.
		// *
		// * @param x input value
		// * @return the closest power-of-two number less then or equal to x
		// *//*
		//public static int prevPow2(int x)
		//{
		//	if (x < 1)
		//	{
		//		throw new IllegalArgumentException("x must be greater or equal 1");
		//	}
		//	return (int)Math.Pow(2, Math.Floor(Math.Log(x) / Math.Log(2)));
		//}

		///***/
		// * Returns the closest power-of-two number less than or equal to x.
		// *
		// * @param x input value 
		// * @return the closest power-of-two number less then or equal to x
		// *//*
		//public static long prevPow2(long x)
		//{
		//	if (x < 1)
		//	{
		//		throw new IllegalArgumentException("x must be greater or equal 1");
		//	}
		//	return (long)Java.Lang.Math.Pow(2, Math.Floor(Math.Log(x) / Math.Log(2)));
		//}

		///***/
		// * Checks if x is a power-of-two number.
		// *
		// * @param x input value
		// * @return true if x is a power-of-two number
		// *//*
		//public static bool isPowerOf2(int x)
		//{
		//	if (x <= 0)
		//	{
		//		return false;
		//	}
		//	else
		//	{
		//		return (x & (x - 1)) == 0;
		//	}
		//}

		///***/
		// * Checks if x is a power-of-two number.
		// *
		// * @param x input value
		// * @return true if x is a power-of-two number
		// *//*
		//public static bool isPowerOf2(long x)
		//{
		//	if (x <= 0)
		//	{
		//		return false;
		//	}
		//	else
		//	{
		//		return (x & (x - 1l)) == 0;
		//	}
		//}

		///***/
		// * Causes the currently executing thread to sleep (temporarily cease
		// * execution) for the specified number of milliseconds.
		// *
		// * @param millis the length of time to sleep in milliseconds
		// *//*
		//public static void sleep(long millis)
		//{
		//	try
		//	{
		//		Thread.Sleep(5000);
		//	}
		//	catch (InterruptedException e)
		//	{
		//		e.PrintStackTrace();
		//	}
		//}

		///***/
		// * Submits a Runnable task for execution and returns a Future representing
		// * that task.
		// *
		// * @param task a Runnable task for execution
		// * @return a Future representing the task
		// *//*
		//public static IFuture<?> submit(Runnable task)
		//{
		//	return THREAD_POOL.submit(task);
		//}

		///***/
		// * Waits for all threads to complete computation.
		// *
		// * @param futures array of Future objects 
		// *//*
		//public static void waitForCompletion(IFuture<?>[] futures)
		//{
		//	int size = futures.Length;
		//	try
		//	{
		//		for (int j = 0; j < size; j++)
		//		{
		//			futures[j].get();
		//		}
		//	}
		//	catch (ExecutionException ex)
		//	{
		//		ex.PrintStackTrace();
		//	}
		//	catch (InterruptedException e)
		//	{
		//		e.PrintStackTrace();
		//	}
		//}
	}
}