﻿using static RecovibTiny.Helper.Constant;

namespace RecovibTiny.Droid.NativeLargeArray
{
	public class FloatLargeArray : LargeArray
	{
		private readonly float[] data;
		public FloatLargeArray(float[] data)
		{
			this.type = LargeArrayType.FLOAT;
			this.Sizeof = 4;
			this.length = data.Length;
			this.data = data;
			this.ptr = this.length * this.Sizeof;
		}

		public override float getFloat(long i)
		{
			if (ptr != 0)
			{
				return getFloat(ptr + Sizeof * i);
			}
			else
			{
				return data[(int)i];
			}
		}
	}
}