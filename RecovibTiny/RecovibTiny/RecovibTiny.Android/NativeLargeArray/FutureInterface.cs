﻿using Java.Lang;
using Exception = Java.Lang.Exception;

namespace RecovibTiny.Droid.NativeLargeArray
{
	public interface FutureInterface<V>
	{
		bool cancel(bool var1);

		bool isCancelled();

		bool isDone();

		/*V get() Throws 
			ExecutionException, InterruptedException;

		V get(long var1, TimeUnit var3) Throws ExecutionException, InterruptedException, TimeoutException;*/
	}

	public class ExecutionException : Exception
	{
		protected ExecutionException()
		{
			throw new RuntimeException("Stub!");
		}

		protected ExecutionException(Java.Lang.String message)
		{
			throw new RuntimeException("Stub!");
		}

		public ExecutionException(string message, Throwable cause)
		{
			throw new RuntimeException("Stub!");
		}

		public ExecutionException(Throwable cause)
		{
			throw new RuntimeException("Stub!");
		}
	}
}