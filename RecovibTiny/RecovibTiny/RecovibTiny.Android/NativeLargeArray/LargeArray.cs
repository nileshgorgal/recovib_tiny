﻿using Java.Lang;
using static RecovibTiny.Helper.Constant;

namespace RecovibTiny.Droid.NativeLargeArray
{
	public abstract class LargeArray
	{
		protected LargeArrayType type;
		protected long length;
		protected long Sizeof;
		protected long ptr = 0;
		public abstract float getFloat(long i);

		public float getFloat_safe(long i)
		{
			if (i < 0 || i >= length)
			{
				throw new ArrayIndexOutOfBoundsException(Long.ToString(i));
			}
			return getFloat(i);
		}

	}

}