﻿using Android;
using Android.App;
using Android.Content;
using Android.Graphics.Drawables;
using Android.Hardware.Usb;
using Android.Net;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.App;
using Android.Support.V4.Provider;
using Android.Text.Method;
using Android.Util;
using Android.Views;
using Android.Views.Animations;
using Android.Widget;
using AndroidX.Core.Content.Resources;
using Java.IO;
using Java.Lang;
using Java.Text;
using Java.Util;
using RecovibTiny.Droid.Helper;
using RecovibTiny.Droid.NativeLargeArray;
using RecovibTiny.Helper;
using SciChart.Charting.Model;
using SciChart.Charting.Model.DataSeries;
using SciChart.Charting.Modifiers;
using SciChart.Charting.Visuals;
using SciChart.Charting.Visuals.Animations;
using SciChart.Charting.Visuals.Annotations;
using SciChart.Charting.Visuals.Axes;
using SciChart.Charting.Visuals.PointMarkers;
using SciChart.Charting.Visuals.RenderableSeries;
using SciChart.Core.Model;
using SciChart.Data.Model;
using SciChart.Drawing.Common;
using System.Drawing;
using System.Linq;
using static RecovibTiny.Droid.Helper.AnalysisClass;
using Exception = Java.Lang.Exception;
using Intent = Android.Content.Intent;
using Math = Java.Lang.Math;

namespace RecovibTiny.Droid.ScichartActivities
{
	[Activity(Label = "Recovib Tiny", Theme = "@style/MainTheme")]
	public class DisplayActivity : FragmentActivity, SeekBar.IOnSeekBarChangeListener, IDialogInterfaceOnCancelListener, View.IOnClickListener
	{
		#region Variables
		//Randomly defined codes
		private const int MY_PERMISSIONS_REQUEST_EXTERNAL_STORAGE = 1;
		readonly string[] PERMISSIONS = { Manifest.Permission.WriteExternalStorage };
		readonly int PERMISSION_ALL = 1;
		private readonly string TAG = "RECOVIB.FEEL";
		private readonly string name = "DisplayActivity";
		//Constants
		private const double SQRT_2 = 1.4142135623730950488016887242097d;
		private const double BANDPASS_WARNING_THRESHOLD = 5.0d;
		private const double HP_FILTER_FC_MIN = 0.5d;
		private const double HP_FILTER_FC_MAX = 250d;
		private const double LP_FILTER_FC_MIN = 0.5d;
		private const double LP_FILTER_FC_MAX = 250d;
		private const double VEL_FILTER_FC_MIN = 0.5d;
		private const double VEL_FILTER_FC_MAX = 10.0d;
		private const double DISP_FILTER_FC_MIN = 0.5d;
		private const double DISP_FILTER_FC_MAX = 10.0d;
		private const double VISIBLE_RANGE_MIN = 0.001;
		private const double VISIBLE_RANGE_MAX = 1000;
		private const int DATA_RANGE_MIN = 1;
		private const int DATA_RANGE_MAX = 3600;
		private const int SKIP_NEXT_MIN = 1;
		private const int SKIP_NEXT_MAX = 7800;
		private const int FFT_SIZE_MIN = 64;
		private const int FFT_SIZE_MAX = 4096;
		private const int FIFO_AV_MIN = 1;
		private const int FIFO_AV_MAX = 50;
		//interface
		public static ImageButton startButton;
		public static ImageButton stopButton;
		public static ImageButton recordButton;
		public static ImageButton ffButton;
		public static ImageButton skipNextButton;
		public static ImageButton buttonScale;
		public static ImageButton buttonFS;
		public static LinearLayout controlsLayout;
		public static LinearLayout rmsLayout;
		public static Button buttonT;
		public static Button buttonF;

		private readonly Button dialogButtonRec;
		private Button dialogButton;
		public static TextView ERMSx;
		public static TextView ERMSy;
		public static TextView ERMSz;
		public static TextView TitleRMS;
		//Drawable for buttons to allow changes (play/pause and replay/stop)
		public static Drawable pauseDrawable;
		public static Drawable startDrawable;
		public static Drawable replayDrawable;
		public static Drawable stopDrawable;
		public static Drawable ffDrawable;
		public static Drawable snDrawable;
		//SciChartSurface elements
		public static SciChartSurface chartSurface_T;
		public static SciChartSurface chartSurface_F;
		public static FastLineRenderableSeries Flrs_Ax;
		public static FastLineRenderableSeries Flrs_Ay;
		public static FastLineRenderableSeries Flrs_Az;
		public static FastLineRenderableSeries Flrs_Fft_Ax;
		public static FastLineRenderableSeries Flrs_Fft_Ay;
		public static FastLineRenderableSeries Flrs_Fft_Az;
		public static XyDataSeries dataSeriesAx;
		public static XyDataSeries dataSeriesAy;
		public static XyDataSeries dataSeriesAz;
		public static XyDataSeries dataSeriesFFTAx;
		public static XyDataSeries dataSeriesFFTAy;
		public static XyDataSeries dataSeriesFFTAz;
		public static NumericAxis xAxisT, yAxisT, xAxisF, yAxisF;
		//Fifo Buffers to handle data before treatment
		public static LongValues FIFOticksValues;
		public static FloatValues FIFOaxValues;
		public static FloatValues FIFOayValues;
		public static FloatValues FIFOazValues;
		//SciChartSurface time-domain buffers
		public static FloatValues ticksValues;
		public static FloatValues axValues;
		public static FloatValues ayValues;
		public static FloatValues azValues;
		//SciChartSurface frequency-domain buffers
		public static FloatValues faxValues;
		public static FloatValues fayValues;
		public static FloatValues fazValues;
		//SciChartSurface RMS-Computing buffers
		public static FloatValues RMSaxValues;
		public static FloatValues RMSayValues;
		public static FloatValues RMSazValues;
		//copy buffers
		public static FloatValues ticksValuesCopy;
		public static FloatValues axValuesCopy;
		public static FloatValues ayValuesCopy;
		public static FloatValues azValuesCopy;
		//fft buffers
		public static FloatValues fftFreq;
		public static FloatValues fftax;
		public static FloatValues fftay;
		public static FloatValues fftaz;
		//fft copy buffers
		public static FloatValues fftFreqCopy;
		public static FloatValues fftaxCopy;
		public static FloatValues fftayCopy;
		public static FloatValues fftazCopy;
		//Recording
		//Flag set to define a different onClick action
		public static bool replayFlag = false;
		//Flag set when streaming and replaying (start and pause)
		private bool flagStartActivate = false;
		//Dialogs
		/*private InfoDialog infoDial = new infoDialog();
		private pickDialog pickDial = new pickDialog();
		private numDialog numDial = new numDialog();
		private licenseDialog licenseDial = new licenseDialog();
		private aboutDialog about = new aboutDialog();
		public ViewDialog alert = new ViewDialog();
		public globalDialog globalParam = new globalDialog();*/
		//Checkboxes of the viewDialog
		//Values to Display
		private CheckBox Ax_cb;
		private CheckBox Ay_cb;
		private CheckBox Az_cb;
		//Filter to apply or not
		private CheckBox hpfilterCb;
		private double local_hp_fc;
		private CheckBox lpfilterCb;
		private double local_lp_fc;
		private RadioButton accelerationRadio;
		private RadioButton velocityRadio;
		private double local_velocity_fc;
		private RadioButton displacementRadio;
		private double local_displacement_fc;
		//Window Funtions
		private RadioButton rectangular_radio;
		private RadioButton hanning_radio;
		private RadioButton flattop_radio;
		//FFT base Signal analysis
		private RadioButton a_s_q_p_radio;
		private RadioButton a_s_rms_radio;
		private RadioButton a_s_d_radio;
		private RadioButton p_s_radio;
		private RadioButton p_s_d_radio;
		//Auto-scale to apply or not
		private RadioButton no_scaling;
		private RadioButton chart_scaling;
		//Time/frequency domain switch
		private Switch tfSwitch;
		//Range radio button
		private RadioButton sixg_radio;
		private RadioButton twog_radio;
		//Units radio button
		private RadioButton ms2units_radio;
		private RadioButton gunits_radio;

		private float minimumExponentialRms = .1f;
		private int minimumRecording = 1;
		//FontStyle used for axis labels
		private SciChart.Drawing.Common.FontStyle fontStyle_axis;
		public float convertDpToPixel(float dp)
		{
			return (int)System.Math.Round(dp * (this.Resources.DisplayMetrics.Xdpi / (float)DisplayMetricsDensity.Default));
			//return dp * ((float)this.getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT);
		}
		//Flag to signal that a recording is underway
		private bool recordFlag = false;

		private static Context mContext;
		public static string toastString_eof;
		public static string toastString_rtl;
		public static string toastString_rtlm;
		public static string toastString_fifo;
		private View myRecordView;
		private Toast toastff = null;
		public static bool manuallyOpened = false;
		private UsbManager usbManager;
		private UsbDeviceConnection connection;
		private UsbInterface usbInterface;
		private UsbDevice device;
		private UsbEndpoint endPointWrite;
		private UsbEndpoint endPointRead;
		private int packetSize;

		public static Date DT_start;
		public string DT_start_string;
		private File folder;
		private File snFolder;
		private File dtFolder;
		public static File viewerFolder;
		public static File binfile;
		public static FileOutputStream fosout;
		public static FileInputStream istToRead;
		public static File bin_file;
		private int ScreenWidth = 500;
		private int ScreenHeight = 500;
		readonly int vendorID = 8263;//0x2047
		readonly int productIDTiny = 2379;//0x094B
		readonly int productIDFeel = 2378;//0x094B
		private TextView editTau;
		//Conversion parameters
		private static AnalysisClass HidCom;
		private FrameLayout lytToolBar;
		private ImageButton btnSettings, btnInfo;
		#endregion Variables
		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			SetContentView(Resource.Layout.layout_display_activity);
			mContext = this;
			ContextBean.SetLocalContext(mContext);
			ScreenWidth = Resources.DisplayMetrics.WidthPixels;
			ScreenHeight = Resources.DisplayMetrics.HeightPixels;
			InitUI();
			InitScichart();
		}
		public void InitUI()
		{
			Window.AddFlags(WindowManagerFlags.KeepScreenOn);
			if (!manuallyOpened)
			{
				Intent intent = this.Intent;
				device = (UsbDevice)intent.GetParcelableExtra(UsbManager.ExtraDevice);
				usbManager = (UsbManager)this.GetSystemService(Context.UsbService);
				foreach (UsbDevice usbDevice in usbManager.DeviceList.Values)
				{
					if ((usbDevice.VendorId == vendorID) && (usbDevice.ProductId == productIDTiny))
					{
						device = usbDevice;
						usbInterface = device.GetInterface(1);
					}
					else if ((usbDevice.VendorId == vendorID) && (usbDevice.ProductId == productIDFeel))
					{
						device = usbDevice;
						usbInterface = device.GetInterface(0);
					}
				}
				if (device != null)
				{
					connection = usbManager.OpenDevice(device);
					usbInterface = device.GetInterface(0);
					if (null == connection)
					{
						Log.Debug(TAG, "unable to connect");
					}
					else
					{
						connection.ClaimInterface(usbInterface, true);            //Device connected - claim ownership over the interface
					}
					try
					{
						//Direction of end point 1 - OUT - from host to device
						if (UsbConstant.UsbDirOut == usbInterface.GetEndpoint(1).Direction)
						{
							endPointWrite = usbInterface.GetEndpoint(1);
						}
					}
					catch (Exception e)
					{
						Log.Error(TAG, "Device have no endPointWrite", e);
					}
					try
					{
						//Direction of end point 0 - IN - from device to host
						if (0 != usbInterface.GetEndpoint(0).Direction)
						{
							endPointRead = usbInterface.GetEndpoint(0);
							packetSize = endPointRead.MaxPacketSize;
						}
					}
					catch (Exception e)
					{
						Log.Error(TAG, "Device have no endPointRead", e);
					}
					HidCom = new AnalysisClass(endPointWrite, endPointRead, device, usbManager, connection, packetSize);
				}
				ViewerMode(false);
			}
			//SciChart
			chartSurface_T = (SciChartSurface)FindViewById(Resource.Id.chartT);
			chartSurface_F = (SciChartSurface)FindViewById(Resource.Id.chartF);
			toastString_eof = GetString(Resource.String.end_of_file_reached);
			toastString_fifo = GetString(Resource.String.fifo_full);
			toastString_rtl = GetString(Resource.String.time_limit_exceeded);
			toastString_rtlm = GetString(Resource.String.minute);
			//Drawables definitions for buttons whose icons can possibly change (play/pause, stop/replay)
			pauseDrawable = ResourcesCompat.GetDrawable(this.Resources, Resource.Drawable.ic_pause_white_36dp, null);
			startDrawable = ResourcesCompat.GetDrawable(this.Resources, Resource.Drawable.ic_play_arrow_white_36dp, null);
			replayDrawable = ResourcesCompat.GetDrawable(this.Resources, Resource.Drawable.ic_replay_white_36dp, null);
			stopDrawable = ResourcesCompat.GetDrawable(this.Resources, Resource.Drawable.ic_stop_white_36dp, null);
			snDrawable = ResourcesCompat.GetDrawable(this.Resources, Resource.Drawable.ic_skip_next_white_36dp, null);
			ffDrawable = ResourcesCompat.GetDrawable(this.Resources, Resource.Drawable.ic_fast_forward_white_36dp, null);

			startButton = FindViewById<ImageButton>(Resource.Id.buttonStart);
			stopButton = FindViewById<ImageButton>(Resource.Id.buttonStop);
			recordButton = FindViewById<ImageButton>(Resource.Id.recordButton);
			ffButton = FindViewById<ImageButton>(Resource.Id.fastForwardButton);
			skipNextButton = FindViewById<ImageButton>(Resource.Id.skipNextButton);
			buttonScale = FindViewById<ImageButton>(Resource.Id.buttonScale);
			buttonFS = FindViewById<ImageButton>(Resource.Id.buttonFS);
			buttonT = FindViewById<Button>(Resource.Id.buttonT);
			buttonF = FindViewById<Button>(Resource.Id.buttonF);
			controlsLayout = FindViewById<LinearLayout>(Resource.Id.controlsLayout);
			rmsLayout = FindViewById<LinearLayout>(Resource.Id.rmsLayout);
			TitleRMS = FindViewById<TextView>(Resource.Id.titleRms);
			ERMSx = FindViewById<TextView>(Resource.Id.ERMSx);
			ERMSy = FindViewById<TextView>(Resource.Id.ERMSy);
			ERMSz = FindViewById<TextView>(Resource.Id.ERMSz);

			btnSettings = FindViewById<ImageButton>(Resource.Id.btnSettings);
			btnInfo = FindViewById<ImageButton>(Resource.Id.btnInfo);
			btnSettings.SetOnClickListener(this);
			btnInfo.SetOnClickListener(this);
			chartSurface_T.Theme = Resource.Style.SciChart_Bright_Spark;
			chartSurface_F.Theme = Resource.Style.SciChart_Bright_Spark;
			//FrameLayout
			lytToolBar = FindViewById<FrameLayout>(Resource.Id.lytToolBar);

			buttonT.SetOnClickListener(this);
			buttonF.SetOnClickListener(this);
			buttonScale.SetOnClickListener(this);
			buttonFS.SetOnClickListener(this);
			ffButton.SetOnClickListener(this);
			skipNextButton.SetOnClickListener(this);
			recordButton.SetOnClickListener(this);
			startButton.SetOnClickListener(this);
			stopButton.SetOnClickListener(this);
		}
		// Added scichart setup code
		private void InitScichart()
		{
			#region Start
			if (HidCom.Get_input_Choice() != INPUT_CHOICE.RECORDED_VALUES)
			{
				startButton.Visibility = ViewStates.Visible;
				stopButton.Visibility = ViewStates.Visible;
				recordButton.Visibility = ViewStates.Visible;
				ffButton.Visibility = ViewStates.Gone;
				skipNextButton.Visibility = ViewStates.Gone;
				DisableImButton(stopButton);
				DisableImButton(recordButton);
			}
			else
			{
				startButton.Visibility = ViewStates.Visible;
				stopButton.Visibility = ViewStates.Visible;
				recordButton.Visibility = ViewStates.Gone;
				ffButton.Visibility = ViewStates.Visible;
				skipNextButton.Visibility = ViewStates.Visible;
				DisableImButton(stopButton);
				DisableImButton(skipNextButton);
				DisableImButton(ffButton);
			}

			HidCom.Setcontext(mContext);
			//m/s2 or g
			HidCom.SetgUnits(true);
			//The sampling frequency used (Should not be set to another value)
			HidCom.SetSampling_Freq(1024);
			//Default speed for file reading
			HidCom.SetSpeedMult(1);
			//Flag for recording (false at launch)
			HidCom.SetRecordTemp(false);

			//Retrieve conversion parameters (Gains and offsets) if streamer mode selected
			if (HidCom.Get_input_Choice() != INPUT_CHOICE.RECORDED_VALUES)
			{
				HidCom.Ext_Event_Get_Params();
			}

			//Buffers
			FIFOticksValues = new LongValues();
			FIFOazValues = new FloatValues();
			FIFOayValues = new FloatValues();
			FIFOaxValues = new FloatValues();

			ticksValues = new FloatValues();
			azValues = new FloatValues();
			ayValues = new FloatValues();
			axValues = new FloatValues();

			fazValues = new FloatValues();
			fayValues = new FloatValues();
			faxValues = new FloatValues();

			RMSazValues = new FloatValues();
			RMSayValues = new FloatValues();
			RMSaxValues = new FloatValues();

			//Copy buffers
			ticksValuesCopy = new FloatValues();
			azValuesCopy = new FloatValues();
			ayValuesCopy = new FloatValues();
			axValuesCopy = new FloatValues();
			//fft buffers
			fftFreq = new FloatValues();
			fftax = new FloatValues();
			fftay = new FloatValues();
			fftaz = new FloatValues();
			//fft copy buffers
			fftFreqCopy = new FloatValues();
			fftaxCopy = new FloatValues();
			fftayCopy = new FloatValues();
			fftazCopy = new FloatValues();

			//fontStyle_axis = new SciChart.Drawing.Common.FontStyle(convertDpToPixel(16), Resource.Color.black);
			fontStyle_axis = new SciChart.Drawing.Common.FontStyle(convertDpToPixel(16), Color.Black);

			xAxisT = new NumericAxis(this)
			{
				//GrowBy = new DoubleRange(0, 0),
				AutoRange = AutoRange.Always,
				DrawMajorBands = false,
				TitleStyle = fontStyle_axis,
				CursorTextFormatting = "0.####",
				AxisTitle = Resources.GetString(Resource.String.x_label_time)
			};
			yAxisT = new NumericAxis(this)
			{
				//GrowBy = new DoubleRange(0, 0),
				AutoRange = AutoRange.Always,
				VisibleRangeLimit = new DoubleRange(double.NegativeInfinity, double.PositiveInfinity),
				DrawMajorBands = false,
				TitleStyle = fontStyle_axis,
				TextFormatting = "0.#####E0",
				CursorTextFormatting = "0.#####E0",
				AxisAlignment = AxisAlignment.Left
			};
			xAxisF = new NumericAxis(this)
			{
				//GrowBy = new DoubleRange(0, 0),
				AutoRange = AutoRange.Always,
				DrawMajorBands = false,
				TitleStyle = fontStyle_axis,
				CursorTextFormatting = "0.####",
				AxisTitle = Resources.GetString(Resource.String.x_label_freq)
			};
			yAxisF = new NumericAxis(this)
			{
				//GrowBy = new DoubleRange(0, 0),
				AutoRange = AutoRange.Always,
				VisibleRangeLimit = new DoubleRange(double.NegativeInfinity, double.PositiveInfinity),
				DrawMajorBands = false,
				TitleStyle = fontStyle_axis,
				TextFormatting = "0.#####E0",
				CursorTextFormatting = "0.#####E0",
				AxisAlignment = AxisAlignment.Left
			};
			//Dashed lines for grid lines
			PenStyle AxisMajorGridStyle = new SolidPenStyle(Color.Gray, 1f, true, new float[] { 5, 5 });
			xAxisT.MajorGridLineStyle = AxisMajorGridStyle;
			yAxisT.MajorGridLineStyle = AxisMajorGridStyle;
			xAxisF.MajorGridLineStyle = AxisMajorGridStyle;
			yAxisF.MajorGridLineStyle = AxisMajorGridStyle;

			chartSurface_T.XAxes.Add(xAxisT);
			chartSurface_T.YAxes.Add(yAxisT);
			chartSurface_F.XAxes.Add(xAxisF);
			chartSurface_F.YAxes.Add(yAxisF);

			//Default modifiers (pinch zooming + panning)
			ModifierGroup chartModifiersT = new ModifierGroup();
			//Cursor Modifier
			RolloverModifier rolloverModifierT = new RolloverModifier
			{
				ShowTooltip = true,
				ShowAxisLabels = true,
				DrawVerticalLine = true,
				UseInterpolation = false,
				SourceMode = SourceMode.AllSeries
			};

			//Default modifiers (pinch zooming + panning)
			ModifierGroup chartModifiersF = new ModifierGroup();
			//Cursor Modifier
			RolloverModifier rolloverModifierF = new RolloverModifier
			{
				ShowTooltip = true,
				ShowAxisLabels = true,
				DrawVerticalLine = true,
				UseInterpolation = false,
				SourceMode = SourceMode.AllSeries
			};

			chartSurface_T.ChartModifiers.Add(chartModifiersT);
			chartSurface_T.ChartModifiers.Add(rolloverModifierT);
			chartSurface_F.ChartModifiers.Add(chartModifiersF);
			chartSurface_F.ChartModifiers.Add(rolloverModifierF);

			dataSeriesAx = new XyDataSeries<float, float>() { SeriesName = "Ax" };
			dataSeriesAy = new XyDataSeries<float, float>() { SeriesName = "Ay" };
			dataSeriesAz = new XyDataSeries<float, float>() { SeriesName = "Az" };
			dataSeriesFFTAx = new XyDataSeries<float, float>() { SeriesName = "FFT Ax" };
			dataSeriesFFTAy = new XyDataSeries<float, float>() { SeriesName = "FFT Ay" };
			dataSeriesFFTAz = new XyDataSeries<float, float>() { SeriesName = "FFT Az" };

			//FastLineRenderableSeries linked to the corresponding XyDataSeries
			Flrs_Ax = new FastLineRenderableSeries()
			{
				StrokeStyle = new SolidPenStyle(Color.DarkBlue, 3f, true, null),
				DataSeries = dataSeriesAx
			};
			Flrs_Ay = new FastLineRenderableSeries()
			{
				StrokeStyle = new SolidPenStyle(Color.DarkGreen, 3f, true, null),
				DataSeries = dataSeriesAy
			};
			Flrs_Az = new FastLineRenderableSeries()
			{
				StrokeStyle = new SolidPenStyle(Color.DarkRed, 3f, true, null),
				DataSeries = dataSeriesAz
			};
			Flrs_Fft_Ax = new FastLineRenderableSeries()
			{
				StrokeStyle = new SolidPenStyle(Color.DarkBlue, 3f, true, null),
				DataSeries = dataSeriesFFTAx
			};
			Flrs_Fft_Ay = new FastLineRenderableSeries()
			{
				StrokeStyle = new SolidPenStyle(Color.DarkGreen, 3f, true, null),
				DataSeries = dataSeriesFFTAy
			};
			Flrs_Fft_Az = new FastLineRenderableSeries()
			{
				StrokeStyle = new SolidPenStyle(Color.DarkRed, 3f, true, null),
				DataSeries = dataSeriesFFTAz
			};

			ContextBean.SetLocalContext(this);

			if (!GlobalPreferences.g_2 && !GlobalPreferences.g_6 && (HidCom.Get_input_Choice() == INPUT_CHOICE.REAL_VALUES))
			{
				HidCom.Set_recordingTimeLimit(10);
				GlobalPreferences.recordlimitinput = 10;
				GlobalPreferences.Ax_cb = true;
				GlobalPreferences.Ay_cb = true;
				GlobalPreferences.Az_cb = true;
				GlobalPreferences.T_switch = true;
				GlobalPreferences.F_switch = true;

				//Entering this condition means that the user enters the app for the first time
				SettingsDialog();
			}
			else if (!GlobalPreferences.gunits && !GlobalPreferences.ms2units && (HidCom.Get_input_Choice() == INPUT_CHOICE.RECORDED_VALUES))
			{
				HidCom.Set_recordingTimeLimit(10);
				GlobalPreferences.recordlimitinput = 10;
				GlobalPreferences.Ax_cb = true;
				GlobalPreferences.Ay_cb = true;
				GlobalPreferences.Az_cb = true;
				GlobalPreferences.T_switch = true;
				GlobalPreferences.F_switch = true;

				//Entering this condition means that the user enters the app for the first time
				SettingsDialog();
			}
			//If app already opened once, retrieve the last config
			else
			{
				//retrieve the last recording config
				HidCom.Set_recordingTimeLimit(GlobalPreferences.recordlimitinput);
				//Retrieve the last parameters set by the user
				//[g]/[m/s²]
				if (GlobalPreferences.gunits)
				{
					HidCom.SetgUnits(true);
				}
				else
				{
					HidCom.SetgUnits(false);
				}
				//Filter
				if (GlobalPreferences.acc_radio)
				{
					HidCom.Set_mode_Choice(MODE_CHOICE.ACCELERATION_MODE);
					if (GlobalPreferences.hpfilterCb)
					{
						if (GlobalPreferences.lpfilterCb)
						{
							HidCom.Set_filter_Choice(FILTER_CHOICE.HP_LP);
						}
						else
						{
							HidCom.Set_filter_Choice(FILTER_CHOICE.HP);
						}
					}
					else
					{
						if (GlobalPreferences.lpfilterCb)
						{
							HidCom.Set_filter_Choice(FILTER_CHOICE.LP);
						}
						else
						{
							HidCom.Set_filter_Choice(FILTER_CHOICE.NO_FILTER);
						}
					}
				}
				else if (GlobalPreferences.vel_radio)
				{
					HidCom.Set_mode_Choice(MODE_CHOICE.VELOCITY_MODE);
				}
				else
				{
					HidCom.Set_mode_Choice(MODE_CHOICE.DISPLACEMENT_MODE);
				}

				//FFT bases analysis
				if (GlobalPreferences.a_s_q_p_radio)
				{
					HidCom.Set_freq_Choice_Update(FREQUENTIAL_CHOICE.AMPLITUDE_SPECTRUM_PEAK);
				}
				else if (GlobalPreferences.a_s_rms_radio)
				{
					HidCom.Set_freq_Choice_Update(FREQUENTIAL_CHOICE.AMPLITUDE_SPECTRUM_RMS);
				}
				else if (GlobalPreferences.p_s_radio)
				{
					HidCom.Set_freq_Choice_Update(FREQUENTIAL_CHOICE.POWER_SPECTRUM);
				}
				else if (GlobalPreferences.p_s_d_radio)
				{
					HidCom.Set_freq_Choice_Update(FREQUENTIAL_CHOICE.POWER_SPECTRAL_DENSITY);
				}
				else
				{
					HidCom.Set_freq_Choice_Update(FREQUENTIAL_CHOICE.AMPLITUDE_SPECTRAL_DENSITY);
				}
				//FFT windowing
				if (GlobalPreferences.rectangular_radio)
				{
					HidCom.Set_window_Choice(WINDOW_CHOICE.RECTANGULAR_WINDOW);
					HidCom.Set_window_npbw(1.0f);
				}
				else if (GlobalPreferences.hanning_radio)
				{
					HidCom.Set_window_Choice(WINDOW_CHOICE.HANNING_WINDOW);
					HidCom.Set_window_npbw(1.5f);
				}
				else
				{
					HidCom.Set_window_Choice(WINDOW_CHOICE.FLAT_TOP_WINDOW);
					HidCom.Set_window_npbw(3.77f);
				}
				switch (HidCom.Get_mode_Choice())
				{
					case MODE_CHOICE.ACCELERATION_MODE:
						if (!HidCom.GetgUnits())
						{
							switch (HidCom.Get_freq_Choice_Update())
							{
								case FREQUENTIAL_CHOICE.AMPLITUDE_SPECTRUM_PEAK:
									yAxisF.AxisTitle = GetString(Resource.String.y_label_freq_ms2_asqp);
									break;
								case FREQUENTIAL_CHOICE.AMPLITUDE_SPECTRUM_RMS:
									yAxisF.AxisTitle = GetString(Resource.String.y_label_freq_ms2_asrms);
									break;
								case FREQUENTIAL_CHOICE.AMPLITUDE_SPECTRAL_DENSITY:
									yAxisF.AxisTitle = GetString(Resource.String.y_label_freq_ms2_asd);
									break;
								case FREQUENTIAL_CHOICE.POWER_SPECTRUM:
									yAxisF.AxisTitle = GetString(Resource.String.y_label_freq_ms2_ps);
									break;
								case FREQUENTIAL_CHOICE.POWER_SPECTRAL_DENSITY:
									yAxisF.AxisTitle = GetString(Resource.String.y_label_freq_ms2_psd);
									break;
								default:
									break;
							}
							yAxisT.AxisTitle = GetString(Resource.String.y_label_time_ms2);
						}
						else
						{
							switch (HidCom.Get_freq_Choice_Update())
							{
								case FREQUENTIAL_CHOICE.AMPLITUDE_SPECTRUM_PEAK:
									yAxisF.AxisTitle = GetString(Resource.String.y_label_freq_g_asqp);
									break;
								case FREQUENTIAL_CHOICE.AMPLITUDE_SPECTRUM_RMS:
									yAxisF.AxisTitle = GetString(Resource.String.y_label_freq_g_asrms);
									break;
								case FREQUENTIAL_CHOICE.AMPLITUDE_SPECTRAL_DENSITY:
									yAxisF.AxisTitle = GetString(Resource.String.y_label_freq_g_asd);
									break;
								case FREQUENTIAL_CHOICE.POWER_SPECTRUM:
									yAxisF.AxisTitle = GetString(Resource.String.y_label_freq_g_ps);
									break;
								case FREQUENTIAL_CHOICE.POWER_SPECTRAL_DENSITY:
									yAxisF.AxisTitle = GetString(Resource.String.y_label_freq_g_psd);
									break;
								default:
									break;
							}
							yAxisT.AxisTitle = GetString(Resource.String.y_label_time_g);
						}
						break;
					case MODE_CHOICE.VELOCITY_MODE:
						switch (HidCom.Get_freq_Choice_Update())
						{
							case FREQUENTIAL_CHOICE.AMPLITUDE_SPECTRUM_PEAK:
								yAxisF.AxisTitle = GetString(Resource.String.y_label_freq_ms_asqp);
								break;
							case FREQUENTIAL_CHOICE.AMPLITUDE_SPECTRUM_RMS:
								yAxisF.AxisTitle = GetString(Resource.String.y_label_freq_ms_asrms);
								break;
							case FREQUENTIAL_CHOICE.AMPLITUDE_SPECTRAL_DENSITY:
								yAxisF.AxisTitle = GetString(Resource.String.y_label_freq_ms_asd);
								break;
							case FREQUENTIAL_CHOICE.POWER_SPECTRUM:
								yAxisF.AxisTitle = GetString(Resource.String.y_label_freq_ms_ps);
								break;
							case FREQUENTIAL_CHOICE.POWER_SPECTRAL_DENSITY:
								yAxisF.AxisTitle = GetString(Resource.String.y_label_freq_ms_psd);
								break;
							default:
								break;
						}
						yAxisT.AxisTitle = GetString(Resource.String.y_label_time_ms);
						break;
					case MODE_CHOICE.DISPLACEMENT_MODE:
						switch (HidCom.Get_freq_Choice_Update())
						{
							case FREQUENTIAL_CHOICE.AMPLITUDE_SPECTRUM_PEAK:
								yAxisF.AxisTitle = GetString(Resource.String.y_label_freq_m_asqp);
								break;
							case FREQUENTIAL_CHOICE.AMPLITUDE_SPECTRUM_RMS:
								yAxisF.AxisTitle = GetString(Resource.String.y_label_freq_m_asrms);
								break;
							case FREQUENTIAL_CHOICE.AMPLITUDE_SPECTRAL_DENSITY:
								yAxisF.AxisTitle = GetString(Resource.String.y_label_freq_m_asd);
								break;
							case FREQUENTIAL_CHOICE.POWER_SPECTRUM:
								yAxisF.AxisTitle = GetString(Resource.String.y_label_freq_m_ps);
								break;
							case FREQUENTIAL_CHOICE.POWER_SPECTRAL_DENSITY:
								yAxisF.AxisTitle = GetString(Resource.String.y_label_freq_m_psd);
								break;
							default:
								break;
						}
						yAxisT.AxisTitle = GetString(Resource.String.y_label_time_m);
						break;
				}
				//Time/Frequency
				Enable_time_domain(GlobalPreferences.T_switch);
				Enable_freq_domain(GlobalPreferences.F_switch);

				//2g/6g
				if (!CheckMySerial(HidCom.Sn()))
				{
					if (HidCom.Get_input_Choice() != INPUT_CHOICE.RECORDED_VALUES)
					{
						HidCom.SetSensitivity((byte)0x02);
					}
					HidCom.Set_2g_sensitivity(true);
				}
				else
				{
					if (GlobalPreferences.g_2)
					{
						if (HidCom.Get_input_Choice() != INPUT_CHOICE.RECORDED_VALUES)
						{
							HidCom.SetSensitivity((byte)0x02);
						}
						HidCom.Set_2g_sensitivity(true);
					}
					else
					{
						if (HidCom.Get_input_Choice() != INPUT_CHOICE.RECORDED_VALUES)
						{
							HidCom.SetSensitivity((byte)0x06);
						}
						HidCom.Set_2g_sensitivity(false);
					}
				}
				if (!GlobalPreferences.Ax_cb)
				{
					if (chartSurface_T.RenderableSeries.Contains(Flrs_Ax))
					{
						chartSurface_T.RenderableSeries.Remove(Flrs_Ax);
					}

					if (chartSurface_F.RenderableSeries.Contains(Flrs_Fft_Ax))
					{
						chartSurface_F.RenderableSeries.Remove(Flrs_Fft_Ax);
					}
				}
				else
				{
					if (!chartSurface_T.RenderableSeries.Contains(Flrs_Ax))
					{
						chartSurface_T.RenderableSeries.Add(Flrs_Ax);
					}

					if (!chartSurface_F.RenderableSeries.Contains(Flrs_Fft_Ax)) chartSurface_F.RenderableSeries.Add(Flrs_Fft_Ax);
				}
				if (!GlobalPreferences.Ay_cb)
				{
					if (chartSurface_T.RenderableSeries.Contains(Flrs_Ay)) chartSurface_T.RenderableSeries.Remove(Flrs_Ay);
					if (chartSurface_F.RenderableSeries.Contains(Flrs_Fft_Ay)) chartSurface_F.RenderableSeries.Remove(Flrs_Fft_Ay);
				}
				else
				{
					if (!chartSurface_T.RenderableSeries.Contains(Flrs_Ay)) chartSurface_T.RenderableSeries.Add(Flrs_Ay);
					if (!chartSurface_F.RenderableSeries.Contains(Flrs_Fft_Ay)) chartSurface_F.RenderableSeries.Add(Flrs_Fft_Ay);
				}
				if (!GlobalPreferences.Az_cb)
				{
					if (chartSurface_T.RenderableSeries.Contains(Flrs_Az)) chartSurface_T.RenderableSeries.Remove(Flrs_Az);
					if (chartSurface_F.RenderableSeries.Contains(Flrs_Fft_Az)) chartSurface_F.RenderableSeries.Remove(Flrs_Fft_Az);
				}
				else
				{
					if (!chartSurface_T.RenderableSeries.Contains(Flrs_Az)) chartSurface_T.RenderableSeries.Add(Flrs_Az);
					if (!chartSurface_F.RenderableSeries.Contains(Flrs_Fft_Az)) chartSurface_F.RenderableSeries.Add(Flrs_Fft_Az);
				}


				if (GlobalPreferences.chart_scaling)
				{
					HidCom.SetyAutoScale(true);
					buttonScale.Alpha = 1f;
				}
				else
				{
					HidCom.SetyAutoScale(false);
					buttonScale.Alpha = 0.3f;
				}
				//ExpoRMS tau
				HidCom.SetRms_tau(GlobalPreferences.EditTau);
				//HP cutoff freq
				HidCom.Set_hp_filter_fc(GlobalPreferences.fc_hp);
				//LP cutoff freq
				HidCom.Set_lp_filter_fc(GlobalPreferences.fc_lp);
				//VEL cutoff freq
				HidCom.Set_vel_filter_fc(GlobalPreferences.fc_vel);
				//DISP cutoff freq
				HidCom.Set_disp_filter_fc(GlobalPreferences.fc_dis);
				//Period to show
				HidCom.SetnPointsToShow((int)((double)HidCom.GetSampling_Freq() * GlobalPreferences.EditPeriodShow)); // to set n point to show
																													  //Period to keep
				HidCom.SetnPointsToKeep(HidCom.GetSampling_Freq() * GlobalPreferences.EditPeriodKeep); // to set n point to keep
																									   //Period to skip
				HidCom.SetSkipNextValue(GlobalPreferences.EditPeriodSkipNext); // to set n point to keep
																			   //FFT : Average Depth
				HidCom.SetAverageDepthUpdate(GlobalPreferences.avginput);
				//FFT : Window Size
				HidCom.SetFft_Window_Size_Update(GlobalPreferences.fftinput);


				//Toast to warn for narrow bandwith
				if (HidCom.Get_filter_Choice() == FILTER_CHOICE.HP_LP)
				{
					double lpf = HidCom.Get_lp_filter_fc();
					double hpf = HidCom.Get_hp_filter_fc();
					if ((lpf - hpf) < BANDPASS_WARNING_THRESHOLD)
					{
						Toast.MakeText(this, GetString(Resource.String.narrow_bandwith), ToastLength.Short).Show();
					}
				}
			}

			#endregion start
		}

		#region Dialogs
		public void LicenceShowDialog()
		{
			Dialog dialog = new Dialog(this);
			dialog.RequestWindowFeature((int)WindowFeatures.NoTitle);
			dialog.SetCancelable(true);
			dialog.SetContentView(Resource.Layout.about_license);
			TextView textViewGUAVA = (TextView)dialog.FindViewById(Resource.Id.textViewGUAVA);
			TextView textViewJODA = (TextView)dialog.FindViewById(Resource.Id.textViewJODA);
			TextView textViewDATETIMEPICKER = (TextView)dialog.FindViewById(Resource.Id.textViewDATETIMEPICKER);
			TextView textViewfolderpicker = (TextView)dialog.FindViewById(Resource.Id.textViewfolderpicker);
			textViewGUAVA.Text = Resources.GetString(Resource.String.text_license_Guava);
			textViewJODA.Text = Resources.GetString(Resource.String.text_license_jodatime);
			textViewDATETIMEPICKER.Text = Resources.GetString(Resource.String.text_license_datetimepicker);
			textViewfolderpicker.Text = Resources.GetString(Resource.String.text_license_folder_picker);
			Button dialogButton = (Button)dialog.FindViewById(Resource.Id.btn_dialog);
			dialogButton.Click += (args, e) =>
			{
				dialog.Dismiss();
			};
			int width = this.Resources.DisplayMetrics.WidthPixels;
			int height = this.Resources.DisplayMetrics.HeightPixels;
			dialog.Window.SetLayout(width, height - 20);
			/*To set the dialog to the entire phone screen */
			dialog.Show();
		}
		public void SettingsDialog()
		{
			Dialog dialog = new Dialog(this);
			dialog.RequestWindowFeature((int)WindowFeatures.NoTitle);
			dialog.SetCancelable(true);
			dialog.SetContentView(Resource.Layout.settings_dialog);
			int width = this.Resources.DisplayMetrics.WidthPixels;
			int height = this.Resources.DisplayMetrics.HeightPixels;
			dialog.Window.SetLayout(width, height - 20);
			#region variables
			RadioGroup signal_proc_grp = dialog.FindViewById<RadioGroup>(Resource.Id.grp_signal_proc);
			ImageButton signal_proc_exp = dialog.FindViewById<ImageButton>(Resource.Id.sinal_proc_exp);

			LinearLayout grp_fft_analysis_l = dialog.FindViewById<LinearLayout>(Resource.Id.grp_fft_analysis_l);
			ImageButton fft_ana_exp = (ImageButton)dialog.FindViewById<ImageButton>(Resource.Id.fft_ana_exp);

			Drawable exp_dr_more = ResourcesCompat.GetDrawable(this.Resources, Resource.Drawable.ic_expand_more_white_24dp, null);
			Drawable exp_dr_less = ResourcesCompat.GetDrawable(this.Resources, Resource.Drawable.ic_expand_less_white_24dp, null);


			LinearLayout showLayout = dialog.FindViewById<LinearLayout>(Resource.Id.temporalAddonShow);
			LinearLayout keepLayout = dialog.FindViewById<LinearLayout>(Resource.Id.temporalAddonKeep);
			LinearLayout autoScaleDataLayout = dialog.FindViewById<LinearLayout>(Resource.Id.autoscalelayout);

			LinearLayout skipNextLayout = dialog.FindViewById<LinearLayout>(Resource.Id.skipNextLayout);

			LinearLayout panelResolution = dialog.FindViewById<LinearLayout>(Resource.Id.panelResolution);
			LinearLayout panelResolution2 = dialog.FindViewById<LinearLayout>(Resource.Id.panelResolution2);

			LinearLayout layout_hp = dialog.FindViewById<LinearLayout>(Resource.Id.layout_hp);
			LinearLayout layout_lp = dialog.FindViewById<LinearLayout>(Resource.Id.layout_lp);

			LinearLayout fftLabelLayout = dialog.FindViewById<LinearLayout>(Resource.Id.layoutTitleFft);
			LinearLayout fftDataInput = dialog.FindViewById<LinearLayout>(Resource.Id.fftDataInput);
			LinearLayout fftDataInput3 = dialog.FindViewById<LinearLayout>(Resource.Id.fftDataInput3);
			LinearLayout a_s_q_p_radio_l = dialog.FindViewById<LinearLayout>(Resource.Id.a_s_q_p_radio_l);
			LinearLayout a_s_d_radio_l = dialog.FindViewById<LinearLayout>(Resource.Id.a_s_d_radio_l);
			LinearLayout a_s_rms_radio_l = dialog.FindViewById<LinearLayout>(Resource.Id.a_s_rms_radio_l);
			LinearLayout p_s_d_radio_l = dialog.FindViewById<LinearLayout>(Resource.Id.p_s_d_radio_l);
			LinearLayout p_s_radio_l = dialog.FindViewById<LinearLayout>(Resource.Id.p_s_radio_l);

			TextView resolutionText = dialog.FindViewById<TextView>(Resource.Id.resolutionText);
			TextView signalProcText = dialog.FindViewById<TextView>(Resource.Id.signal_proc_label);
			TextView valuesText = dialog.FindViewById<TextView>(Resource.Id.valueText);
			TextView unitsText = dialog.FindViewById<TextView>(Resource.Id.unitsText);
			TextView showText = dialog.FindViewById<TextView>(Resource.Id.periodShow);
			TextView keepText = dialog.FindViewById<TextView>(Resource.Id.periodKeep);
			TextView skipNextText = dialog.FindViewById<TextView>(Resource.Id.skipNextText);
			TextView autoscaleText = dialog.FindViewById<TextView>(Resource.Id.label_scale);
			TextView rmsExpoText = dialog.FindViewById<TextView>(Resource.Id.rmsExpo);
			TextView wSizeText = dialog.FindViewById<TextView>(Resource.Id.fftWsizeTextText);
			TextView avDepthText = dialog.FindViewById<TextView>(Resource.Id.averageInputText);
			TextView window_function = dialog.FindViewById<TextView>(Resource.Id.window_function);
			TextView fft_based_computations = dialog.FindViewById<TextView>(Resource.Id.fft_based_computations);

			ImageButton editHP = (ImageButton)dialog.FindViewById<ImageButton>(Resource.Id.edit_hp);
			ImageButton editLP = (ImageButton)dialog.FindViewById<ImageButton>(Resource.Id.edit_lp);
			ImageButton editVelocity = (ImageButton)dialog.FindViewById<ImageButton>(Resource.Id.edit_velocity);
			ImageButton editDisplacement = (ImageButton)dialog.FindViewById<ImageButton>(Resource.Id.edit_displacement);

			ImageButton editVisibleRange = (ImageButton)dialog.FindViewById<ImageButton>(Resource.Id.edit_visible_range);
			ImageButton editDataRange = (ImageButton)dialog.FindViewById<ImageButton>(Resource.Id.edit_data_range);
			ImageButton editSkipNext = (ImageButton)dialog.FindViewById<ImageButton>(Resource.Id.edit_skip_next);
			ImageButton editFIFOAvDept = (ImageButton)dialog.FindViewById<ImageButton>(Resource.Id.edit_fifo_depth);
			ImageButton editFFTSize = (ImageButton)dialog.FindViewById<ImageButton>(Resource.Id.edit_fft_size);

			TextView serialnumberText = dialog.FindViewById<TextView>(Resource.Id.SerialNumber);
			#endregion variables

			if (HidCom.Get_input_Choice() == INPUT_CHOICE.RECORDED_VALUES)
			{
				panelResolution.Visibility = ViewStates.Gone;
				skipNextLayout.Visibility = ViewStates.Visible;
				serialnumberText.Text = GetString(Resource.String.serial_number) + " " + GlobalPreferences.Serial_Number;
			}
			else
			{
				skipNextLayout.Visibility = ViewStates.Gone;
				serialnumberText.Text = GetString(Resource.String.serial_number) + " " + GlobalPreferences.Serial_Number;
				//no choice between 2g/6g if 15g and 200g
				if (!CheckMySerial(GlobalPreferences.Serial_Number))
				{
					panelResolution.Visibility = ViewStates.Gone;
				}
				else
				{
					panelResolution.Visibility = ViewStates.Visible;
				}

			}

			sixg_radio = (RadioButton)dialog.FindViewById(Resource.Id.sixg_radio);
			twog_radio = (RadioButton)dialog.FindViewById(Resource.Id.twog_radio);

			gunits_radio = (RadioButton)dialog.FindViewById(Resource.Id.gunits_radio);
			ms2units_radio = (RadioButton)dialog.FindViewById(Resource.Id.ms2units_radio);

			Ax_cb = (CheckBox)dialog.FindViewById(Resource.Id.ax_cb);
			Ay_cb = (CheckBox)dialog.FindViewById(Resource.Id.ay_cb);
			Az_cb = (CheckBox)dialog.FindViewById(Resource.Id.az_cb);

			hpfilterCb = (CheckBox)dialog.FindViewById(Resource.Id.filter_hp);
			lpfilterCb = (CheckBox)dialog.FindViewById(Resource.Id.filter_lp);
			accelerationRadio = (RadioButton)dialog.FindViewById(Resource.Id.acc_radio);
			velocityRadio = (RadioButton)dialog.FindViewById(Resource.Id.vel_radio);
			displacementRadio = (RadioButton)dialog.FindViewById(Resource.Id.disp_radio);

			a_s_q_p_radio = (RadioButton)dialog.FindViewById(Resource.Id.a_s_q_p_radio);
			a_s_rms_radio = (RadioButton)dialog.FindViewById(Resource.Id.a_s_rms_radio);
			p_s_radio = (RadioButton)dialog.FindViewById(Resource.Id.p_s_radio);
			p_s_d_radio = (RadioButton)dialog.FindViewById(Resource.Id.p_s_d_radio);
			a_s_d_radio = (RadioButton)dialog.FindViewById(Resource.Id.a_s_d_radio);

			rectangular_radio = (RadioButton)dialog.FindViewById(Resource.Id.rectangular_radio);
			hanning_radio = (RadioButton)dialog.FindViewById(Resource.Id.hanning_radio);
			flattop_radio = (RadioButton)dialog.FindViewById(Resource.Id.flattop_radio);

			if (flagStartActivate)
			{
				if (!replayFlag)
				{
					//no possibility to switch between modes, filter consistency reason
					DisableRadio(accelerationRadio);
					DisableRadio(velocityRadio);
					DisableRadio(displacementRadio);
					//Disable cutoff freq selection
					DisableImButton(editHP);
					DisableImButton(editLP);
					DisableImButton(editVelocity);
					DisableImButton(editDisplacement);
					editHP.Enabled = false;
					editLP.Enabled = false;
					editVelocity.Enabled = false;
					editDisplacement.Enabled = false;
					//no possibility to switch between g/(m/s^²), graph consistency reason
					DisableRadio(gunits_radio);
					DisableRadio(ms2units_radio);
				}
				//no possibility to switch between 2g/6g, software reason
				DisableRadio(sixg_radio);
				DisableRadio(twog_radio);

			}
			gunits_radio.Click += (args, e) =>
			 {
				 if (accelerationRadio.Checked)
				 {
					 a_s_q_p_radio.Text = GetString(Resource.String.y_label_freq_g_asqp);
					 a_s_rms_radio.Text = GetString(Resource.String.y_label_freq_g_asrms);
					 p_s_radio.Text = GetString(Resource.String.y_label_freq_g_ps);
					 p_s_d_radio.Text = GetString(Resource.String.y_label_freq_g_psd);
					 a_s_d_radio.Text = GetString(Resource.String.y_label_freq_g_asd);
				 }
			 };

			ms2units_radio.Click += (args, e) =>
			{
				if (accelerationRadio.Checked)
				{
					a_s_q_p_radio.Text = GetString(Resource.String.y_label_freq_ms2_asqp);
					a_s_rms_radio.Text = GetString(Resource.String.y_label_freq_ms2_asrms);
					p_s_radio.Text = GetString(Resource.String.y_label_freq_ms2_ps);
					p_s_d_radio.Text = GetString(Resource.String.y_label_freq_ms2_psd);
					a_s_d_radio.Text = GetString(Resource.String.y_label_freq_ms2_asd);
				}
			};
			accelerationRadio.Click += (args, e) =>
			{
				panelResolution2.Visibility = ViewStates.Visible;
				layout_hp.Visibility = ViewStates.Visible;
				layout_lp.Visibility = ViewStates.Visible;
				accelerationRadio.Checked = true;
				velocityRadio.Checked = false;
				displacementRadio.Checked = false;
				if (gunits_radio.Checked)
				{
					a_s_q_p_radio.Text = GetString(Resource.String.y_label_freq_g_asqp);
					a_s_rms_radio.Text = GetString(Resource.String.y_label_freq_g_asrms);
					p_s_radio.Text = GetString(Resource.String.y_label_freq_g_ps);
					p_s_d_radio.Text = GetString(Resource.String.y_label_freq_g_psd);
					a_s_d_radio.Text = GetString(Resource.String.y_label_freq_g_asd);
				}
				else
				{
					a_s_q_p_radio.Text = GetString(Resource.String.y_label_freq_ms2_asqp);
					a_s_rms_radio.Text = GetString(Resource.String.y_label_freq_ms2_asrms);
					p_s_radio.Text = GetString(Resource.String.y_label_freq_ms2_ps);
					p_s_d_radio.Text = GetString(Resource.String.y_label_freq_ms2_psd);
					a_s_d_radio.Text = GetString(Resource.String.y_label_freq_ms2_asd);
				}
			};

			velocityRadio.Click += (args, e) =>
			{
				panelResolution2.Visibility = ViewStates.Gone;
				layout_hp.Visibility = ViewStates.Gone;
				layout_lp.Visibility = ViewStates.Gone;
				accelerationRadio.Checked = false;
				velocityRadio.Checked = true;
				displacementRadio.Checked = false;
				a_s_q_p_radio.Text = GetString(Resource.String.y_label_freq_ms_asqp);
				a_s_rms_radio.Text = GetString(Resource.String.y_label_freq_ms_asrms);
				p_s_radio.Text = GetString(Resource.String.y_label_freq_ms_ps);
				p_s_d_radio.Text = GetString(Resource.String.y_label_freq_ms_psd);
				a_s_d_radio.Text = GetString(Resource.String.y_label_freq_ms_asd);
			};
			displacementRadio.Click += (args, e) =>
			{
				panelResolution2.Visibility = ViewStates.Gone;
				layout_hp.Visibility = ViewStates.Gone;
				layout_lp.Visibility = ViewStates.Gone;
				accelerationRadio.Checked = false;
				velocityRadio.Checked = false;
				displacementRadio.Checked = true;
				a_s_q_p_radio.Text = GetString(Resource.String.y_label_freq_m_asqp);
				a_s_rms_radio.Text = GetString(Resource.String.y_label_freq_m_asrms);
				p_s_radio.Text = GetString(Resource.String.y_label_freq_m_ps);
				p_s_d_radio.Text = GetString(Resource.String.y_label_freq_m_psd);
				a_s_d_radio.Text = GetString(Resource.String.y_label_freq_m_asd);
			};

			a_s_q_p_radio.Click += (args, e) =>
			{
				a_s_q_p_radio.Checked = true;
				a_s_rms_radio.Checked = false;
				p_s_radio.Checked = false;
				p_s_d_radio.Checked = false;
				a_s_d_radio.Checked = false;
			};
			a_s_rms_radio.Click += (args, e) =>
			{
				a_s_q_p_radio.Checked = false;
				a_s_rms_radio.Checked = true;
				p_s_radio.Checked = false;
				p_s_d_radio.Checked = false;
				a_s_d_radio.Checked = false;
			};
			p_s_radio.Click += (args, e) =>
			{
				a_s_q_p_radio.Checked = false;
				a_s_rms_radio.Checked = false;
				p_s_radio.Checked = true;
				p_s_d_radio.Checked = false;
				a_s_d_radio.Checked = false;
			};
			p_s_d_radio.Click += (args, e) =>
			{
				a_s_q_p_radio.Checked = false;
				a_s_rms_radio.Checked = false;
				p_s_radio.Checked = false;
				p_s_d_radio.Checked = true;
				a_s_d_radio.Checked = false;
			};
			a_s_d_radio.Click += (args, e) =>
			{
				a_s_q_p_radio.Checked = false;
				a_s_rms_radio.Checked = false;
				p_s_radio.Checked = false;
				p_s_d_radio.Checked = false;
				a_s_d_radio.Checked = true;
			};
			rectangular_radio.Click += (args, e) =>
			{
				rectangular_radio.Checked = true;
				hanning_radio.Checked = false;
				flattop_radio.Checked = false;
			};
			hanning_radio.Click += (args, e) =>
			{
				rectangular_radio.Checked = false;
				hanning_radio.Checked = true;
				flattop_radio.Checked = false;
			};
			flattop_radio.Click += (args, e) =>
			 {
				 rectangular_radio.Checked = false;
				 hanning_radio.Checked = false;
				 flattop_radio.Checked = true;
			 };

			no_scaling = (RadioButton)dialog.FindViewById(Resource.Id.no_scaling);
			chart_scaling = (RadioButton)dialog.FindViewById(Resource.Id.chart_scaling);

			editHP.Click += (args, e) =>
			 {
				 if (lpfilterCb.Checked)
				 {
					 NumShowDialog(HP_FILTER_FC_MIN, local_lp_fc, hpfilterCb, 1);
				 }
				 else
				 {
					 NumShowDialog(HP_FILTER_FC_MIN, HP_FILTER_FC_MAX, hpfilterCb, 1);
				 }
			 };
			editLP.Click += (args, e) =>
			{
				if (hpfilterCb.Checked)
				{
					NumShowDialog(local_hp_fc, LP_FILTER_FC_MAX, lpfilterCb, 2);
				}
				else
				{
					NumShowDialog(LP_FILTER_FC_MIN, LP_FILTER_FC_MAX, lpfilterCb, 2);
				}

			};
			editVelocity.Click += (args, e) =>
			{
				NumShowDialog(VEL_FILTER_FC_MIN, VEL_FILTER_FC_MAX, velocityRadio, 3);
			};
			editDisplacement.Click += (args, e) =>
			{
				NumShowDialog(DISP_FILTER_FC_MIN, DISP_FILTER_FC_MAX, displacementRadio, 4);
			};
			hpfilterCb.Click += (args, e) =>
			{
				if (lpfilterCb.Checked && (local_lp_fc < local_hp_fc))
				{
					hpfilterCb.Checked = false;
					Toast.MakeText(this, GetString(Resource.String.fc_unauthorized),
							ToastLength.Long).Show();
					NumShowDialog(HP_FILTER_FC_MIN, local_lp_fc, hpfilterCb, 1);
				}
			};
			lpfilterCb.Click += (args, e) =>
			{
				if (hpfilterCb.Checked && (local_lp_fc < local_hp_fc))
				{
					lpfilterCb.Checked = false;
					Toast.MakeText(this, GetString(Resource.String.fc_unauthorized), ToastLength.Long).Show();
					NumShowDialog(local_hp_fc, LP_FILTER_FC_MAX, lpfilterCb, 2);
				}
			};

			TextView editPeriodShow = (TextView)dialog.FindViewById(Resource.Id.editPeriodShow);
			TextView avginput = (TextView)dialog.FindViewById(Resource.Id.averageInput);
			TextView fftinput = (TextView)dialog.FindViewById(Resource.Id.fftSizeInput);
			TextView editPeriodKeep = (TextView)dialog.FindViewById(Resource.Id.editPeriodKeep);
			TextView editPeriodSkipNext = (TextView)dialog.FindViewById(Resource.Id.skipNextValue);

			editVisibleRange.Click += (args, e) =>
			{
				NumShowDialog(VISIBLE_RANGE_MIN, VISIBLE_RANGE_MAX, editPeriodShow, 5);
			};
			editDataRange.Click += (args, e) =>
			{
				PickDialShowDialog(DATA_RANGE_MIN, DATA_RANGE_MAX, editPeriodKeep, 4);
			};
			editSkipNext.Click += (args, e) =>
			{
				PickDialShowDialog(SKIP_NEXT_MIN, SKIP_NEXT_MAX, editPeriodSkipNext, 5);
			};
			editFIFOAvDept.Click += (args, e) =>
			{
				PickDialShowDialog(FIFO_AV_MIN, FIFO_AV_MAX, avginput, 1);
			};
			editFFTSize.Click += (args, e) =>
			{
				PickDialShowDialog(FFT_SIZE_MIN, FFT_SIZE_MAX, fftinput, 2);
			};

			SeekBar valRmsExpo = (SeekBar)dialog.FindViewById(Resource.Id.valRmsExpo);
			editTau = (TextView)dialog.FindViewById(Resource.Id.editTau);

			valRmsExpo.SetOnSeekBarChangeListener(this);
			signal_proc_exp.Click += (args, e) =>
			{
				if (GlobalPreferences.Signal_Proc_Hide)
				{
					signal_proc_grp.Visibility = ViewStates.Gone;
					signal_proc_exp.SetImageDrawable(exp_dr_more);
					GlobalPreferences.Signal_Proc_Hide = true;
				}
				else
				{
					signal_proc_grp.Visibility = ViewStates.Visible;
					signal_proc_exp.SetImageDrawable(exp_dr_less);
					GlobalPreferences.Signal_Proc_Hide = false;
				}
			};
			fft_ana_exp.Click += (args, e) =>
			{
				if (!GlobalPreferences.Fft_Ana_Hide)
				{
					grp_fft_analysis_l.Visibility = ViewStates.Gone;
					fft_ana_exp.SetImageDrawable(exp_dr_more);
					GlobalPreferences.Fft_Ana_Hide = true;
				}
				else
				{
					grp_fft_analysis_l.Visibility = ViewStates.Visible;
					fft_ana_exp.SetImageDrawable(exp_dr_less);
					GlobalPreferences.Fft_Ana_Hide = false;
				}
			};
			sixg_radio.Checked = GlobalPreferences.g_6;
			twog_radio.Checked = GlobalPreferences.g_2;

			gunits_radio.Checked = GlobalPreferences.gunits;
			ms2units_radio.Checked = GlobalPreferences.ms2units;
			Ax_cb.Checked = GlobalPreferences.Ax_cb;
			Ay_cb.Checked = GlobalPreferences.Ay_cb;
			Az_cb.Checked = GlobalPreferences.Az_cb;

			accelerationRadio.Checked = GlobalPreferences.acc_radio;
			hpfilterCb.Checked = GlobalPreferences.hpfilterCb;

			if (Objects.Equals(GlobalPreferences.fc_hp, false))
			{
				hpfilterCb.Text = GetString(Resource.String.filer_hp_high_pass) + " (" + GetString(Resource.String.fc_label) + " 1 [Hz])";
				local_hp_fc = 1;
			}
			else
			{
				hpfilterCb.Text = GetString(Resource.String.filer_hp_high_pass) + " (" + GetString(Resource.String.fc_label) + " " + GlobalPreferences.fc_hp + " [Hz])";
				local_hp_fc = GlobalPreferences.fc_hp;
			}

			//Cannot be equal to 0
			if (Objects.Equals(GlobalPreferences.fc_hp, 0))
			{
				hpfilterCb.Text = GetString(Resource.String.filer_hp_high_pass) + " (" + GetString(Resource.String.fc_label) + " 1 [Hz])";
				local_hp_fc = 1;
			}

			lpfilterCb.Checked = GlobalPreferences.lpfilterCb;
			if (Objects.Equals(GlobalPreferences.fc_lp, "false"))
			{
				lpfilterCb.Text = GetString(Resource.String.filer_low_pass) + " (" + GetString(Resource.String.fc_label) + " 200 [Hz])";
				local_lp_fc = 200;
			}
			else
			{
				lpfilterCb.Text = GetString(Resource.String.filer_low_pass) + " (" + GetString(Resource.String.fc_label) + " " + GlobalPreferences.fc_lp + " [Hz])";
				local_lp_fc = GlobalPreferences.fc_lp;
			}
			if (Objects.Equals(GlobalPreferences.fc_lp, 0))
			{
				lpfilterCb.Text = GetString(Resource.String.filer_low_pass) + " (" + GetString(Resource.String.fc_label) + " 200 [Hz])";
				local_lp_fc = 200;
			}

			velocityRadio.Checked = GlobalPreferences.vel_radio;

			if (Objects.Equals(GlobalPreferences.fc_vel, "false"))
			{
				velocityRadio.Text = GetString(Resource.String.filer_velocity_mode) + " (" + GetString(Resource.String.fc_label) + " 1 [Hz])";
				local_velocity_fc = 1;
			}
			else
			{
				velocityRadio.Text = GetString(Resource.String.filer_velocity_mode) + " (" + GetString(Resource.String.fc_label) + " " + GlobalPreferences.fc_vel + " [Hz])";
				local_velocity_fc = GlobalPreferences.fc_vel;
			}
			//Cannot be equal to 0
			if (Objects.Equals(GlobalPreferences.fc_vel, 0))
			{
				velocityRadio.Text = GetString(Resource.String.filer_velocity_mode) + " (" + GetString(Resource.String.fc_label) + " 1 [Hz])";
				local_velocity_fc = 1;
			}

			displacementRadio.Checked = GlobalPreferences.disp_radio;

			if (Objects.Equals(GlobalPreferences.fc_dis, "false"))
			{
				displacementRadio.Text = GetString(Resource.String.filer_displacement_mode) + " (" + GetString(Resource.String.fc_label) + " 1 [Hz])";
				local_displacement_fc = 1;
			}
			else
			{
				displacementRadio.Text = GetString(Resource.String.filer_displacement_mode) + " (" + GetString(Resource.String.fc_label) + " " + GlobalPreferences.fc_dis + " [Hz])";
				local_displacement_fc = GlobalPreferences.fc_dis;
			}
			//Cannot be equal to 0
			if (Objects.Equals(GlobalPreferences.fc_dis, 0))
			{
				displacementRadio.Text = GetString(Resource.String.filer_displacement_mode) + " (" + GetString(Resource.String.fc_label) + " 1 [Hz])";
				local_displacement_fc = 1;
			}

			if (!GlobalPreferences.acc_radio && !GlobalPreferences.vel_radio && !GlobalPreferences.disp_radio)
			{
				accelerationRadio.Checked = true;
			}
			if (accelerationRadio.Checked)
			{
				layout_hp.Visibility = ViewStates.Visible;
				layout_lp.Visibility = ViewStates.Visible;
				panelResolution2.Visibility = ViewStates.Visible;
				if (gunits_radio.Checked)
				{
					a_s_q_p_radio.Text = GetString(Resource.String.y_label_freq_g_asqp);
					a_s_rms_radio.Text = GetString(Resource.String.y_label_freq_g_asrms);
					p_s_radio.Text = GetString(Resource.String.y_label_freq_g_ps);
					p_s_d_radio.Text = GetString(Resource.String.y_label_freq_g_psd);
					a_s_d_radio.Text = GetString(Resource.String.y_label_freq_g_asd);
				}
				else
				{
					a_s_q_p_radio.Text = GetString(Resource.String.y_label_freq_ms2_asqp);
					a_s_rms_radio.Text = GetString(Resource.String.y_label_freq_ms2_asrms);
					p_s_radio.Text = GetString(Resource.String.y_label_freq_ms2_ps);
					p_s_d_radio.Text = GetString(Resource.String.y_label_freq_ms2_psd);
					a_s_d_radio.Text = GetString(Resource.String.y_label_freq_ms2_asd);
				}
			}
			else
			{
				if (velocityRadio.Checked)
				{
					a_s_q_p_radio.Text = GetString(Resource.String.y_label_freq_ms_asqp);
					a_s_rms_radio.Text = GetString(Resource.String.y_label_freq_ms_asrms);
					p_s_radio.Text = GetString(Resource.String.y_label_freq_ms_ps);
					p_s_d_radio.Text = GetString(Resource.String.y_label_freq_ms_psd);
					a_s_d_radio.Text = GetString(Resource.String.y_label_freq_ms_asd);
				}
				else
				{
					a_s_q_p_radio.Text = GetString(Resource.String.y_label_freq_m_asqp);
					a_s_rms_radio.Text = GetString(Resource.String.y_label_freq_m_asrms);
					p_s_radio.Text = GetString(Resource.String.y_label_freq_m_ps);
					p_s_d_radio.Text = GetString(Resource.String.y_label_freq_m_psd);
					a_s_d_radio.Text = GetString(Resource.String.y_label_freq_m_asd);
				}
				layout_hp.Visibility = ViewStates.Gone;
				layout_lp.Visibility = ViewStates.Gone;
				panelResolution2.Visibility = ViewStates.Gone;
			}
			a_s_q_p_radio.Checked = GlobalPreferences.a_s_q_p_radio;
			a_s_rms_radio.Checked = GlobalPreferences.a_s_rms_radio;
			p_s_radio.Checked = GlobalPreferences.p_s_radio;
			p_s_d_radio.Checked = GlobalPreferences.p_s_d_radio;
			a_s_d_radio.Checked = GlobalPreferences.a_s_d_radio;

			if (!GlobalPreferences.a_s_q_p_radio && !GlobalPreferences.a_s_rms_radio && !GlobalPreferences.p_s_radio && !GlobalPreferences.p_s_d_radio && !GlobalPreferences.a_s_d_radio)
			{
				p_s_d_radio.Checked = true;
			}

			rectangular_radio.Checked = GlobalPreferences.rectangular_radio;
			hanning_radio.Checked = GlobalPreferences.hanning_radio;
			flattop_radio.Checked = GlobalPreferences.flattop_radio;

			if (!GlobalPreferences.rectangular_radio && !GlobalPreferences.hanning_radio && !GlobalPreferences.flattop_radio)
			{
				rectangular_radio.Checked = true;
			}

			no_scaling.Checked = GlobalPreferences.no_scaling;
			chart_scaling.Checked = GlobalPreferences.chart_scaling;

			avginput.Text = GlobalPreferences.avginput.ToString();
			if (Objects.Equals(GlobalPreferences.avginput, "false"))
			{
				avginput.Text = "10";
				HidCom.SetAverageDepthUpdate(10);
			}
			//Cannot be equal to 0
			if (Objects.Equals(GlobalPreferences.avginput, 0))
			{
				avginput.Text = "10";
				HidCom.SetAverageDepthUpdate(10);
			}

			fftinput.Text = GlobalPreferences.fftinput.ToString();
			if (Objects.Equals(GlobalPreferences.fftinput, "false"))
			{
				fftinput.Text = "512";
				HidCom.SetFft_Window_Size_Update(512);
			}
			//Cannot be equal to 0
			if (Objects.Equals(GlobalPreferences.fftinput, 0))
			{
				fftinput.Text = "512";
				HidCom.SetFft_Window_Size_Update(512);
			}

			editTau.Text = GlobalPreferences.EditTau.ToString();
			if (Objects.Equals(GlobalPreferences.EditTau, "false"))
				editTau.Text = "2.0";
			//Cannot be equal to 0
			if (Objects.Equals(GlobalPreferences.EditTau, 0.0))
				editTau.Text = "2.0";

			valRmsExpo.SetProgress(GetConvertedFloatValue(Float.ParseFloat(editTau.Text) - minimumExponentialRms), true);

			editPeriodShow.Text = ((int)GlobalPreferences.EditPeriodShow).ToString();
			if (Objects.Equals(GlobalPreferences.EditPeriodShow, "false"))
			{
				editPeriodShow.Text = "2";
			}
			//Cannot be equal to 0
			if (Objects.Equals(GlobalPreferences.EditPeriodShow, 0))
			{
				editPeriodShow.Text = "2";
			}

			editPeriodKeep.Text = GlobalPreferences.EditPeriodKeep.ToString();
			if (Objects.Equals(GlobalPreferences.EditPeriodKeep, "false"))
				editPeriodKeep.Text = "4";
			//Cannot be equal to 0
			if (Objects.Equals(GlobalPreferences.EditPeriodKeep, 0))
				editPeriodKeep.Text = "4";

			editPeriodSkipNext.Text = GlobalPreferences.EditPeriodSkipNext.ToString();
			if (Objects.Equals(GlobalPreferences.EditPeriodSkipNext, "false"))
				editPeriodSkipNext.Text = "10";
			//Cannot be equal to 0
			if (Objects.Equals(GlobalPreferences.EditPeriodSkipNext, 0))
				editPeriodSkipNext.Text = "10";

			//hide or show this menu
			if (GlobalPreferences.Signal_Proc_Hide)
			{
				signal_proc_grp.Visibility = ViewStates.Gone;
				signal_proc_exp.SetImageDrawable(exp_dr_more);
			}
			else
			{
				signal_proc_grp.Visibility = ViewStates.Visible;
				signal_proc_exp.SetImageDrawable(exp_dr_less);
			}

			Enable_time_domain(GlobalPreferences.T_switch);
			Enable_freq_domain(GlobalPreferences.F_switch);

			/*
		   * Clicking on a text field in the menu open a dialog with explanation
		   */
			resolutionText.Click += (args, e) =>
			 {
				 InfoShowDialog(3);
			 };
			signalProcText.Click += (args, e) =>
			{
				InfoShowDialog(4);
			};
			valuesText.Click += (args, e) =>
			{
				InfoShowDialog(5);
			};
			unitsText.Click += (args, e) =>
			{
				InfoShowDialog(6);
			};
			showText.Click += (args, e) =>
			{
				InfoShowDialog(7);
			};
			keepText.Click += (args, e) =>
			{
				InfoShowDialog(8);
			};
			autoscaleText.Click += (args, e) =>
			{
				InfoShowDialog(9);
			};
			rmsExpoText.Click += (args, e) =>
			{
				InfoShowDialog(10);
			};
			wSizeText.Click += (args, e) =>
			{
				InfoShowDialog(11);
			};
			avDepthText.Click += (args, e) =>
			{
				InfoShowDialog(12);
			};
			skipNextText.Click += (args, e) =>
			{
				InfoShowDialog(13);
			};
			window_function.Click += (args, e) =>
			{
				InfoShowDialog(14);
			};
			fft_based_computations.Click += (args, e) =>
			{
				InfoShowDialog(15);
			};

			dialogButton = (Button)dialog.FindViewById(Resource.Id.btn_dialog);
			dialog.SetOnCancelListener(this);

			dialogButton.Click += (args, e) =>
			{
				if (!flagStartActivate)
				{

					if (!CheckMySerial(HidCom.Sn()))
					{
						if (HidCom.Get_input_Choice() == INPUT_CHOICE.REAL_VALUES)
						{
							HidCom.SetSensitivity((byte)0x02);
							HidCom.Set_2g_sensitivity(true);
							GlobalPreferences.g_2 = true;
							GlobalPreferences.g_6 = false;
						}
					}
					else
					{

						if (twog_radio.Checked)
						{
							if (HidCom.Get_input_Choice() == INPUT_CHOICE.REAL_VALUES)
							{
								HidCom.SetSensitivity((byte)0x02);
								HidCom.Set_2g_sensitivity(true);
								GlobalPreferences.g_2 = true;
								GlobalPreferences.g_6 = false;
							}

						}
						else
						{
							if (HidCom.Get_input_Choice() == INPUT_CHOICE.REAL_VALUES)
							{
								HidCom.SetSensitivity((byte)0x02);
								HidCom.Set_2g_sensitivity(false);
								GlobalPreferences.g_2 = false;
								GlobalPreferences.g_6 = true;
							}
						}
					}
					if (gunits_radio.Checked)
					{
						HidCom.SetgUnits(true);
						GlobalPreferences.gunits = true;
						GlobalPreferences.ms2units = false;
					}
					else
					{
						HidCom.SetgUnits(false);
						GlobalPreferences.gunits = false;
						GlobalPreferences.ms2units = true;
					}

				}
				if (chart_scaling.Checked)
				{
					HidCom.SetyAutoScale(true);
					buttonScale.Alpha = 1f;
					GlobalPreferences.chart_scaling = true;
					GlobalPreferences.data_scaling = false;
					GlobalPreferences.no_scaling = true;
				}
				else
				{
					HidCom.SetyAutoScale(false);
					buttonScale.Alpha = 0.3f;
					GlobalPreferences.chart_scaling = false;
					GlobalPreferences.data_scaling = false;
					GlobalPreferences.no_scaling = true;
				}

				GlobalPreferences.Ax_cb = Ax_cb.Checked;
				GlobalPreferences.Ay_cb = Ay_cb.Checked;
				GlobalPreferences.Az_cb = Az_cb.Checked;

				GlobalPreferences.acc_radio = accelerationRadio.Checked;
				GlobalPreferences.hpfilterCb = hpfilterCb.Checked;
				GlobalPreferences.fc_hp = local_hp_fc;
				GlobalPreferences.lpfilterCb = lpfilterCb.Checked;
				GlobalPreferences.fc_lp = local_lp_fc;
				GlobalPreferences.vel_radio = velocityRadio.Checked;
				GlobalPreferences.fc_vel = local_velocity_fc;
				GlobalPreferences.disp_radio = displacementRadio.Checked;
				GlobalPreferences.fc_dis = local_displacement_fc;

				GlobalPreferences.a_s_q_p_radio = a_s_q_p_radio.Checked;
				GlobalPreferences.a_s_rms_radio = a_s_rms_radio.Checked;
				GlobalPreferences.p_s_radio = p_s_radio.Checked;
				GlobalPreferences.p_s_d_radio = p_s_d_radio.Checked;
				GlobalPreferences.a_s_d_radio = a_s_d_radio.Checked;

				GlobalPreferences.rectangular_radio = rectangular_radio.Checked;
				GlobalPreferences.hanning_radio = hanning_radio.Checked;
				GlobalPreferences.flattop_radio = flattop_radio.Checked;

				GlobalPreferences.avginput = int.Parse(avginput.Text);
				GlobalPreferences.fftinput = int.Parse(fftinput.Text);

				GlobalPreferences.EditTau = Float.ParseFloat(editTau.Text);

				GlobalPreferences.EditPeriodKeep = int.Parse(editPeriodKeep.Text);
				GlobalPreferences.EditPeriodShow = int.Parse(editPeriodShow.Text);
				GlobalPreferences.EditPeriodSkipNext = int.Parse(editPeriodSkipNext.Text);

				//Filter
				if (accelerationRadio.Checked)
				{
					HidCom.Set_mode_Choice(MODE_CHOICE.ACCELERATION_MODE);
					if (hpfilterCb.Checked)
					{
						if (lpfilterCb.Checked)
						{
							HidCom.Set_filter_Choice(FILTER_CHOICE.HP_LP);
						}
						else
						{
							HidCom.Set_filter_Choice(FILTER_CHOICE.HP);
						}
					}
					else
					{
						if (lpfilterCb.Checked)
						{
							HidCom.Set_filter_Choice(FILTER_CHOICE.LP);
						}
						else
						{
							HidCom.Set_filter_Choice(FILTER_CHOICE.NO_FILTER);
						}
					}
				}
				else if (velocityRadio.Checked)
				{
					HidCom.Set_mode_Choice(MODE_CHOICE.VELOCITY_MODE);
				}
				else
				{
					HidCom.Set_mode_Choice(MODE_CHOICE.DISPLACEMENT_MODE);
				}

				//windowing
				if (rectangular_radio.Checked)
				{
					HidCom.Set_window_Choice(WINDOW_CHOICE.RECTANGULAR_WINDOW);
					HidCom.Set_window_npbw(1.0f);
				}
				else if (hanning_radio.Checked)
				{
					HidCom.Set_window_Choice(WINDOW_CHOICE.HANNING_WINDOW);
					HidCom.Set_window_npbw(1.5f);
				}
				else
				{
					HidCom.Set_window_Choice(WINDOW_CHOICE.FLAT_TOP_WINDOW);
					HidCom.Set_window_npbw(3.77f);
				}

				//FFT analysis
				if (a_s_q_p_radio.Checked)
				{
					HidCom.Set_freq_Choice_Update(FREQUENTIAL_CHOICE.AMPLITUDE_SPECTRUM_PEAK);
				}
				else if (a_s_rms_radio.Checked)
				{
					HidCom.Set_freq_Choice_Update(FREQUENTIAL_CHOICE.AMPLITUDE_SPECTRUM_RMS);
				}
				else if (p_s_radio.Checked)
				{
					HidCom.Set_freq_Choice_Update(FREQUENTIAL_CHOICE.POWER_SPECTRUM);
				}
				else if (p_s_d_radio.Checked)
				{
					HidCom.Set_freq_Choice_Update(FREQUENTIAL_CHOICE.POWER_SPECTRAL_DENSITY);
				}
				else
				{
					HidCom.Set_freq_Choice_Update(FREQUENTIAL_CHOICE.AMPLITUDE_SPECTRAL_DENSITY);
				}

				if (!GlobalPreferences.Ax_cb)
				{
					if (chartSurface_T.RenderableSeries.Contains(Flrs_Ax)) chartSurface_T.RenderableSeries.Remove(Flrs_Ax);
					if (chartSurface_F.RenderableSeries.Contains(Flrs_Fft_Ax)) chartSurface_F.RenderableSeries.Remove(Flrs_Fft_Ax);
				}
				else
				{
					if (!chartSurface_T.RenderableSeries.Contains(Flrs_Ax)) chartSurface_T.RenderableSeries.Add(Flrs_Ax);
					if (!chartSurface_F.RenderableSeries.Contains(Flrs_Fft_Ax)) chartSurface_F.RenderableSeries.Add(Flrs_Fft_Ax);
				}
				if (!GlobalPreferences.Ay_cb)
				{
					if (chartSurface_T.RenderableSeries.Contains(Flrs_Ay)) chartSurface_T.RenderableSeries.Remove(Flrs_Ay);
					if (chartSurface_F.RenderableSeries.Contains(Flrs_Fft_Ay)) chartSurface_F.RenderableSeries.Remove(Flrs_Fft_Ay);
				}
				else
				{
					if (!chartSurface_T.RenderableSeries.Contains(Flrs_Ay)) chartSurface_T.RenderableSeries.Add(Flrs_Ay);
					if (!chartSurface_F.RenderableSeries.Contains(Flrs_Fft_Ay)) chartSurface_F.RenderableSeries.Add(Flrs_Fft_Ay);
				}
				if (!GlobalPreferences.Az_cb)
				{
					if (chartSurface_T.RenderableSeries.Contains(Flrs_Az)) chartSurface_T.RenderableSeries.Remove(Flrs_Az);
					if (chartSurface_F.RenderableSeries.Contains(Flrs_Fft_Az)) chartSurface_F.RenderableSeries.Remove(Flrs_Fft_Az);
				}
				else
				{
					if (!chartSurface_T.RenderableSeries.Contains(Flrs_Az)) chartSurface_T.RenderableSeries.Add(Flrs_Az);
					if (!chartSurface_F.RenderableSeries.Contains(Flrs_Fft_Az)) chartSurface_F.RenderableSeries.Add(Flrs_Fft_Az);
				}

				switch (HidCom.Get_mode_Choice())
				{
					case MODE_CHOICE.ACCELERATION_MODE:
						if (!HidCom.GetgUnits())
						{
							switch (HidCom.Get_freq_Choice_Update())
							{
								case FREQUENTIAL_CHOICE.AMPLITUDE_SPECTRUM_PEAK:
									yAxisF.AxisTitle = GetString(Resource.String.y_label_freq_ms2_asqp);
									break;
								case FREQUENTIAL_CHOICE.AMPLITUDE_SPECTRUM_RMS:
									yAxisF.AxisTitle = GetString(Resource.String.y_label_freq_ms2_asrms);
									break;
								case FREQUENTIAL_CHOICE.AMPLITUDE_SPECTRAL_DENSITY:
									yAxisF.AxisTitle = GetString(Resource.String.y_label_freq_ms2_asd);
									break;
								case FREQUENTIAL_CHOICE.POWER_SPECTRUM:
									yAxisF.AxisTitle = GetString(Resource.String.y_label_freq_ms2_ps);
									break;
								case FREQUENTIAL_CHOICE.POWER_SPECTRAL_DENSITY:
									yAxisF.AxisTitle = GetString(Resource.String.y_label_freq_ms2_psd);
									break;
								default:
									break;
							}
							yAxisT.AxisTitle = GetString(Resource.String.y_label_time_ms2);
						}
						else
						{
							switch (HidCom.Get_freq_Choice_Update())
							{
								case FREQUENTIAL_CHOICE.AMPLITUDE_SPECTRUM_PEAK:
									yAxisF.AxisTitle = GetString(Resource.String.y_label_freq_g_asqp);
									break;
								case FREQUENTIAL_CHOICE.AMPLITUDE_SPECTRUM_RMS:
									yAxisF.AxisTitle = GetString(Resource.String.y_label_freq_g_asrms);
									break;
								case FREQUENTIAL_CHOICE.AMPLITUDE_SPECTRAL_DENSITY:
									yAxisF.AxisTitle = GetString(Resource.String.y_label_freq_g_asd);
									break;
								case FREQUENTIAL_CHOICE.POWER_SPECTRUM:
									yAxisF.AxisTitle = GetString(Resource.String.y_label_freq_g_ps);
									break;
								case FREQUENTIAL_CHOICE.POWER_SPECTRAL_DENSITY:
									yAxisF.AxisTitle = GetString(Resource.String.y_label_freq_g_psd);
									break;
								default:
									break;
							}
							yAxisT.AxisTitle = GetString(Resource.String.y_label_time_g);
						}
						break;
					case MODE_CHOICE.VELOCITY_MODE:
						switch (HidCom.Get_freq_Choice_Update())
						{
							case FREQUENTIAL_CHOICE.AMPLITUDE_SPECTRUM_PEAK:
								yAxisF.AxisTitle = GetString(Resource.String.y_label_freq_ms_asqp);
								break;
							case FREQUENTIAL_CHOICE.AMPLITUDE_SPECTRUM_RMS:
								yAxisF.AxisTitle = GetString(Resource.String.y_label_freq_ms_asrms);
								break;
							case FREQUENTIAL_CHOICE.AMPLITUDE_SPECTRAL_DENSITY:
								yAxisF.AxisTitle = GetString(Resource.String.y_label_freq_ms_asd);
								break;
							case FREQUENTIAL_CHOICE.POWER_SPECTRUM:
								yAxisF.AxisTitle = GetString(Resource.String.y_label_freq_ms_ps);
								break;
							case FREQUENTIAL_CHOICE.POWER_SPECTRAL_DENSITY:
								yAxisF.AxisTitle = GetString(Resource.String.y_label_freq_ms_psd);
								break;
							default:
								break;
						}
						yAxisT.AxisTitle = GetString(Resource.String.y_label_time_ms);
						break;
					case MODE_CHOICE.DISPLACEMENT_MODE:
						switch (HidCom.Get_freq_Choice_Update())
						{
							case FREQUENTIAL_CHOICE.AMPLITUDE_SPECTRUM_PEAK:
								yAxisF.AxisTitle = GetString(Resource.String.y_label_freq_m_asqp);
								break;
							case FREQUENTIAL_CHOICE.AMPLITUDE_SPECTRUM_RMS:
								yAxisF.AxisTitle = GetString(Resource.String.y_label_freq_m_asrms);
								break;
							case FREQUENTIAL_CHOICE.AMPLITUDE_SPECTRAL_DENSITY:
								yAxisF.AxisTitle = GetString(Resource.String.y_label_freq_m_asd);
								break;
							case FREQUENTIAL_CHOICE.POWER_SPECTRUM:
								yAxisF.AxisTitle = GetString(Resource.String.y_label_freq_m_ps);
								break;
							case FREQUENTIAL_CHOICE.POWER_SPECTRAL_DENSITY:
								yAxisF.AxisTitle = GetString(Resource.String.y_label_freq_m_psd);
								break;
							default:
								break;
						}
						yAxisT.AxisTitle = GetString(Resource.String.y_label_time_m);
						break;
				}
				//Time/Frequency
				Enable_time_domain(GlobalPreferences.T_switch);
				Enable_freq_domain(GlobalPreferences.F_switch);

				HidCom.Set_hp_filter_fc(local_hp_fc);
				HidCom.Set_lp_filter_fc(local_lp_fc);
				HidCom.Set_vel_filter_fc(local_velocity_fc);
				HidCom.Set_disp_filter_fc(local_displacement_fc);
				HidCom.SetAverageDepthUpdate(Integer.ParseInt(avginput.Text.ToString()));
				HidCom.SetFft_Window_Size_Update(Integer.ParseInt(fftinput.Text.ToString()));
				HidCom.SetnPointsToShow((int)((double)HidCom.GetSampling_Freq() * Java.Lang.Double.ParseDouble(editPeriodShow.Text.ToString()))); // to set n point to show
				HidCom.SetnPointsToKeep(HidCom.GetSampling_Freq() * Integer.ParseInt(editPeriodKeep.Text.ToString())); // to set n point to keep
				HidCom.SetSkipNextValue(Integer.ParseInt(editPeriodSkipNext.Text.ToString()));
				HidCom.SetRms_tau(Float.ParseFloat(editTau.Text.ToString()));
				dialog.Dismiss();
				//Toast to warn for narrow bandwith
				if (HidCom.Get_filter_Choice() == FILTER_CHOICE.HP_LP)
				{
					double lpf = HidCom.Get_lp_filter_fc();
					double hpf = HidCom.Get_hp_filter_fc();
					if ((lpf - hpf) < BANDPASS_WARNING_THRESHOLD)
					{
						Toast.MakeText(this, GetString(Resource.String.narrow_bandwith), ToastLength.Short).Show();
					}
				}
			};
			dialog.Window.SetLayout(ScreenWidth, ScreenHeight);
			dialog.Show();
		}
		void NumShowDialog(double min, double max, TextView tv, int type)
		{
			Dialog dialog = new Dialog(this);
			dialog.RequestWindowFeature((int)WindowFeatures.NoTitle);
			dialog.SetCancelable(true);
			dialog.SetContentView(Resource.Layout.num_dialog);
			TextView desc = (TextView)dialog.FindViewById(Resource.Id.description_tv);
			EditText dec_input = (EditText)dialog.FindViewById(Resource.Id.dec_input);
			Button NumDialogButton = (Button)dialog.FindViewById(Resource.Id.btn_dialog);
			switch (type)
			{
				case 1:
					desc.Text = GetString(Resource.String.edit_hp_fc_desc) + "\n\n[Min = " + min + " ; Max = " + max + "]";
					dec_input.Hint = GetString(Resource.String.cutoff_frequency);
					break;
				case 2:
					desc.Text = GetString(Resource.String.edit_lp_fc_desc) + "\n\n[Min = " + min + " ; Max = " + max + "]";
					dec_input.Hint = GetString(Resource.String.cutoff_frequency);
					break;
				case 3:
					desc.Text = GetString(Resource.String.edit_vel_fc_desc) + "\n\n[Min = " + min + " ; Max = " + max + "]";
					dec_input.Hint = GetString(Resource.String.cutoff_frequency);
					break;
				case 4:
					desc.Text = GetString(Resource.String.edit_disp_fc_desc) + "\n\n[Min = " + min + " ; Max = " + max + "]";
					dec_input.Hint = GetString(Resource.String.cutoff_frequency);
					break;
				case 5:
					desc.Text = GetString(Resource.String.edit_visible_range_desc) + "\n\n[Min = " + min + " ; Max = " + max + "]";
					dec_input.Hint = GetString(Resource.String.period_show_title) + " [s]";
					break;

			}
			NumDialogButton.Click += (args, e) =>
			 {
				 double value;
				 string dec = dec_input.Text.ToString();
				 if (!string.IsNullOrEmpty(dec))
				 {
					 value = Java.Lang.Double.ParseDouble(dec);
					 if (value >= min && value <= max)
					 {
						 switch (type)
						 {
							 case 1:
								 try
								 {
									 local_hp_fc = value;
									 tv.Text = GetString(Resource.String.filer_hp_high_pass) + " (" + GetString(Resource.String.fc_label) + " " + value + " [Hz])";
									 // it means it is double
								 }
								 catch (System.Exception e1)
								 {
									 // this means it is not double
									 string error = e1.StackTrace;
								 }
								 break;
							 case 2:
								 try
								 {
									 local_lp_fc = value;
									 tv.Text = GetString(Resource.String.filer_low_pass) + " (" + GetString(Resource.String.fc_label) + " " + value + " [Hz])";
									 // it means it is double
								 }
								 catch (Exception e1)
								 {
									 // this means it is not double
									 string error = e1.StackTrace;
								 }
								 break;
							 case 3:
								 try
								 {
									 local_velocity_fc = value;
									 tv.Text = GetString(Resource.String.filer_velocity_mode) + " (" + GetString(Resource.String.fc_label) + " " + value + " [Hz])";
									 // it means it is double
								 }
								 catch (Exception e1)
								 {
									 // this means it is not double
									 string error = e1.StackTrace;
								 }
								 break;
							 case 4:
								 try
								 {
									 local_displacement_fc = value;
									 tv.Text = GetString(Resource.String.filer_displacement_mode) + " (" + GetString(Resource.String.fc_label) + " " + value + " [Hz])";
									 // it means it is double
								 }
								 catch (Exception e1)
								 {
									 // this means it is not double
									 string error = e1.StackTrace;
								 }
								 break;
							 case 5:
								 tv.Text = value.ToString();
								 break;
						 }
						 dialog.Dismiss();
					 }
					 else
					 {
						 Toast.MakeText(this, GetString(Resource.String.fc_out_of_bnds), ToastLength.Short).Show();
					 }
				 };
			 };
			dialog.Window.SetLayout(ScreenWidth, ScreenHeight);
			dialog.Show();
		}
		void PickDialShowDialog(int min, int max, TextView tv, int type)
		{
			Dialog dialog = new Dialog(this);
			dialog.RequestWindowFeature((int)WindowFeatures.NoTitle);
			dialog.SetCancelable(true);
			dialog.SetContentView(Resource.Layout.pick_dialog);
			TextView desc = (TextView)dialog.FindViewById(Resource.Id.description_tv);
			EditText int_input = (EditText)dialog.FindViewById(Resource.Id.int_input);
			Button dialogButton = (Button)dialog.FindViewById(Resource.Id.btn_dialog);
			switch (type)
			{
				case 1:
					desc.Text = GetString(Resource.String.edit_fifo_depth_desc) + "\n\n[Min = " + min + " ; Max = " + max + "]";
					int_input.Hint = GetString(Resource.String.avg_dpt_title) + " [-]";
					break;
				case 2:
					desc.Text = GetString(Resource.String.edit_fft_size_desc) + "\n\n[Min = " + min + " ; Max = " + max + "]";
					int_input.Hint = GetString(Resource.String.fft_w_size_title) + " [" + GetString(Resource.String.sample) + "]";
					break;
				case 3:
					desc.Text = GetString(Resource.String.edit_visible_range_desc) + "\n\n[Min = " + min + " ; Max = " + max + "]";
					int_input.Hint = GetString(Resource.String.period_show_title) + " [s]";
					break;
				case 4:
					desc.Text = GetString(Resource.String.edit_data_range_desc) + "\n\n[Min = " + min + " ; Max = " + max + "]";
					int_input.Hint = GetString(Resource.String.period_keep_title) + " [s]";
					break;
				case 5:
					desc.Text = GetString(Resource.String.edit_skip_next_desc) + "\n\n[Min = " + min + " ; Max = " + max + "]";
					int_input.Hint = GetString(Resource.String.skip_next_title) + " [s]";
					break;
			}
			dialogButton.Click += (args, e) =>
			 {
				 int value;
				 string dec = int_input.Text.ToString();
				 if (!string.IsNullOrEmpty(dec))
				 {
					 value = Integer.ParseInt(dec);
					 if (value >= min && value <= max)
					 {
						 tv.Text = value.ToString();
						 dialog.Dismiss();
					 }
					 else
					 {
						 Toast.MakeText(this, GetString(Resource.String.fc_out_of_bnds), ToastLength.Short).Show();
					 }
				 }
			 };
			dialog.Window.SetLayout(ScreenWidth, ScreenHeight);
			dialog.Show();
		}
		void InfoShowDialog(int param)
		{
			Dialog dialog = new Dialog(this);
			dialog.RequestWindowFeature((int)WindowFeatures.NoTitle);
			dialog.SetCancelable(true);
			dialog.SetContentView(Resource.Layout.info_dialog);
			TextView param_title = (TextView)dialog.FindViewById(Resource.Id.param_title);
			TextView param_def = (TextView)dialog.FindViewById(Resource.Id.param_def);
			Button dialogButton = (Button)dialog.FindViewById(Resource.Id.btn_dialog);
			switch (param)
			{
				case 1:
					param_title.Text = GetString(Resource.String.time_limit_param_title);
					param_def.Text = GetString(Resource.String.time_limit_param_def);
					break;
				case 2:
					param_title.Text = GetString(Resource.String.csv_configuration_title);
					param_def.Text = GetString(Resource.String.csv_configuration_def);
					break;
				case 3:
					param_title.Text = GetString(Resource.String.resolution_title);
					param_def.Text = GetString(Resource.String.resolution_def);
					break;
				case 4:
					param_title.Text = GetString(Resource.String.signal_proc_title);
					param_def.Text = GetString(Resource.String.signal_proc_def);
					break;
				case 5:
					param_title.Text = GetString(Resource.String.value_diplay_title);
					param_def.Text = GetString(Resource.String.value_diplay_def);
					break;
				case 6:
					param_title.Text = GetString(Resource.String.units_title);
					param_def.Text = GetString(Resource.String.units_def);
					break;
				case 7:
					param_title.Text = GetString(Resource.String.period_show_title);
					param_def.Text = GetString(Resource.String.period_show_def);
					break;
				case 8:
					param_title.Text = GetString(Resource.String.period_keep_title);
					param_def.Text = GetString(Resource.String.period_keep_def);
					break;
				case 9:
					param_title.Text = GetString(Resource.String.autoscale_title);
					param_def.Text = GetString(Resource.String.autoscale_def);
					break;
				case 10:
					param_title.Text = GetString(Resource.String.rms_expo_title);
					param_def.Text = GetString(Resource.String.rms_expo_def);
					break;
				case 11:
					param_title.Text = GetString(Resource.String.fft_w_size_title);
					param_def.Text = GetString(Resource.String.fft_w_size_def);
					break;
				case 12:
					param_title.Text = GetString(Resource.String.avg_dpt_title);
					param_def.Text = GetString(Resource.String.avg_dpt_def);
					break;
				case 13:
					param_title.Text = GetString(Resource.String.skip_next_title);
					param_def.Text = GetString(Resource.String.skip_next_def);
					break;
				case 14:
					param_title.Text = GetString(Resource.String.window_function_title);
					param_def.Text = GetString(Resource.String.window_function_def);
					break;
				case 15:
					param_title.Text = GetString(Resource.String.fft_based_computations_title);
					param_def.Text = GetString(Resource.String.fft_based_computations_def);
					break;
				default:
					break;
			}
			dialogButton.Click += (args, e) =>
			 {
				 dialog.Dismiss();
			 };
			/*To set the dialog to the entire phone screen */
			dialog.Window.SetLayout(ScreenWidth, ScreenHeight);
			dialog.Show();
		}
		#endregion Dialogs

		#region Methods
		public bool IsExternalStorageWritable()
		{
			string state = Android.OS.Environment.GetExternalStorageState(null);
			if (Android.OS.Environment.MediaMounted.Equals(state))
			{
				return true;
			}
			return false;
		}
		public static bool HasPermissions(Context context, string[] permissions)
		{
			if (permissions != null)
			{
				foreach (string permission in permissions)
				{
					if (ActivityCompat.CheckSelfPermission(context, permission) != Android.Content.PM.Permission.Granted)
					{
						return false;
					}
				}
			}
			return true;
		}
		private bool full_screen = false;
		private void Enable_full_screen(bool en)
		{
			if (en)
			{
				buttonFS.Alpha = 1f;
				controlsLayout.Visibility = ViewStates.Gone;
				rmsLayout.Visibility = ViewStates.Gone;
				SetActionBar(null);
				lytToolBar.Visibility = ViewStates.Gone;
			}
			else
			{
				buttonFS.Alpha = 0.3f;
				controlsLayout.Visibility = ViewStates.Visible;
				rmsLayout.Visibility = ViewStates.Visible;
				lytToolBar.Visibility = ViewStates.Visible;
			}
			full_screen = en;
		}
		//Retrieve params from RecoVIB Feel
		public static void DisableImButton(ImageButton button)
		{
			button.Clickable = false;
			button.Alpha = 0.3f;
		}
		public static void EnableImButton(ImageButton button)
		{
			button.Clickable = true;
			button.Alpha = 1f;
		}
		public static void ViewerMode(bool value)
		{
			if (value)
			{
				HidCom.Set_input_Choice(INPUT_CHOICE.RECORDED_VALUES);
			}
			else
			{
				HidCom.Set_input_Choice(INPUT_CHOICE.REAL_VALUES);
			}
		}
		private void Enable_time_domain(bool en)
		{
			if (en)
			{
				try
				{
					buttonT.Alpha = 1f;
					chartSurface_T.Visibility = ViewStates.Visible;
					//chartSurface_T.RenderSurface = new RenderSurfaceGL(this);
					//chartSurface_T.RenderSurface = new RenderSurfaceGL(this);
					//chartSurface_T.SetRenderSurface(new RenderSurfaceGL(this));
				}
				catch (Exception ex)
				{
					ex.PrintStackTrace();
				}
			}
			else
			{
				if (GlobalPreferences.F_switch)
				{
					buttonT.Alpha = 0.3f;
					chartSurface_T.Visibility = ViewStates.Gone;
					//chartSurface_T.RenderSurface = null;
				}
				else
				{
					GlobalPreferences.T_switch = true;
					buttonT.Alpha = 1f;
					chartSurface_T.Visibility = ViewStates.Visible;
					//chartSurface_T.RenderSurface = new RenderSurfaceGL(this);
				}
			}
		}
		private void Enable_freq_domain(bool en)
		{
			if (en)
			{
				buttonF.Alpha = 1f;
				chartSurface_F.Visibility = ViewStates.Visible;
				//chartSurface_F.RenderSurface = new RenderSurfaceGL(this);
			}
			else
			{
				if (GlobalPreferences.T_switch)
				{
					buttonF.Alpha = 0.3f;
					chartSurface_F.Visibility = ViewStates.Gone;
					//chartSurface_F.RenderSurface = null;
				}
				else
				{
					GlobalPreferences.F_switch = true;
					buttonF.Alpha = 1f;
					chartSurface_F.Visibility = ViewStates.Visible;
					//chartSurface_F.RenderSurface = new RenderSurfaceGL(this);
				}
			}
		}
		public int GetConvertedFloatValue(float floatValue)
		{
			int converted = 0;
			converted = (int)floatValue * 10;
			return converted;

		}
		public bool CheckMySerial(string serial)
		{
			if (serial != null && serial.Length > 2)
			{
				if (serial.Substring(2, 2).Equals("80"))
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		public static void DisableRadio(RadioButton button)
		{
			button.Enabled = false;
			button.Alpha = 0.3f;
		}
		public float GetConvertedValue(int intVal)
		{
			float floatVal = (float)0.0;
			floatVal = .1f * intVal;
			return floatVal;
		}
		public void OnProgressChanged(SeekBar seekBar, int progress, bool fromUser)
		{
			if (fromUser)
			{
				editTau.Text = (GetConvertedValue(progress) + minimumExponentialRms).ToString();
			}
		}
		public void OnStartTrackingTouch(SeekBar seekBar)
		{

		}
		public void OnStopTrackingTouch(SeekBar seekBar)
		{

		}
		public void OnCancel(IDialogInterface dialog)
		{
			dialogButton.CallOnClick();
		}
		public void OnClick(View v)
		{
			switch (v.Id)
			{
				case Resource.Id.recordButton:
					{
						if (!HasPermissions(this, PERMISSIONS))
						{
							Toast.MakeText(this, GetString(Resource.String.permission_needed), ToastLength.Short).Show();
							ActivityCompat.RequestPermissions(this, PERMISSIONS, PERMISSION_ALL);
						}
						else
						{
							//time domain
							myRecordView = v;
							//if no recording is underway
							//*Create a new bin file
							//*Start the Recording time-limit timer (handler)
							//*Set the RecordTemp Flag to true to start writing values in HidCom
							//*Start to toggle the recording button to let the user know that recording has started
							//if a recording is underway
							//*Stop recording in HidCom
							//*Stop the recording button animation
							if (!recordFlag)
							{
								if (IsExternalStorageWritable())
								{
									string baseDir = Android.OS.Environment.GetExternalStoragePublicDirectory(Android.OS.Environment.DirectoryDocuments).AbsolutePath;
									SimpleDateFormat sdf = new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss");
									SimpleDateFormat sdf2 = new SimpleDateFormat("yy-MM-dd HH:mm:ss");

									string folderName = "RecoVIB_Feel_App";
									string snFolderName = HidCom.Sn();
									string dtFolderName = sdf.Format(new Date());
									//viewer becomes basic to fit android projects definition
									string viewerFolderName = "basic";

									string folderPath = baseDir + File.Separator + folderName;
									string snFolderPath = folderPath + File.Separator + snFolderName;
									string dtFolderPath = snFolderPath + File.Separator + dtFolderName;
									string viewerFolderPath = dtFolderPath + File.Separator + viewerFolderName;

									string axg = string.Format((string)Locale.Us, "%.10f", HidCom.Get_Ax_Gain_2G());
									string ayg = string.Format((string)Locale.Us, "%.10f", HidCom.Get_ay_gain());
									string azg = string.Format((string)Locale.Us, "%.10f", HidCom.Get_az_gain());
									string axo = string.Format((string)Locale.Us, "%.10f", HidCom.Get_ax_offset());
									string ayo = string.Format((string)Locale.Us, "%.10f", HidCom.Get_ay_offset());
									string azo = string.Format((string)Locale.Us, "%.10f", HidCom.Get_az_offset());

									DT_start = new Date();
									DT_start_string = sdf2.Format(DT_start);

									folder = new File(folderPath);
									snFolder = new File(snFolderPath);
									dtFolder = new File(dtFolderPath);

									viewerFolder = new File(viewerFolderPath);

									File paramfile = new File(viewerFolder, "parameters.txt");
									//VIEWER becomes DATA to fit android projects definition
									binfile = new File(viewerFolder, "DATA.BIN");

									try
									{
										folder.Mkdirs();
										snFolder.Mkdirs();
										dtFolder.Mkdirs();
										viewerFolder.Mkdirs();
										FileOutputStream fp = new FileOutputStream(paramfile);
										PrintWriter pw = new PrintWriter(paramfile);
										pw.Println("Serial#AxGain#AyGain#AzGain#AxOffset#AyOffset#AzOffset");
										string oparams = HidCom.Sn() + "#" + axg + "#" + ayg + "#" + azg + "#" + axo + "#" + ayo + "#" + azo;
										pw.Println(oparams);
										pw.Flush();
										pw.Close();
										fp.Close();
										fosout = new FileOutputStream(binfile, true);
									}
									catch (Exception ex)
									{
										Log.Error(TAG, "File not found EXCEPTION" + ex);
									}
									HidCom.SetRecordTemp(true);
									//Stop button disabled when recording
									DisableImButton(stopButton);
								}
								//Graphic animation aspect
								Animation mAnimation = new AlphaAnimation(1, 0);
								mAnimation.Duration = 600;
								mAnimation.Interpolator = new LinearInterpolator();
								mAnimation.RepeatCount = Animation.Infinite;
								mAnimation.RepeatMode = RepeatMode.Reverse;
								recordButton.StartAnimation(mAnimation);
								recordFlag = true;
								//buttonTF.setVisibility(View.INVISIBLE);

							}
							else
							{
								HidCom.SetRecordTemp(false);
								//Stop button disabled when recording
								EnableImButton(stopButton);
								v.ClearAnimation();
								recordFlag = false;

								string serialnumber = HidCom.Sn();
								Date DT_stop = new Date();
								string sensitivity;

								HidCom.SetCloseFOS(true);

								switch (serialnumber.ElementAt(3))
								{
									case '0':
										if (HidCom.Get_2g_sensitivity())
										{
											sensitivity = "2g";
										}
										else
										{
											sensitivity = "6g";
										}
										break;
									case '1':
										sensitivity = "15g";
										break;
									case '2':
										sensitivity = "200g";
										break;
									default:
										sensitivity = "0g";
										break;
								}
								long duration = DT_stop.Time - DT_start.Time; //Time duration in ms
								duration = duration / 1000;//time duration in s
								int hours = (int)(duration / 3600);
								int minutes = (int)((duration % 3600) / 60);
								int seconds = (int)(duration % 60);
								string time_duration = string.Format("%02d:%02d:%02d", hours, minutes, seconds);
								File logfile = new File(viewerFolder, "times_log.txt");
								try
								{
									FileOutputStream fp = new FileOutputStream(logfile);
									PrintWriter pw = new PrintWriter(logfile);
									string log = "Serial:" + serialnumber + " Start at (yy-mm-dd hh:mm:ss):" + DT_start_string + " Time duration:" + time_duration + " Mode:" + sensitivity;
									pw.Println(log);
									pw.Flush();
									pw.Close();
									fp.Close();
								}
								catch (Exception e)
								{
									Log.Error(TAG, "File not found EXCEPTION" + e);
								}
							}

						}
					}
					break;

				case Resource.Id.buttonStart:
					{//If viewer mode :
					 //*Start the handler to detect the end of the file flag
					 //*if streaming running, perform a pause, replay, or play action depending on the current situation
					 //*else open a file browser to let the user select another file to read
					 //If streamer mode :
					 //*Start the streaming by calling HidCom.ext_Event_Start()
						if (HidCom.Get_input_Choice() == INPUT_CHOICE.RECORDED_VALUES)
						{
							if (flagStartActivate)
							{
								if (HidCom.Get_pause_Signal())
								{
									HidCom.Set_pause_Signal(false);
									startButton.SetImageDrawable(pauseDrawable);
									EnableImButton(skipNextButton);
									EnableImButton(stopButton);
									EnableImButton(ffButton);
								}
								else if (replayFlag)
								{
									Toast.MakeText(this, GetString(Resource.String.select_recovib_file), ToastLength.Short).Show();
									PerformFileSearch();
								}
								else
								{
									HidCom.Set_pause_Signal(true);
									startButton.SetImageDrawable(startDrawable);
									DisableImButton(skipNextButton);
									DisableImButton(ffButton);
								}
							}
							else
							{
								Toast.MakeText(this, GetString(Resource.String.select_recovib_file), ToastLength.Short).Show();
								PerformFileSearch();
							}
						}
						else
						{
							EnableImButton(stopButton);
							EnableImButton(recordButton);
							DisableImButton(startButton);
							flagStartActivate = true;
							HidCom.Set_stop_Signal(false);
							HidCom.Ext_Event_Start();
						}
					}
					break;

				case Resource.Id.buttonStop:
					{
						//if replayFlag set (end of file reached in viewer mode), relaunch the viewer with the same file
						//else :
						//*if a recording is underway, stop it
						//*if a streaming/viewing is underway, stop it by calling HidCom.ext_Event_Stop()
						if (replayFlag)
						{
							replayFlag = false;

							HidCom.Set_stop_Signal(true);
							try
							{
								istToRead = new FileInputStream(bin_file);
							}
							catch (Exception e)
							{
								Log.Error(TAG, "", "" + e.Message);
							}
							startButton.SetImageDrawable(pauseDrawable);
							stopButton.SetImageDrawable(stopDrawable);
							EnableImButton(skipNextButton);
							EnableImButton(stopButton);
							EnableImButton(ffButton);
							EnableImButton(startButton);
							flagStartActivate = true;
							HidCom.Set_stop_Signal(false);
							HidCom.Ext_Event_Start();
						}
						else
						{
							if (HidCom.Get_input_Choice() == INPUT_CHOICE.RECORDED_VALUES)
							{
								startButton.SetImageDrawable(startDrawable);
								DisableImButton(skipNextButton);
								DisableImButton(stopButton);
								DisableImButton(ffButton);
								EnableImButton(startButton);
								flagStartActivate = false;
								HidCom.Set_stop_Signal(true);
								HidCom.Ext_Event_Stop();
							}
							else
							{
								if (myRecordView != null)
								{
									myRecordView.ClearAnimation();
									recordFlag = false;
								}
								HidCom.SetRecordTemp(false);
								DisableImButton(stopButton);
								DisableImButton(recordButton);
								EnableImButton(startButton);

								flagStartActivate = false;

								HidCom.Set_stop_Signal(true);
								HidCom.Ext_Event_Stop();
							}
						}
					}
					break;

				case Resource.Id.btnSettings:
					SettingsDialog();
					break;
				case Resource.Id.btnInfo:
					{
						Dialog dialog = new Dialog(this);
						dialog.RequestWindowFeature((int)WindowFeatures.NoTitle);
						dialog.SetCancelable(true);
						dialog.SetContentView(Resource.Layout.about);

						TextView website = (TextView)dialog.FindViewById(Resource.Id.text_website);
						TextView license = (TextView)dialog.FindViewById(Resource.Id.text_license);
						license.PaintFlags = Android.Graphics.PaintFlags.UnderlineText;
						website.MovementMethod = LinkMovementMethod.Instance;
						Button dialogButton = (Button)dialog.FindViewById(Resource.Id.btn_dialog);
						dialogButton.Click += (args, e) =>
						{
							dialog.Dismiss();
						};
						license.Click += (args, e) =>
						{
							LicenceShowDialog();
						};

						dialog.Show();
					}
					break;
				case Resource.Id.buttonT:
					{
						if (!GlobalPreferences.T_switch)
						{
							GlobalPreferences.T_switch = true;
							Enable_time_domain(true);
						}
						else
						{
							GlobalPreferences.T_switch = false;
							Enable_time_domain(false);
						}
					}
					break;
				case Resource.Id.buttonF:
					{
						if (!GlobalPreferences.F_switch)
						{
							GlobalPreferences.F_switch = true;
							Enable_freq_domain(true);
						}
						else
						{
							GlobalPreferences.F_switch = false;
							Enable_freq_domain(false);
						}
					}
					break;
				case Resource.Id.skipNextButton:
					{
						HidCom.SetSkipNext(true);
					}
					break;
				case Resource.Id.buttonFS:
					{
						Enable_full_screen(!full_screen);
					}
					break;
				case Resource.Id.buttonScale:
					{
						if (GlobalPreferences.no_scaling)
						{
							HidCom.SetyAutoScale(true);
							buttonScale.Alpha = 1f;
							GlobalPreferences.chart_scaling = true;
							GlobalPreferences.no_scaling = false;
							Toast.MakeText(this, GetString(Resource.String.enable_scaling_def), ToastLength.Short).Show();
						}
						else
						{
							HidCom.SetyAutoScale(false);
							buttonScale.Alpha = 0.3f;
							GlobalPreferences.chart_scaling = false;
							GlobalPreferences.no_scaling = true;
							Toast.MakeText(this, GetString(Resource.String.disable_scaling_def), ToastLength.Short).Show();
						}
					}
					break;
				case Resource.Id.fastForwardButton:
					{
						HidCom.SetStartTimeMillis();
						HidCom.SetCounterMillis(0);
						//Set the reading speed to 1,2,4,8,16;
						if (HidCom.GetSpeedMult() == 5)
						{
							HidCom.SetSpeedMult(1);
							if (toastff != null)
							{
								toastff.Cancel();
							}
							toastff = Toast.MakeText(this, ">> x1", ToastLength.Short);
							toastff.Show();
						}
						else
						{
							HidCom.SetSpeedMult(HidCom.GetSpeedMult() + 1);
							if (toastff != null)
							{
								toastff.Cancel();
							}
							toastff = Toast.MakeText(this, ">> x" + (int)System.Math.Pow(2.0, HidCom.GetSpeedMult() - 1), ToastLength.Short);
							toastff.Show();
						}
					}
					break;
			}
		}

		readonly int REQUEST_CODE_OPEN_DIRECTORY = 12;
		public void PerformFileSearch()
		{
			//Intent intent = new Intent(this, typeof(DirectorySelectionActivity));
			Intent intent = new Intent(Intent.ActionOpenDocumentTree);
			intent.PutExtra(Android.Provider.DocumentsContract.ExtraInitialUri, Environment.DirectoryDocuments);
			this.StartActivityForResult(intent, REQUEST_CODE_OPEN_DIRECTORY);
			//Intent intent = new Intent(Intent.ActionGetContent);
			//intent.SetType("/**");//set type , here is setted to every type.
			//intent.AddCategory(Intent.CategoryOpenable);
			//StartActivityForResult(intent, REQUEST_CODE_OPEN_DIRECTORY); 
		}

		protected override void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent data)
		{
			base.OnActivityResult(requestCode, resultCode, data);
			if (resultCode == Result.Ok && requestCode == REQUEST_CODE_OPEN_DIRECTORY)
			{
				if (data == null) return;
				Uri uri = data.Data;
				DocumentFile param_file = null, log_file = null, binfile = null;
				DocumentFile documentFile = DocumentFile.FromTreeUri(this, uri);
				int fileCount = 0;
				if (documentFile != null && documentFile.IsDirectory)
				{
					foreach (DocumentFile file in documentFile.ListFiles())
					{
						fileCount++;
						if (file.Name.Equals("parameters.txt"))
						{
							param_file = DocumentFile.FromTreeUri(this, file.Uri);
						}
						if (file.Name.Equals("times_log.txt"))
						{
							log_file = DocumentFile.FromTreeUri(this, file.Uri);
						}
						if (file.Name.Equals("DATA.BIN"))
						{
							binfile = DocumentFile.FromTreeUri(this, file.Uri);
						}
					}
					if (replayFlag)
					{
						replayFlag = false;
						HidCom.Set_stop_Signal(true);
					}
					if (!binfile.Exists() || !param_file.Exists() || !log_file.Exists())
					{
						Toast.MakeText(this, GetString(Resource.String.select_valid_project), ToastLength.Short).Show();
						PerformFileSearch();
					}
					else
					{
						try
						{
							string path = CopyMeasureBinFile(log_file.Uri, "times_log.txt");
							FileInputStream inputStream = new FileInputStream(path);
							int i;
							char c;
							string log_str = "";
							while ((i = inputStream.Read()) != -1)
							{
								c = (char)i;
								log_str = log_str + c;
							}
							string sn_str = log_str.Substring(7, 8);
							//Log.i(TAG,"sn_str : " + sn_str);
							string mode_str = log_str.Substring(91, 4);
							//Log.i(TAG,"mode_str : " + mode_str);
							HidCom.Set_sn(sn_str);
							if (mode_str.Equals("2g") || mode_str.Equals("15g") || mode_str.Equals("200g"))
							{
								HidCom.Set_2g_sensitivity(true);
							}
							else
							{
								HidCom.Set_2g_sensitivity(false);
							}
							inputStream.Close();
							System.IO.File.Delete(path);
						}
						catch (FileNotFoundException e)
						{
							e.PrintStackTrace();
						}
						catch (IOException e)
						{
							e.PrintStackTrace();
						}
						try
						{
							string path = CopyMeasureBinFile(param_file.Uri, "parameters.txt");
							FileInputStream fileInputStream = new FileInputStream(new File(path));
							int i;
							char c;
							string param_str = "";
							//Skip first line
							while ((c = (char)fileInputStream.Read()) != '\n') ;
							//Read till end of file detected
							while ((i = fileInputStream.Read()) != -1)
							{
								c = (char)i;
								param_str = param_str + c;
							}
							//Log.i(TAG,"param : " + param_str);
							string[] paramsStr = param_str.Split("#");
							HidCom.Set_ax_gain(float.Parse(paramsStr[1]));
							HidCom.Set_ay_gain(float.Parse(paramsStr[2]));
							HidCom.Set_az_gain(float.Parse(paramsStr[3]));
							HidCom.Set_ax_offset(float.Parse(paramsStr[4]));
							HidCom.Set_ay_offset(float.Parse(paramsStr[5]));
							HidCom.Set_az_offset(float.Parse(paramsStr[6]));
							fileInputStream.Close();
							System.IO.File.Delete(path);
						}
						catch (FileNotFoundException e)
						{
							e.PrintStackTrace();
						}
						catch (IOException e)
						{
							e.PrintStackTrace();
						}

						try
						{
							string tempPath = CopyMeasureBinFile(binfile.Uri, "DATA.BIN");
							istToRead = new FileInputStream(tempPath);
							bin_file = new File(tempPath); // set binfile path
						}
						catch (Exception e)
						{
							e.PrintStackTrace();
						}
						startButton.SetImageDrawable(pauseDrawable);
						stopButton.SetImageDrawable(stopDrawable);
						EnableImButton(skipNextButton);
						EnableImButton(stopButton);
						EnableImButton(ffButton);
						//Once the file retrieved and the InputStream opened, start the reading with HidCom.ext_Event_Start().
						flagStartActivate = true;
						HidCom.Set_stop_Signal(false);
						HidCom.Ext_Event_Start();
					}
				}

			}
		}
		public string CopyMeasureBinFile(Uri uri, string fileName)
		{
			string dest = "";
			try
			{
				string parentFolder = System.IO.Path.Combine(Environment.GetExternalStoragePublicDirectory(Android.OS.Environment.DirectoryDocuments).AbsolutePath, "Temp");
				if (!System.IO.File.Exists(parentFolder))
				{
					System.IO.Directory.CreateDirectory(parentFolder);
				}
				dest = System.IO.Path.Combine(parentFolder, fileName);
				if (System.IO.File.Exists(dest))
				{
					System.IO.File.Delete(dest);
				}
				const int BufferSize = 1024;
				using (var inputStream = ContentResolver.OpenInputStream(uri))
				{
					using (var outputStream = System.IO.File.Create(dest))
					{
						var buffer = new byte[1024];
						while (true)
						{
							var count = inputStream.Read(buffer, 0, BufferSize);
							if (count > 0)
							{
								outputStream.Write(buffer, 0, count);
							}

							if (count < BufferSize) break;
						}
					}
				}
			}
			catch (Exception ex)
			{
				string error = ex.Message;
			}
			return dest;
		}

		//Create the set of frequencies based on the FFT parameters chosen
		public static void SetFftFreq(int fft_Window_Size, int Sampling_Freq, FloatValues fftFreq)
		{
			//fftBuffer differently created whether fft_Window_Size is odd or even
			//See JTransform API
			if ((fft_Window_Size & 1) == 0)
			{
				for (int i = 0; i <= (fft_Window_Size / 2); i++)
				{
					fftFreq.Add((float)i * Sampling_Freq / fft_Window_Size);
				}
			}
			else
			{
				for (int i = 0; i <= ((fft_Window_Size - 1) / 2); i++)
				{
					fftFreq.Add((float)i * Sampling_Freq / fft_Window_Size);
				}
			}
		}
		public static void SetFftVal(int fft_Window_Size, float noise_power_bw, LargeArray fftBuffer, FloatValues fftVal, FREQUENTIAL_CHOICE freqChoice)
		{
			float magnitude;
			float re, im;
			float val0, valn, vali;
			float delta_f = ((float)HidCom.GetSampling_Freq()) / fft_Window_Size;
			float sq = (float)Math.Sqrt(delta_f * noise_power_bw);
			//fftBuffer differently created whether fft_Window_Size is odd or even
			// /!\ two-sided form --> convert to single sided form by multiplying all values by 2 except DC
			//See JTransform API
			if ((fft_Window_Size & 1) == 0)
			{
				val0 = fftBuffer.getFloat(0) / fft_Window_Size;
				switch (freqChoice)
				{
					case FREQUENTIAL_CHOICE.AMPLITUDE_SPECTRUM_PEAK:
						fftVal.Add(val0);
						break;
					case FREQUENTIAL_CHOICE.AMPLITUDE_SPECTRUM_RMS:
						fftVal.Add(val0);
						break;
					case FREQUENTIAL_CHOICE.POWER_SPECTRUM:
						val0 *= val0;
						fftVal.Add(val0);
						break;
					case FREQUENTIAL_CHOICE.POWER_SPECTRAL_DENSITY:
						val0 *= val0;
						val0 /= delta_f;
						val0 /= noise_power_bw;
						fftVal.Add(val0);
						break;
					case FREQUENTIAL_CHOICE.AMPLITUDE_SPECTRAL_DENSITY:
						val0 /= sq;
						fftVal.Add(val0);
						break;
					default:
						break;
				}
				for (int i = 1; i < (fft_Window_Size / 2); i++)
				{
					re = fftBuffer.getFloat(2 * i);
					im = fftBuffer.getFloat(1 + 2 * i);
					magnitude = (float)Math.Sqrt(re * re + im * im);//Sqrt(re*re + im*im)
					vali = 2 * magnitude / fft_Window_Size;
					switch (freqChoice)
					{
						case FREQUENTIAL_CHOICE.AMPLITUDE_SPECTRUM_PEAK:
							fftVal.Add(vali);
							break;
						case FREQUENTIAL_CHOICE.AMPLITUDE_SPECTRUM_RMS:
							fftVal.Add(vali / (float)SQRT_2);
							break;
						case FREQUENTIAL_CHOICE.POWER_SPECTRUM:
							vali /= (float)SQRT_2;
							vali *= vali;
							fftVal.Add(vali);
							break;
						case FREQUENTIAL_CHOICE.POWER_SPECTRAL_DENSITY:
							vali /= (float)SQRT_2;
							vali *= vali;
							vali /= delta_f;
							vali /= noise_power_bw;
							fftVal.Add(vali);
							break;
						case FREQUENTIAL_CHOICE.AMPLITUDE_SPECTRAL_DENSITY:
							vali /= (float)SQRT_2;
							vali /= sq;
							fftVal.Add(vali);
							break;
						default:
							break;
					}

				}
				valn = (float)fftBuffer.getFloat(1) / fft_Window_Size;
				valn *= 2;
				switch (freqChoice)
				{
					case FREQUENTIAL_CHOICE.AMPLITUDE_SPECTRUM_PEAK:
						fftVal.Add(valn);
						break;
					case FREQUENTIAL_CHOICE.AMPLITUDE_SPECTRUM_RMS:
						fftVal.Add(valn / (float)SQRT_2);
						break;
					case FREQUENTIAL_CHOICE.POWER_SPECTRUM:
						valn /= (float)SQRT_2;
						valn *= valn;
						fftVal.Add(valn);
						break;
					case FREQUENTIAL_CHOICE.POWER_SPECTRAL_DENSITY:
						valn /= (float)SQRT_2;
						valn *= valn;
						valn /= delta_f;
						valn /= noise_power_bw;
						fftVal.Add(valn);
						break;
					case FREQUENTIAL_CHOICE.AMPLITUDE_SPECTRAL_DENSITY:
						valn /= (float)SQRT_2;
						valn /= sq;
						fftVal.Add(valn);
						break;
					default:
						break;
				}
			}
			else
			{
				val0 = fftBuffer.getFloat(0) / fft_Window_Size;
				switch (freqChoice)
				{
					case FREQUENTIAL_CHOICE.AMPLITUDE_SPECTRUM_PEAK:
						fftVal.Add(val0);
						break;
					case FREQUENTIAL_CHOICE.AMPLITUDE_SPECTRUM_RMS:
						fftVal.Add(val0);
						break;
					case FREQUENTIAL_CHOICE.POWER_SPECTRUM:
						val0 *= val0;
						fftVal.Add(val0);
						break;
					case FREQUENTIAL_CHOICE.POWER_SPECTRAL_DENSITY:
						val0 *= val0;
						val0 /= delta_f;
						val0 /= noise_power_bw;
						fftVal.Add(val0);
						break;
					case FREQUENTIAL_CHOICE.AMPLITUDE_SPECTRAL_DENSITY:
						val0 /= sq;
						fftVal.Add(val0);
						break;
					default:
						break;
				}
				for (int i = 1; i < ((fft_Window_Size - 1) / 2); i++)
				{
					re = fftBuffer.getFloat(2 * i);
					im = fftBuffer.getFloat(2 * i + 1);
					magnitude = (float)Math.Sqrt(re * re + im * im);//Sqrt(re*re + im*im)
					vali = 2 * magnitude / fft_Window_Size;
					switch (freqChoice)
					{
						case FREQUENTIAL_CHOICE.AMPLITUDE_SPECTRUM_PEAK:
							fftVal.Add(vali);
							break;
						case FREQUENTIAL_CHOICE.AMPLITUDE_SPECTRUM_RMS:
							fftVal.Add(vali / (float)SQRT_2);
							break;
						case FREQUENTIAL_CHOICE.POWER_SPECTRUM:
							vali /= (float)SQRT_2;
							vali *= vali;
							fftVal.Add(vali);
							break;
						case FREQUENTIAL_CHOICE.POWER_SPECTRAL_DENSITY:
							vali /= (float)SQRT_2;
							vali *= vali;
							vali /= delta_f;
							vali /= noise_power_bw;
							fftVal.Add(vali);
							break;
						case FREQUENTIAL_CHOICE.AMPLITUDE_SPECTRAL_DENSITY:
							vali /= (float)SQRT_2;
							vali /= sq;
							fftVal.Add(vali);
							break;
						default:
							break;
					}
				}
				re = fftBuffer.getFloat(fft_Window_Size - 1);
				im = fftBuffer.getFloat(1);
				magnitude = (float)Math.Sqrt(re * re + im * im);//Sqrt(re*re + im*im)
				valn = 2 * magnitude / fft_Window_Size;
				switch (freqChoice)
				{
					case FREQUENTIAL_CHOICE.AMPLITUDE_SPECTRUM_PEAK:
						fftVal.Add(valn);
						break;
					case FREQUENTIAL_CHOICE.AMPLITUDE_SPECTRUM_RMS:
						fftVal.Add(valn / (float)SQRT_2);
						break;
					case FREQUENTIAL_CHOICE.POWER_SPECTRUM:
						valn /= (float)SQRT_2;
						valn *= valn;
						fftVal.Add(valn);
						break;
					case FREQUENTIAL_CHOICE.POWER_SPECTRAL_DENSITY:
						valn /= (float)SQRT_2;
						valn *= valn;
						valn /= delta_f;
						valn /= noise_power_bw;
						fftVal.Add(valn);
						break;
					case FREQUENTIAL_CHOICE.AMPLITUDE_SPECTRAL_DENSITY:
						valn /= (float)SQRT_2;
						valn /= sq;
						fftVal.Add(valn);
						break;
					default:
						break;
				}
			}
		}

		//Perform a moving averaging on the fft values
		public static void AverageFftValues(FloatValues fftVal, FloatValues fftValCopy, int fft_Window_Size, int averageDepth)
		{

			int length = (fftVal.Size()) / (fft_Window_Size / 2 + 1);
			float[] init_values = new float[(fft_Window_Size / 2) + 1];
			FloatValues avFftValues = new FloatValues();
			avFftValues.Add(init_values);

			if (length < averageDepth)
			{
				for (int i = 0; i < (fft_Window_Size / 2) + 1; i++)
				{
					for (int j = 0; j < length; j++)
					{
						avFftValues.Set(i, avFftValues.Get(i) + fftVal.Get(i + j * ((fft_Window_Size / 2) + 1)));
					}
					fftValCopy.Add(avFftValues.Get(i) / length);
				}
			}
			else
			{
				for (int i = 0; i < (fft_Window_Size / 2) + 1; i++)
				{
					for (int j = 0; j < averageDepth; j++)
					{
						avFftValues.Set(i, avFftValues.Get(i) + fftVal.Get(j * ((fft_Window_Size / 2) + 1)));
					}
					fftValCopy.Add(avFftValues.Get(i) / averageDepth);
					fftVal.Remove(0);
				}
			}
		}

		//Function called to create an instance of the class java
		public static void CreateAnalysisClass(UsbEndpoint endPointWrite, UsbEndpoint endPointRead, UsbDevice device, UsbManager mUsbManager, UsbDeviceConnection connection, int packetSize)
		{
			HidCom = new AnalysisClass(endPointWrite, endPointRead, device, mUsbManager, connection, packetSize);
		}
		#endregion Methods
	}
}