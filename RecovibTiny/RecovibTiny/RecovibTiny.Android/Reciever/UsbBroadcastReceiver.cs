﻿using Android.App;
using Android.Content;
using Android.Hardware.Usb;
using Java.Nio;
using RecovibTiny.Droid.Helper;
using RecovibTiny.Droid.Reciever;
using RecovibTiny.Interface;
using System;
using System.Linq;
using System.Text;

[assembly: Xamarin.Forms.Dependency(typeof(UsbBroadcastReceiver))]
namespace RecovibTiny.Droid.Reciever
{
	[BroadcastReceiver]
	[IntentFilter(new[] { UsbManager.ActionUsbAccessoryAttached, UsbManager.ActionUsbAccessoryDetached })]
	public class UsbBroadcastReceiver : BroadcastReceiver, IUsbCommunication
	{
		//USB
		private static String ACTION_USB_PERMISSION = "com.micromega_dynamics.recovibtinyapp.USB_PERMISSION";
		public UsbDevice device;
		private UsbManager mUsbManager;
		private PendingIntent mPermissionIntent;
		private Context mContext;
		public bool IsDeviceConnected()
		{
			return GetDevice();
		}

		private bool GetDevice()
		{
			try
			{
				mContext = Android.App.Application.Context;
				Intent intent = new Intent();
				device = (UsbDevice)intent.GetParcelableExtra(UsbManager.ExtraDevice);
				mUsbManager = (UsbManager)mContext.GetSystemService(Context.UsbService);


				mPermissionIntent = PendingIntent.GetBroadcast(mContext, 0, new Intent(ACTION_USB_PERMISSION), 0);
				/*  IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
				  filter.AddAction(UsbManager.ActionUsbDeviceAttached);
				  filter.AddAction(UsbManager.ActionUsbDeviceDetached);*/
				//  mContext.RegisterReceiver(new UsbBroadcast(), filter);
			}
			catch (Exception ex)
			{
				Console.WriteLine("GetDevice exception : " + ex.Message);
			}
			return true;
		}
		public override void OnReceive(Context context, Intent intent)
		{
			String action = intent.Action;

			if (ACTION_USB_PERMISSION.Equals(action))
			{
				if (device == null)
				{
					device = (UsbDevice)intent.GetParcelableExtra(UsbManager.ExtraDevice);
				}
				if (device.VendorId == 8263 && device.ProductId == 2379)
				{
					Console.WriteLine(" device connected.");
					// setDevice(intent, true);
				}
				else if (device.VendorId == 8263 && device.ProductId == 2378)
				{
					Console.WriteLine(" device connected.");
					// setDevice(intent, true);
				}

			}

			//device attached
			if (UsbManager.ActionUsbDeviceAttached.Equals(action))
			{
				Console.WriteLine(" device attached.");
				/*synchronized(this) {
                    setDevice(intent, false);
                    Device_detected();
                }*/
				Device_detected();
				Xamarin.Forms.MessagingCenter.Send<object, String>(this, "Connect", "true");
			}

			//device detached
			if (UsbManager.ActionUsbDeviceDetached.Equals(action))
			{
				Console.WriteLine(" device detached.");
				Xamarin.Forms.MessagingCenter.Send<object, String>(this, "Connect", "false");
			}
		}

		private void Device_detected()
		{
			mUsbManager = (UsbManager)Application.Context.GetSystemService(Context.UsbService);
			// List<CharSequence> list = new LinkedList<>();
			foreach (UsbDevice usbDevice in mUsbManager.DeviceList.Values)
			{  //list VID/PID of all devices
				int vendorID = 8263;//0x2047
				int productIDTiny = 2379;//0x094B
				int productIDFeel = 2378;//0x094B
				if ((usbDevice.VendorId == vendorID) && (usbDevice.ProductId == productIDTiny))
				{
					// list.add("devID:" + usbDevice.DeviceId + " VID:" + int.Parse(usbDevice.VendorId) + " PID:" + Integer.toHexString(usbDevice.ProductId) + " " + usbDevice.DeviceName);
					device = usbDevice;
				}
				else if ((usbDevice.VendorId == vendorID) && (usbDevice.ProductId == productIDFeel))
				{
					// list.add("devID:" + usbDevice.DeviceId + " VID:" + int.Parse(usbDevice.VendorId) + " PID:" + Integer.toHexString(usbDevice.ProductId) + " " + usbDevice.DeviceName);
					device = usbDevice;
				}
			}
			// CharSequence[] devicesName = new CharSequence[mUsbManager.DeviceList.Count];
			// list.toArray(devicesName); 

			if (device.VendorId == 8263 && device.ProductId == 2379)
			{
				if (mUsbManager.HasPermission(device))
				{
					connection = mUsbManager.OpenDevice(device);
					usbInterface = device.GetInterface(1);
					device = (UsbDevice)mUsbManager.DeviceList.Values.ToArray()[0];  //set the selected device

					if (null == connection)
					{
						//Log.d(TAG, "unable to connect");
					}
					else
					{
						connection.ClaimInterface(usbInterface, true);            //Device connected - claim ownership over the interface
					}

					//	accessPermissionAndroid11();
					Console.WriteLine(" device connected.");
					try
					{
						//Direction of end point 1 - OUT - from host to device
						if (0 == usbInterface.GetEndpoint(1).Direction)
						{
							endPointWrite = usbInterface.GetEndpoint(1);
						}
					}
					catch (Exception e)
					{
						//Log.e(TAG, "Device have no endPointWrite", e);
					}
					try
					{
						//Direction of end point 0 - IN - from device to host
						if (0 != usbInterface.GetEndpoint(0).Direction)
						{
							endPointRead = usbInterface.GetEndpoint(0);
							packetSize = endPointRead.MaxPacketSize;
						}
					}
					catch (Exception e)
					{
						//Log.e(TAG, "Device have no endPointRead", e);
					}
					
					getState();
				}
				else if (device.VendorId == 8263 && device.ProductId == 2378)
				{
					if (mUsbManager.HasPermission(device))
					{
						connection = mUsbManager.OpenDevice(device);
						usbInterface = device.GetInterface(1);
						device = (UsbDevice)mUsbManager.DeviceList.Values.ToArray()[0];  //set the selected device

						if (null == connection)
						{
							//Log.d(TAG, "unable to connect");
						}
						else
						{
							connection.ClaimInterface(usbInterface, true);            //Device connected - claim ownership over the interface
						}

						//	accessPermissionAndroid11();
						Console.WriteLine(" device connected.");
						try
						{
							//Direction of end point 1 - OUT - from host to device
							if (0 == usbInterface.GetEndpoint(1).Direction)
							{
								endPointWrite = usbInterface.GetEndpoint(1);
							}
						}
						catch (Exception e)
						{
							//Log.e(TAG, "Device have no endPointWrite", e);
						}
						try
						{
							//Direction of end point 0 - IN - from device to host
							if (0 != usbInterface.GetEndpoint(0).Direction)
							{
								endPointRead = usbInterface.GetEndpoint(0);
								packetSize = endPointRead.MaxPacketSize;
							}
						}
						catch (Exception e)
						{
							//Log.e(TAG, "Device have no endPointRead", e);
						}

						getState();
					}
					else
					{
						//	handler_getPath.postDelayed(runnable_getPath, 1000);
						mUsbManager.RequestPermission(device, mPermissionIntent);
					}
				}
			}
		}


		private UsbDeviceConnection connection;
		private UsbInterface usbInterface;
		private UsbEndpoint endPointWrite;
		private UsbEndpoint endPointRead;
		private int packetSize;
		public byte[] receiveData()
		{
			// STATE_RECEIVE = 1;
			int size = 0;
			StringBuilder stringBuilder = new StringBuilder();        //Create new instances of the StringBuilder every time this method is called

			byte[] buffer = new byte[packetSize];

			if (connection != null && endPointRead != null)
			{    //Verify USB connection and data at end point
				int status = connection.BulkTransfer(endPointRead, buffer, packetSize, 1024);    //Read 64 bytes of data from end point 0 and store
																								 // Log.d("MessageProcesss", "Status rcvd : " + status);

			}

			byte[] frameCut = new byte[buffer.Length - 2];
			Array.Copy(buffer, 2, frameCut, 0, buffer.Length - 2);


			// STATE_RECEIVE = 0;
			return frameCut;
		}

		public void getState()
		{

			byte[] rcvd;
			// Get_Event_Method((byte)0x01);
			rcvd = receiveData();

			byte[] chargeState = new byte[16];
			byte[] checksum = new byte[16];


			if (rcvd[3] == 1) _charge_state = false;
			else _charge_state = true;

			Array.Copy(rcvd, 4, chargeState, 0, 2); // copy 16 bytes into chargeState from rcvd pos 4
			_battery_level = ByteBuffer.Wrap(chargeState).Order(ByteOrder.LittleEndian).Short;// get short from checksum

			Array.Copy(rcvd, 6, checksum, 0, 2); // copy 16 bytes into checksum from rcvd pos 20
			_checksum = ByteBuffer.Wrap(checksum).Order(ByteOrder.LittleEndian).Short;// get short from checksum

		}

		bool _charge_state;

		public bool charge_state()
		{

			return _charge_state;

		}

		private float _battery_level;

		public float battery_level()
		{
			return _battery_level;
		}
		private string _sn;
		private short _checksum;


		public string sn()
		{
			return _sn;
		}

		public short checksum()
		{
			return _checksum;
		}
	}
}