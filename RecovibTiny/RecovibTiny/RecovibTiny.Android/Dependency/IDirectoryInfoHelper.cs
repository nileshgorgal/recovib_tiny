﻿using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Support.V4.Provider;
using RecovibTiny.Droid.Dependency;
using RecovibTiny.Helper;
using RecovibTiny.Interface;
using System;
using Xamarin.Forms;

[assembly: Xamarin.Forms.Dependency(typeof(IDirectoryInfoHelper))]
namespace RecovibTiny.Droid.Dependency
{
	public class IDirectoryInfoHelper : Activity, IDirectoryInfo
	{
		public string GetExternalStoragePath()
		{
			return Android.OS.Environment.GetExternalStoragePublicDirectory(Android.OS.Environment.DirectoryDocuments).AbsolutePath;
		}
		public bool ComapareVersion()
		{
			if (Android.OS.Build.VERSION.SdkInt >= Android.OS.BuildVersionCodes.R)
			{
				return true;
			}
			return false;
		}

		public void GetAndroid11Permission()
		{
			try
			{
				Intent intent = new Intent(Intent.ActionOpenDocumentTree);
				//StartActivityForResult(intent, 9);
				((Activity)Forms.Context).StartActivityForResult(intent, 9);
				//((Activity)Android.App.Application.Context).StartActivityForResult(intent, 9);
			}
			catch (Exception ex)
			{
				string error = ex.Message;
			}
		}

		public void CopyMeasureBinFile()
		{
			try
			{
				string dest = System.IO.Path.Combine(GetExternalStoragePath(), "MEASURE.BIN");
				if (System.IO.File.Exists(dest))
				{
					System.IO.File.Delete(dest);
				}
				const int BufferSize = 1024;
				using (var inputStream = ((Activity)Forms.Context).ContentResolver.OpenInputStream(Android.Net.Uri.Parse(GlobalPreferences.Android11_Path)))
				{
					using (var outputStream = System.IO.File.Create(dest))
					{
						var buffer = new byte[1024];
						while (true)
						{
							var count = inputStream.Read(buffer, 0, BufferSize);
							if (count > 0)
							{
								outputStream.Write(buffer, 0, count);
							}

							if (count < BufferSize) break;
						}
					}
				}
				DependencyService.Get<IReadUsbDevice>(DependencyFetchTarget.GlobalInstance).GetConvParamFromHIDCOM(dest);
			}
			catch (Exception ex)
			{
				var Error = ex.Message;
			}
		}

		protected override void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent data)
		{
			base.OnActivityResult(requestCode, resultCode, data);
			if (resultCode == Result.Ok)
			{
				if (data != null)
				{
					if (requestCode == 9)
					{
						Android.Net.Uri uriTree = data.Data;
						DocumentFile documentFile = DocumentFile.FromTreeUri(this, uriTree);
						int fileCount = 0;
						if (documentFile != null && documentFile.IsDirectory)
						{
							foreach (DocumentFile file in documentFile.ListFiles())
							{
								fileCount++;
								if (file.Name.Equals("MEASURE.BIN"))
								{
									GlobalPreferences.Android11_Path = file.Uri.ToString();
									CopyMeasureBinFile();
									break;
								}
							}
						}
					}
				}
			}
		}
	}
}