﻿using Android.App;
using Android.Content;
using Android.Hardware.Usb;
using RecovibTiny.Droid.Dependency;
using RecovibTiny.Interface;
using System;
[assembly: Xamarin.Forms.Dependency(typeof(IGetPermissionHelper))]
namespace RecovibTiny.Droid.Dependency
{
	class IGetPermissionHelper : IGetPermission
	{
		private static readonly String ACTION_USB_PERMISSION = "com.micromega_dynamics.recovibtinyapp.USB_PERMISSION";
		public bool GetDeviceConnected()
		{
			try
			{
				UsbManager mUsbManager = (UsbManager)Application.Context.GetSystemService(Context.UsbService);
				foreach (UsbDevice usbDevice in mUsbManager.DeviceList.Values)
				{  //list VID/PID of all devices
					int vendorID = 8263;//0x2047
					int productIDTiny = 2379;//0x094B
					int productIDFeel = 2378;//0x094B
					if ((usbDevice.VendorId == vendorID) && (usbDevice.ProductId == productIDTiny))
					{
						if (mUsbManager.HasPermission(usbDevice))
						{
							return true;
						}
						else
						{
							mUsbManager.RequestPermission(usbDevice, PendingIntent.GetBroadcast(Application.Context, 0, new Intent(ACTION_USB_PERMISSION), 0));
						}
						break;
					}
					else if (usbDevice.VendorId == vendorID && usbDevice.ProductId == productIDFeel)
					{
						if (mUsbManager.HasPermission(usbDevice))
						{
							return true;
						}
						else
						{
							mUsbManager.RequestPermission(usbDevice, PendingIntent.GetBroadcast(Application.Context, 0, new Intent(ACTION_USB_PERMISSION), 0));
							return false;
						}
					}
				}
			}
			catch (Exception ex)
			{
				string message = ex.Message;
			}
			return false;
		}
	}
}