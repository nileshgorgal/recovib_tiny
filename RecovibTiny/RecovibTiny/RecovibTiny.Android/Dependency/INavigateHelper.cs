﻿using Android.App;
using Android.Content;
using RecovibTiny.Droid.Dependency;
using RecovibTiny.Droid.ScichartActivities;
using RecovibTiny.Interface;

[assembly: Xamarin.Forms.Dependency(typeof(INavigateHelper))]
namespace RecovibTiny.Droid.Dependency
{
	public class INavigateHelper : Activity, INavigate
	{
		public void StartNativeIntentOrActivity()
		{
			DisplayActivity.CreateAnalysisClass(null, null, null, null, null, 0);
			DisplayActivity.manuallyOpened = true;
			DisplayActivity.ViewerMode(true);
			Context mContext = Android.App.Application.Context;
			var intent = new Intent(mContext, typeof(DisplayActivity));
			intent.SetFlags(ActivityFlags.NewTask);
			mContext.StartActivity(intent);
		}
	}
}