﻿using Android.App;
using Android.Content;
using RecovibTiny.Droid.Dependency;
using RecovibTiny.Interface;
using System;
using Xamarin.Forms;

[assembly: Dependency(typeof(IRestartAppHelper))]
namespace RecovibTiny.Droid.Dependency
{
	public class IRestartAppHelper : Activity, IRestartApp
	{
		public void RestartApp()
		{
			Intent mStartActivity = new Intent(Android.App.Application.Context, typeof(MainActivity));
			int mPendingIntentId = 123456;
			PendingIntent mPendingIntent = PendingIntent.GetActivity(Android.App.Application.Context, mPendingIntentId, mStartActivity, PendingIntentFlags.CancelCurrent);
			AlarmManager mgr = (AlarmManager)Android.App.Application.Context.GetSystemService(AlarmService);
			mgr.Set(AlarmType.Rtc, DateTimeOffset.Now.ToUnixTimeMilliseconds() + 100, mPendingIntent);
			var activity = (Activity)Forms.Context;
			activity.FinishAffinity();
		}

		public void CloseApp()
		{
			Intent launchIntent = new Intent(Android.App.Application.Context, typeof(MainActivity));
			PendingIntent pending = PendingIntent.GetActivity(Android.App.Application.Context, 0,
				  launchIntent, PendingIntentFlags.CancelCurrent);
			AlarmManager mgr = (AlarmManager)Android.App.Application.Context.GetSystemService(AlarmService);
			mgr.Set(AlarmType.Rtc, DateTimeOffset.Now.ToUnixTimeMilliseconds() + 100, pending);
			Finish();
		}
	}
}