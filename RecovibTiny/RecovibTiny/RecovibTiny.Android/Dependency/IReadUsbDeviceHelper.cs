﻿using Android;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Hardware.Usb;
using Android.Util;
using Android.Widget;
using AndroidX.Core.Content;
using Java.IO;
using Java.Nio;
using Java.Text;
using RecovibTiny.Droid.Dependency;
using RecovibTiny.Droid.Helper;
using RecovibTiny.Helper;
using RecovibTiny.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using DeviceInfo = RecovibTiny.Model.DeviceInfo;

[assembly: Xamarin.Forms.Dependency(typeof(IReadUsbDeviceHelper))]
namespace RecovibTiny.Droid.Dependency
{
	[IntentFilter(new[] { "android.settings.MANAGE_ALL_FILES_ACCESS_PERMISSION" })]
	public class IReadUsbDeviceHelper : Activity, IReadUsbDevice
	{
		//USB
		public UsbDevice device;
		private UsbManager mUsbManager;
		private readonly PendingIntent mPermissionIntent;
		private Context mContext;
		private UsbDeviceConnection connection;
		private UsbInterface usbInterface;
		private UsbEndpoint endPointWrite;
		private UsbEndpoint endPointRead;
		private int packetSize;
		int vendorID = 8263;//0x2047
		int productIDTiny = 2379;//0x094B
		int productIDFeel = 2378;//0x094B
		public List<DeviceInfo> getBatteryLevel()
		{
			Device_detected();
			GetState();
			return GetDeviceList();
		}
		private void Device_detected()
		{
			mContext = Application.Context;
			mUsbManager = (UsbManager)Application.Context.GetSystemService(Context.UsbService);

			// List<CharSequence> list = new LinkedList<>();
			foreach (UsbDevice usbDevice in mUsbManager.DeviceList.Values)
			{
				if ((usbDevice.VendorId == vendorID) && (usbDevice.ProductId == productIDTiny))
				{
					device = usbDevice;
					usbInterface = device.GetInterface(1);
				}
				else if ((usbDevice.VendorId == vendorID) && (usbDevice.ProductId == productIDFeel))
				{
					device = usbDevice;
					usbInterface = device.GetInterface(0);
				}
			}
			// CharSequence[] devicesName = new CharSequence[mUsbManager.DeviceList.Count];
			// list.toArray(devicesName);
			//device = (UsbDevice)mUsbManager.DeviceList.Values.ToArray()[0];  //set the selected device			

			connection = mUsbManager.OpenDevice(device);

			if (null == connection)
			{
				//Log.d(TAG, "unable to connect");
			}
			else
			{
				connection.ClaimInterface(usbInterface, true);            //Device connected - claim ownership over the interface
			}
			if (device != null)
			{
				if (device.VendorId == vendorID && device.ProductId == productIDTiny)
				{
					GetPoint();
				}
				else if (device.VendorId == vendorID && device.ProductId == productIDFeel)
				{
					GetPoint();
				}
			}
			else { return; }
			//Log.e(TAG_11, "Android accessPermissionAndroid11 calling ");
			//Asking android 11 permissions

		}
		private void GetPoint()
		{
			if (mUsbManager.HasPermission(device))
			{
				System.Console.WriteLine(" device connected.");
				try
				{
					//Direction of end point 1 - OUT - from host to device
					if (0 == usbInterface.GetEndpoint(1).Direction)
					{
						endPointWrite = usbInterface.GetEndpoint(1);
					}
				}
				catch (Exception e)
				{
					Log.Error(TAG, "Device have no endPointWrite", e);
				}
				try
				{
					//Direction of end point 0 - IN - from device to host
					if (0 != usbInterface.GetEndpoint(0).Direction)
					{
						endPointRead = usbInterface.GetEndpoint(0);
						packetSize = endPointRead.MaxPacketSize;
					}
				}
				catch (Exception e)
				{
					Log.Error(TAG, "Device have no endPointRead", e);
				}
			}
			else
			{
				//	handler_getPath.postDelayed(runnable_getPath, 1000);
				mUsbManager.RequestPermission(device, mPermissionIntent);
			}
		}
		public byte[] ReceiveData()
		{
			// STATE_RECEIVE = 1;
			int size = 0;
			StringBuilder stringBuilder = new StringBuilder();        //Create new instances of the StringBuilder every time this method is called

			byte[] buffer = new byte[packetSize];

			if (connection != null && endPointRead != null)
			{    //Verify USB connection and data at end point
				int status = connection.BulkTransfer(endPointRead, buffer, packetSize, 1024);    //Read 64 bytes of data from end point 0 and store
																								 // Log.d("MessageProcesss", "Status rcvd : " + status);

			}

			byte[] frameCut = new byte[buffer.Length - 2];
			Array.Copy(buffer, 2, frameCut, 0, buffer.Length - 2);


			// STATE_RECEIVE = 0;
			return frameCut;
		}
		public void GetState()
		{

			byte[] rcvd;
			Get_Event_Method((byte)0x01);
			rcvd = ReceiveData();

			byte[] chargeState = new byte[16];
			byte[] checksum = new byte[16];


			if (rcvd[3] == 1) _charge_state = false;
			else _charge_state = true;

			Array.Copy(rcvd, 4, chargeState, 0, 2); // copy 16 bytes into chargeState from rcvd pos 4
			_battery_level = ByteBuffer.Wrap(chargeState).Order(ByteOrder.LittleEndian).Short;// get short from checksum

			Array.Copy(rcvd, 6, checksum, 0, 2); // copy 16 bytes into checksum from rcvd pos 20
			_checksum = ByteBuffer.Wrap(checksum).Order(ByteOrder.LittleEndian).Short;// get short from checksum

		}
		private List<DeviceInfo> GetDeviceList()
		{
			//Get the battery level
			battLevel = 1.0F; ;
			if (Charge_state())
			{
				battLevel = 0.000124528 * Battery_level() - 1.99245283;
				if (battLevel > 0.99) battLevel = 0.99;
				if (battLevel < 0.00) battLevel = 0.00;
			}
			battLevel = battLevel * 100;

			string hex = Java.Lang.Integer.ToHexString(Checksum() & 0xffff);

			return new List<DeviceInfo>() { new DeviceInfo { _batteryLevel = battLevel, _SerialNumber = GetSN(), _FirmwareVersion = hex, _CalibDate = GetCalibDate() } };
		}

		private double battLevel;
		bool _charge_state;
		public bool Charge_state()
		{

			return _charge_state;

		}

		private float _battery_level;
		public float Battery_level()
		{
			return _battery_level;
		}
		private string _sn;
		private short _checksum;
		public string Sn()
		{
			return _sn;
		}
		public short Checksum()
		{
			return _checksum;
		}
		private List<byte> outputbuff;
		public void Get_Event_Method(byte Event)
		{
			outputbuff = new List<byte>();
			outputbuff.Clear();
			outputbuff.Add(Event);
			SendData(outputbuff);
		}
		public int SendData(List<byte> buffIn)
		{
			int status = 0;
			if (device != null && endPointWrite != null && mUsbManager.HasPermission(device))
			{
				outputbuff = new List<byte>(buffIn);
				byte[] report = new byte[64];
				report[0] = 0x3f;
				report[1] = (byte)outputbuff.Count;
				for (int i = 0; i < outputbuff.Count; i++)
				{
					report[i + 2] = outputbuff[i];
				}

				status = connection.BulkTransfer(endPointWrite, report, report.Length, 1024);
				//Log.d("MessageProcesss", "Status send : " + status);

			}
			return status;
		}
		public string GetSN()
		{
			byte[] bufferRcvd;

			/* --GET THE SERIAL NUMBER --*/

			string SerialNumber;
			Get_Method((byte)Func.EVENT_EXT_GET_PARAM, ParamId.SN_ID, ParamType.SN8);
			bufferRcvd = ReceiveData();
			try
			{
				Log.Error("RCVD ", "hexadecimal" + GetHexString(bufferRcvd));
			}
			catch (Exception e)
			{
				e.StackTrace.ToString();
			}
			SerialNumber = (string)Compute_DataType(bufferRcvd);
			_sn = SerialNumber;


			return _sn;

		}
		public void Get_Method(byte EventType, byte idFunction, byte ParamType)
		{
			outputbuff.Clear();
			outputbuff.Add(EventType);
			outputbuff.Add(idFunction);
			outputbuff.Add(ParamType);
			SendData(outputbuff);
		}
		public static string GetHexString(byte[] b)
		{
			string result = "";
			for (int i = 0; i < b.Length; i++)
			{
				result += ((b[i] & 0xff) + 0x100, 16).ToString().Substring(1);
			}
			return result;
		}
		public object Compute_DataType(byte[] rcvd)
		{
			byte dataType;
			dataType = rcvd[2];
			byte[] frameCut = new byte[rcvd.Length - 3];
			Array.Copy(rcvd, 3, frameCut, 0, rcvd.Length - 3);
			if (dataType == ParamType.SN8)
			{
				byte[] dataRcvd = new byte[ParamType.SN8_SIZE];
				Array.Copy(frameCut, 0, dataRcvd, 0, ParamType.SN8_SIZE);

				Log.Error("MessageProcesss", "in the datatype");
				Java.Lang.String data;
				data = new Java.Lang.String(dataRcvd, "UTF-8");
				return (string)data;
			}

			if (dataType == ParamType.FLOAT32)
			{
				byte[] dataRcvd = new byte[ParamType.FLOAT32_SIZE];
				Array.Copy(frameCut, 0, dataRcvd, 0, ParamType.FLOAT32_SIZE);
				float data = ByteBuffer.Wrap(frameCut).Order(ByteOrder.LittleEndian).GetFloat(0);
				return data;
			}
			if (dataType == ParamType.DT)
			{
				byte[] byteDateBuffer = new byte[2];

				int year, month, day, hour, minute, second, millisecond;

				byteDateBuffer[0] = frameCut[0];
				byteDateBuffer[1] = frameCut[1];


				short year_s = ByteBuffer.Wrap(byteDateBuffer).Order(ByteOrder.LittleEndian).GetShort(0);
				year = year_s;


				byteDateBuffer[0] = frameCut[2];
				byteDateBuffer[1] = frameCut[3];

				short month_s = ByteBuffer.Wrap(byteDateBuffer).Order(ByteOrder.LittleEndian).GetShort(0);
				month = month_s;


				byteDateBuffer[0] = frameCut[4];
				byteDateBuffer[1] = frameCut[5];

				short day_s = ByteBuffer.Wrap(byteDateBuffer).Order(ByteOrder.LittleEndian).GetShort(0);
				day = day_s;


				byteDateBuffer[0] = frameCut[6];
				byteDateBuffer[1] = frameCut[7];

				short hour_s = ByteBuffer.Wrap(byteDateBuffer).Order(ByteOrder.LittleEndian).GetShort(0);

				hour = hour_s;


				byteDateBuffer[0] = frameCut[8];
				byteDateBuffer[1] = frameCut[9];

				short minute_s = ByteBuffer.Wrap(byteDateBuffer).Order(ByteOrder.LittleEndian).GetShort(0);
				minute = minute_s;


				byteDateBuffer[0] = frameCut[10];
				byteDateBuffer[1] = frameCut[11];

				short second_s = ByteBuffer.Wrap(byteDateBuffer).Order(ByteOrder.LittleEndian).GetShort(0);
				second = second_s;


				byteDateBuffer[0] = frameCut[12];
				byteDateBuffer[1] = frameCut[13];

				short millisecond_s = ByteBuffer.Wrap(byteDateBuffer).Order(ByteOrder.LittleEndian).GetShort(0);
				millisecond = millisecond_s;


				DateTime data = new DateTime(year, month, day, hour, minute, second, millisecond);

				return data;
			}

			if (dataType == ParamType.UINT16)
			{
				byte[] dataRcvd = new byte[ParamType.UINT16_SIZE];
				Array.Copy(frameCut, 0, dataRcvd, 0, ParamType.UINT16_SIZE);
				short data = ByteBuffer.Wrap(frameCut).Order(ByteOrder.LittleEndian).GetShort(0);
				return data;
			}

			if (dataType == ParamType.INT8)
			{
				return frameCut[0];
			}

			Log.Error("MessageProcesss", "null dataType");
			return null;
		}

		#region GetCalibDate

		private byte _calib_state;

		public byte Calib_state()
		{
			return _calib_state;
		}

		private DateTime _calib_date;

		public DateTime Calib_date()
		{
			return _calib_date;
		}

		private DateTime _use_date;

		public DateTime Use_date()
		{
			return _use_date;
		}

		private DateTime _reminder_date;

		public DateTime Reminder_date()
		{
			return _reminder_date;
		}

		public DateTime GetCalibDateId()
		{
			byte[] bufferRcvd;
			Get_Method((byte)Func.EVENT_EXT_GET_PARAM, (byte)ParamId.CALIB_DATE_ID, (byte)ParamType.DT);
			bufferRcvd = ReceiveData();

			try
			{
				_calib_date = (DateTime)Compute_DataType(bufferRcvd);
			}
			catch (UnsupportedEncodingException e)
			{
				e.PrintStackTrace();
			}

			return _calib_date;

		}

		public byte[] ComputeDateTime(DateTime toCompute)
		{
			DateTime dt_compute = toCompute;

			int year, month, day, hour, minute, second, millisecond;

			year = dt_compute.Year;
			month = dt_compute.Month;
			day = dt_compute.Day;
			hour = dt_compute.Hour;
			minute = dt_compute.Minute;
			second = dt_compute.Second;
			millisecond = dt_compute.Millisecond;

			//convert the date
			byte[] data = new byte[14];
			data[0] = (byte)(year & 0xFF);
			data[1] = (byte)((year >> 8) & 0xFF);

			data[2] = (byte)(month & 0xFF);
			data[3] = (byte)((month >> 8) & 0xFF);

			data[4] = (byte)(day & 0xFF);
			data[5] = (byte)((day >> 8) & 0xFF);

			data[6] = (byte)(hour & 0xFF);
			data[7] = (byte)((hour >> 8) & 0xFF);

			data[8] = (byte)(minute & 0xFF);
			data[9] = (byte)((minute >> 8) & 0xFF);

			data[10] = (byte)(second & 0xFF);
			data[11] = (byte)((second >> 8) & 0xFF);

			data[12] = (byte)(millisecond & 0xFF);
			data[13] = (byte)((millisecond >> 8) & 0xFF);
			return data;

		}

		public void Set_Method(byte Event, byte idFunction, byte typeFunction, byte[] bufferSet)
		{
			List<Byte> list = new List<byte>();
			outputbuff.Clear();
			outputbuff.Add(Event);
			outputbuff.Add(idFunction);
			outputbuff.Add(typeFunction);
			foreach (byte b in bufferSet)
			{
				list.Add(b);
			}

			foreach (Byte lst in list)
			{
				outputbuff.Add(lst);
			}

			SendData(outputbuff);
		}

		public void SetCalibrationStatus(byte statusCalib)
		{

			byte[] data = new byte[1];
			;
			data[0] = statusCalib;
			Set_Method((byte)Func.EVENT_EXT_SET_PARAM, (byte)ParamId.CALIB_STATE_ID, (byte)ParamType.INT8, data);

			byte[] bufferRcvd;
			bufferRcvd = ReceiveData();
			bufferRcvd = null;
		}
		private string GetCalibDate()
		{
			string calibDate = "";
			try
			{
				DateTime dtNow = DateTime.Now;

				//localHidCom.GetCalibStateId();
				GetCalibDateId();
				//localHidCom.GetUseDateId();
				//localHidCom.GetReminderDateId();

				calibDate = Calib_date().ToString("dd/MM/yyyy");
				//TODO : CALIBRATION CHECK
				if (false)
				{
					if (Calib_state() == 0) //check if a calibration is needed
					{
						byte[] bufferRcvd;
						byte[] dtNowData = new byte[14];

						dtNowData = ComputeDateTime(dtNow);
						Set_Method((byte)Func.EVENT_EXT_SET_PARAM, ParamId.USE_DATE_ID, ParamType.DT, dtNowData);

						bufferRcvd = ReceiveData();
						bufferRcvd = null;
						dtNow.AddYears(1);
						DateTime dtReminder = dtNow;
						byte[] dtReminderData = new byte[14];
						dtReminderData = ComputeDateTime(dtReminder);

						Set_Method((byte)Func.EVENT_EXT_SET_PARAM, ParamId.REMINDER_DATE_ID, ParamType.DT, dtReminderData);

						bufferRcvd = ReceiveData();
						bufferRcvd = null;

						SetCalibrationStatus((byte)1); // set the calibration status

					}
					else
					{
						if (dtNow.CompareTo(Reminder_date()) > 0)
						{
							// custom dialog
						}
					}
				}


			}
			catch (Exception ex)
			{
				return new DateTime().ToString("dd/MM/yyyy");
			}
			return calibDate;
		}
		#endregion GetCalibDate

		#region SetArm
		public void InitArmSetup(byte pSensitivity, DateTime startAt, DateTime stopAt)
		{
			try
			{
				lock (this)
				{
					byte[] dateBuffer = new byte[14];
					byte[] bufferRcvd;
					DateTime durationNow = DateTime.Now;

					SetSensitivity(pSensitivity);

					dateBuffer = ComputeDateTime(DateTime.Now);
					Set_Method(Func.EVENT_EXT_SET_PARAM, ParamId.DT_ARM_ID, ParamType.DT, dateBuffer);
					dateBuffer = null;
					bufferRcvd = ReceiveData();
					bufferRcvd = null;

					dateBuffer = ComputeDateTime(startAt);
					Set_Method(Func.EVENT_EXT_SET_PARAM, ParamId.DT_START_ID, ParamType.DT, dateBuffer);
					dateBuffer = null;
					bufferRcvd = ReceiveData();
					bufferRcvd = null;

					dateBuffer = ComputeDateTime(stopAt);
					Set_Method(Func.EVENT_EXT_SET_PARAM, ParamId.DT_STOP_ID, ParamType.DT, dateBuffer);
					dateBuffer = null;
					bufferRcvd = ReceiveData();
					bufferRcvd = null;

					long a = new DateTimeOffset(startAt.ToUniversalTime()).ToUnixTimeMilliseconds();
					long b = new DateTimeOffset(stopAt.ToUniversalTime()).ToUnixTimeMilliseconds();
					long c = new DateTimeOffset(durationNow.ToUniversalTime()).ToUnixTimeMilliseconds();

					long ms_to_start = new DateTimeOffset(startAt.ToUniversalTime()).ToUnixTimeMilliseconds() - new DateTimeOffset(durationNow.ToUniversalTime()).ToUnixTimeMilliseconds();
					long ms_to_stop = new DateTimeOffset(stopAt.ToUniversalTime()).ToUnixTimeMilliseconds() - new DateTimeOffset(durationNow.ToUniversalTime()).ToUnixTimeMilliseconds();
					if (!(ms_to_start >= ms_to_stop) && !(ms_to_start <= 0) && !(ms_to_stop <= 0))
					{
						int ticks_to_start = (int)ms_to_start / 1000;
						int ticks_to_stop = (int)ms_to_stop / 1000;

						ticks_to_start *= 1024;
						ticks_to_stop *= 1024;

						SetArm(ticks_to_start, ticks_to_stop - 1);
						bufferRcvd = ReceiveData();
						bufferRcvd = null;
					}
					else
					{
						Toast.MakeText(Application.Context, "Bad timing configuration..!", ToastLength.Long).Show();
					}
				}
			}
			catch (Exception ex)
			{
				Log.Error(TAG, ex.Message);
			}
		}
		private readonly string TAG = "ComClassError";
		public void SetSensitivity(byte sensitivity)
		{

			byte[] data = new byte[1];
			data[0] = sensitivity;
			Set_Method((byte)Func.EVENT_EXT_SET_PARAM, (byte)ParamId.SENSITIVITY_ID, (byte)ParamType.INT8, data);

			byte[] bufferRcvd;
			bufferRcvd = ReceiveData();
			bufferRcvd = null;
		}

		public void SetArm(int start_tick, int stop_tick)
		{

			byte[] arrARM = new byte[12];
			arrARM[0] = Func.EVENT_EXT_ARM;
			arrARM[1] = 0x00;
			arrARM[2] = 0x11; // dummy
			arrARM[3] = 0x12; // dummy

			//Log.d("Ticks_out", "startTicks" + start_tick);
			//Log.d("Ticks_out", "stopTicks" + stop_tick);

			byte[] startTicks = new byte[4];
			ByteBuffer startTicksBuffer = ByteBuffer.Allocate(4).Order(ByteOrder.LittleEndian);
			startTicksBuffer.PutInt(start_tick);
			if (startTicksBuffer.HasArray)
			{
				startTicks = Encoding.ASCII.GetBytes(startTicksBuffer.ToString());
			}
			//startTicks = new byte[startTicksBuffer.Remaining()];

			byte[] stopTicks = new byte[4];
			ByteBuffer stopTicksBuffer = ByteBuffer.Allocate(4).Order(ByteOrder.LittleEndian);
			stopTicksBuffer.PutInt(stop_tick);
			stopTicks = Encoding.ASCII.GetBytes(stopTicksBuffer.ToString()); ;


			Array.Copy(startTicks, 0, arrARM, 4, 4);
			Array.Copy(stopTicks, 0, arrARM, 8, 4);

			Set_Method_event(arrARM);


		}
		public void Set_Method_event(byte[] bufferSet)
		{
			List<byte> list = new List<byte>();
			outputbuff.Clear();
			foreach (byte b in bufferSet)
			{
				list.Add(b);
			}
			outputbuff = list;
			SendData(outputbuff);
		}
		#endregion SetArm

		#region Download		

		#region Variables
		private float _ax_gain_2g;
		private float _ay_gain_2g;
		private float _az_gain_2g;
		private float _ax_offset_2g;
		private float _ay_offset_2g;
		private float _az_offset_2g;
		private float _ax_gain_6g;
		private float _ay_gain_6g;
		private float _az_gain_6g;
		private float _ax_offset_6g;
		private float _ay_offset_6g;
		private float _az_offset_6g;
		private byte _sensitivity;
		DateTime start_DT;
		private string record_folder;
		File binfile;
		private string measure_bin_filePath;
		private long ticks_value;
		File folder;
		File snFolder;
		File dtFolder;
		File viewerFolder;
		private int dl_progress = 0;
		FileInputStream istToRead;
		string viewerFolderPath;
		#endregion Variables
		public void GetConvParamFromHIDCOM(string SD_id)
		{
			try
			{
				Device_detected();
				if (outputbuff == null)
				{
					outputbuff = new List<byte>();
				}
				_ax_gain_2g = Get_Ax_Gain_2G();
				_ay_gain_2g = Get_Ay_Gain_2G();
				_az_gain_2g = Get_Az_Gain_2G();
				_ax_offset_2g = Get_Ax_Offset_2G();
				_ay_offset_2g = Get_Ay_Offset_2G();
				_az_offset_2g = Get_Az_Offset_2G();
				_ax_gain_6g = Get_Ax_Gain_6G();
				_ay_gain_6g = Get_Ay_Gain_6G();
				_az_gain_6g = Get_Az_Gain_6G();
				_ax_offset_6g = Get_Ax_Offset_6G();
				_ay_offset_6g = Get_Ay_Offset_6G();
				_az_offset_6g = Get_Az_Offset_6G();

				_sensitivity = Get_Sensitivity();

				_sn = GetSN();

				start_DT = Get_Start_DT();

				CheckAppPermissionsAsync(SD_id);

			}
			catch (UnsupportedEncodingException e)
			{
				string message = e.Message;
			}
		}
		private void CreateViewerFile1(string SD_id)
		{
			//string baseDir = Android.OS.Environment.DirectoryDocuments;
			string baseDir = Android.OS.Environment.GetExternalStoragePublicDirectory(Android.OS.Environment.DirectoryDocuments).AbsolutePath;
			SimpleDateFormat sdf = new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss");

			string folderName = "RecoVIB_Tiny_App";
			string snFolderName = _sn;
			string dtFolderName = start_DT.ToString("dd_MM_yyyy_HH_mm_ss");
			string viewerFolderName = "basic";

			string folderPath = baseDir + File.Separator + folderName;
			string snFolderPath = folderPath + File.Separator + snFolderName;
			string dtFolderPath = snFolderPath + File.Separator + dtFolderName;
			viewerFolderPath = dtFolderPath + File.Separator + viewerFolderName;

			record_folder = "Documents/" + folderName + File.Separator + snFolderName + File.Separator + dtFolderName;

			string axg;
			string ayg;
			string azg;
			string axo;
			string ayo;
			string azo;
			if (_sensitivity == 0x02)
			{
				//2g 15g 200g
				axg = string.Format("en_US", "%.10f", _ax_gain_2g);
				ayg = string.Format("es-US", "%.10f", _ay_gain_2g);
				azg = string.Format("es-US", "%.10f", _az_gain_2g);
				axo = string.Format("es-US", "%.10f", _ax_offset_2g);
				ayo = string.Format("es-US", "%.10f", _ay_offset_2g);
				azo = string.Format("es-US", "%.10f", _az_offset_2g);
			}
			else
			{
				//6g
				axg = string.Format("es-US", "%.10f", _ax_gain_6g);
				ayg = string.Format("es-US", "%.10f", _ay_gain_6g);
				azg = string.Format("es-US", "%.10f", _az_gain_6g);
				axo = string.Format("es-US", "%.10f", _ax_offset_6g);
				ayo = string.Format("es-US", "%.10f", _ay_offset_6g);
				azo = string.Format("es-US", "%.10f", _az_offset_6g);
			}

			string paramfile = System.IO.Path.Combine(viewerFolderPath, "parameters.txt");

			//create param file
			try
			{
				if (!System.IO.File.Exists(folderPath))
				{
					//System.IO.File.Create(folderPath);
					System.IO.Directory.CreateDirectory(folderPath);
				}
				if (!System.IO.File.Exists(snFolderPath))
				{
					//System.IO.File.Create(snFolderPath);
					System.IO.Directory.CreateDirectory(snFolderPath);
				}
				if (!System.IO.File.Exists(dtFolderPath))
				{
					//System.IO.File.Create(dtFolderPath);
					System.IO.Directory.CreateDirectory(dtFolderPath);
				}
				if (!System.IO.File.Exists(viewerFolderPath))
				{
					//System.IO.File.Create(viewerFolderPath);
					System.IO.Directory.CreateDirectory(viewerFolderPath);
				}

				if (!System.IO.File.Exists(paramfile))
				{
					System.IO.File.Create(paramfile);
					//System.IO.File.WriteAllText(paramfile, txtFileText.Text.ToString());
				}

				FileOutputStream fp = new FileOutputStream(paramfile);
				PrintWriter pw = new PrintWriter(paramfile);
				pw.Println("Serial#AxGain#AyGain#AzGain#AxOffset#AyOffset#AzOffset");
				string p = _sn + "#" + axg + "#" + ayg + "#" + azg + "#" + axo + "#" + ayo + "#" + azo;
				pw.Println(p);
				pw.Flush();
				pw.Close();
				fp.Close();
			}
			catch (Exception e)
			{
				Log.Error("ERROR", "File not found EXCEPTION" + e);
			}
			Log.Error("ERROR", "BinaryDid Activity : createViewerFile measure_bin_filePath : " + measure_bin_filePath);

			binfile = new File(SD_id);

			Log.Error("TAG_11", "BinaryDid Activity : createViewerFile binfile : " + binfile);
			//File logfile = new File(viewerFolder, "times_log.txt");
			string logfile = System.IO.Path.Combine(viewerFolderPath, "times_log.txt");
			if (!System.IO.File.Exists(logfile))
			{
				System.IO.File.Create(logfile);
			}
			long bin_n_ticks = binfile.Length() / 10;
			long duration = bin_n_ticks / 1024; //duration in s
			int hours = (int)(duration / 3600);
			int minutes = (int)((duration % 3600) / 60);
			int seconds = (int)(duration % 60);
			string time_duration = string.Format("%02d:%02d:%02d", hours, minutes, seconds);
			Log.Error("ERROR", "time duration : " + time_duration);
			SimpleDateFormat sdf2 = new SimpleDateFormat("yy-MM-dd HH:mm:ss");
			string DT_start_string = start_DT.ToString("yy-MM-dd HH:mm:ss");
			string sensitivity;

			switch (_sn.ElementAt(3))
			{
				case '0':
					if (_sensitivity == 0x02)
					{
						sensitivity = "2g";
					}
					else
					{
						sensitivity = "6g";
					}
					break;
				case '1':
					sensitivity = "15g";
					break;
				case '2':
					sensitivity = "200g";
					break;
				default:
					sensitivity = "0g";
					break;
			}

			try
			{

				FileOutputStream fp = new FileOutputStream(logfile);
				PrintWriter pw = new PrintWriter(logfile);
				string log = "Serial:" + _sn + " Start at (yy-mm-dd hh:mm:ss):" + DT_start_string + " Time duration:" + time_duration + " Mode:" + sensitivity;
				pw.Println(log);
				pw.Flush();
				pw.Close();
				fp.Close();
				istToRead = new FileInputStream(binfile);
			}
			catch (IOException e)
			{
				string message = e.Message;
			}

			try
			{
				if (ContextCompat.CheckSelfPermission(Application.Context, Manifest.Permission.WriteExternalStorage) != (int)Permission.Granted)
				{
					RequestPermissions(new string[] { Manifest.Permission.WriteExternalStorage }, 101);
				}

				if (ContextCompat.CheckSelfPermission(Application.Context, Manifest.Permission.ReadExternalStorage) != (int)Permission.Granted)
				{
					RequestPermissions(new string[] { Manifest.Permission.ReadExternalStorage }, 101);
				}

				FillViewerFile();
			}
			catch (Exception ez)
			{
				string mess = ez.Message;
			}
		}
		public void FillViewerFile()
		{
			Task.Run(() =>
			{
				byte[] ticks_buf = new byte[4];
				byte bytesEc = (byte)0x00;
				byte[] ax_buf = new byte[2];
				byte[] ay_buf = new byte[2];
				byte[] az_buf = new byte[2];
				//File viewer_file = new File(viewerFolderPath, "DATA.BIN");
				string viewer_file = System.IO.Path.Combine(viewerFolderPath, "DATA.BIN");
				if (System.IO.File.Exists(viewer_file))
				{
					System.IO.File.Delete(viewer_file);
				}
				long nlines_to_read = binfile.Length() / 10;
				long nlines_read = 0;
				double progress = 0; //[%]
				int previousPercent = 0;
				ticks_value = 0;
				try
				{
					try
					{
						FileOutputStream o = new FileOutputStream(viewer_file, false);
						while (Read1Line(istToRead, ticks_buf, ax_buf, ay_buf, az_buf) != -1)
						{
							ticks_buf = Encoding.ASCII.GetBytes(ByteBuffer.Allocate(4).Order(ByteOrder.LittleEndian).PutInt((int)ticks_value).ToString());
							o.Write(ticks_buf);
							o.Write(bytesEc);
							o.Write(ax_buf);
							o.Write(ay_buf);
							o.Write(az_buf);
							ticks_value++;
							nlines_read++;
							progress = 100 * ((double)nlines_read) / ((double)nlines_to_read);
							dl_progress = (int)progress;
							if (dl_progress > previousPercent)
							{
								Xamarin.Forms.MessagingCenter.Send<object, int>(this, "downloadProgress", dl_progress);
								previousPercent = dl_progress;
							}
						}
						o.Close();
						istToRead.Close();
					}
					catch (Exception ex)
					{
						string message = ex.Message;
					}
				}
				catch (IOException e)
				{
					string message = e.Message;
				}
				catch (Exception ex)
				{
					string message = ex.Message;
				}
			});
		}
		public int Read1Line(FileInputStream inputStream, byte[] ticks, byte[] ax, byte[] ay, byte[] az)
		{
			int res = 0;
			int bytesread = 0;
			byte[] buf = new byte[10];
			int i;
			try
			{
				while ((i = inputStream.Read()) != -1)
				{
					buf[bytesread] = (byte)i;
					//Log.i(TAG,"byte : " + (byte) i);
					bytesread++;
					if (bytesread >= 10)
					{
						break;
					}
				}
				if (i == -1)
				{
					res = -1;
				}
				else
				{
					Array.Copy(buf, 0, ticks, 0, 4); // copy 4 bytes into ticks pos 0 from buf pos 0
					Array.Copy(buf, 4, ax, 0, 2); // copy 2 bytes into ax pos 0 from buf pos 5
					Array.Copy(buf, 6, ay, 0, 2); // copy 2 bytes into ay pos 0 from buf pos 7
					Array.Copy(buf, 8, az, 0, 2); // copy 2 bytes into az pos 0 from buf pos 9
					res = 0;
				}
			}
			catch (IOException e)
			{
				_ = e.Message;
			}
			return res;
		}
		public byte Get_Sensitivity()
		{
			byte[] bufferRcvd;
			Get_Method((byte)Func.EVENT_EXT_GET_PARAM, (byte)ParamId.SENSITIVITY_ID, (byte)ParamType.INT8);
			bufferRcvd = ReceiveData();

			byte sensitivity = (byte)Compute_DataType(bufferRcvd);
			try
			{
				//Log.d(TAG, "hexadecimal " + getHexString(bufferRcvd)+" float "+ ax_gain_2g);
			}
			catch (Exception e)
			{
				Log.Error("Error", e.Message);
			}
			return sensitivity;

		}
		public float Get_Ax_Gain_2G()
		{

			byte[] bufferRcvd;

			float ax_gain_2g;
			Get_Method((byte)Func.EVENT_EXT_GET_PARAM, (byte)ParamId.AX_GAIN_2G_ID, (byte)ParamType.FLOAT32);
			bufferRcvd = ReceiveData();

			ax_gain_2g = (float)Compute_DataType(bufferRcvd);
			try
			{
				//Log.d(TAG, "hexadecimal " + getHexString(bufferRcvd)+" float "+ ax_gain_2g);
			}
			catch (Exception e)
			{
				Log.Error("Error", e.Message);
			}
			return ax_gain_2g;

		}
		public float Get_Ay_Gain_2G()
		{

			byte[] bufferRcvd;

			float ay_gain_2g;
			Get_Method((byte)Func.EVENT_EXT_GET_PARAM, (byte)ParamId.AY_GAIN_2G_ID, (byte)ParamType.FLOAT32);
			bufferRcvd = ReceiveData();

			ay_gain_2g = (float)Compute_DataType(bufferRcvd);
			try
			{
				//Log.d(TAG, "hexadecimal " + getHexString(bufferRcvd)+" float "+ ay_gain_2g);
			}
			catch (Exception e)
			{
				Log.Error("Error", e.Message);
			}
			return ay_gain_2g;

		}
		public float Get_Az_Gain_2G()
		{

			byte[] bufferRcvd;

			float az_gain_2g;
			Get_Method((byte)Func.EVENT_EXT_GET_PARAM, (byte)ParamId.AZ_GAIN_2G_ID, (byte)ParamType.FLOAT32);
			bufferRcvd = ReceiveData();

			az_gain_2g = (float)Compute_DataType(bufferRcvd);
			try
			{
				//Log.d(TAG, "hexadecimal " + getHexString(bufferRcvd)+" float "+ az_gain_2g);
			}
			catch (Exception e)
			{
				Log.Error("Error", e.Message);
			}
			return az_gain_2g;

		}
		public float Get_Ax_Offset_2G()
		{

			byte[] bufferRcvd;

			float ax_offset_2g;
			Get_Method((byte)Func.EVENT_EXT_GET_PARAM, (byte)ParamId.AX_OFFSET_2G_ID, (byte)ParamType.FLOAT32);
			bufferRcvd = ReceiveData();
			ax_offset_2g = (float)Compute_DataType(bufferRcvd);
			try
			{
				//Log.d(TAG, "hexadecimal " + getHexString(bufferRcvd)+" float "+ ax_offset_2g);
			}
			catch (Exception e)
			{
				Log.Error("Error", e.Message);
			}
			return ax_offset_2g;

		}
		public float Get_Ay_Offset_2G()
		{

			byte[] bufferRcvd;

			float ay_offset_2g;
			Get_Method((byte)Func.EVENT_EXT_GET_PARAM, (byte)ParamId.AY_OFFSET_2G_ID, (byte)ParamType.FLOAT32);
			bufferRcvd = ReceiveData();
			ay_offset_2g = (float)Compute_DataType(bufferRcvd);
			try
			{
				//Log.d(TAG, "hexadecimal " + getHexString(bufferRcvd)+" float "+ ay_offset_2g);
			}
			catch (Exception e)
			{
				Log.Error("Error", e.Message);
			}
			return ay_offset_2g;

		}
		public float Get_Az_Offset_2G()
		{
			byte[] bufferRcvd;
			float az_offset_2g;
			Get_Method((byte)Func.EVENT_EXT_GET_PARAM, (byte)ParamId.AZ_OFFSET_2G_ID, (byte)ParamType.FLOAT32);
			bufferRcvd = ReceiveData();
			az_offset_2g = (float)Compute_DataType(bufferRcvd);
			try
			{
				//Log.d(TAG, "hexadecimal " + getHexString(bufferRcvd)+" float "+ az_offset_2g);
			}
			catch (Exception e)
			{
				Log.Error("Error", e.Message);
			}
			return az_offset_2g;

		}
		public float Get_Ax_Gain_6G()
		{

			byte[] bufferRcvd;

			float ax_gain_6g;
			Get_Method((byte)Func.EVENT_EXT_GET_PARAM, (byte)ParamId.AX_GAIN_6G_ID, (byte)ParamType.FLOAT32);
			bufferRcvd = ReceiveData();

			ax_gain_6g = (float)Compute_DataType(bufferRcvd);
			try
			{
				//Log.d(TAG, "hexadecimal " + getHexString(bufferRcvd)+" float "+ ax_gain_6g);
			}
			catch (Exception e)
			{
				Log.Error("Error", e.Message);
			}
			return ax_gain_6g;

		}
		public float Get_Ay_Gain_6G()
		{

			byte[] bufferRcvd;

			float ay_gain_6g;
			Get_Method((byte)Func.EVENT_EXT_GET_PARAM, (byte)ParamId.AY_GAIN_6G_ID, (byte)ParamType.FLOAT32);
			bufferRcvd = ReceiveData();

			ay_gain_6g = (float)Compute_DataType(bufferRcvd);
			try
			{
				//Log.d(TAG, "hexadecimal " + getHexString(bufferRcvd)+" float "+ ay_gain_6g);
			}
			catch (Exception e)
			{
				Log.Error("Error", e.Message);
			}
			return ay_gain_6g;

		}
		public float Get_Az_Gain_6G()
		{

			byte[] bufferRcvd;

			float az_gain_6g;
			Get_Method((byte)Func.EVENT_EXT_GET_PARAM, (byte)ParamId.AZ_GAIN_6G_ID, (byte)ParamType.FLOAT32);
			bufferRcvd = ReceiveData();

			az_gain_6g = (float)Compute_DataType(bufferRcvd);
			try
			{
				//Log.d(TAG, "hexadecimal " + getHexString(bufferRcvd)+" float "+ az_gain_6g);
			}
			catch (Exception e)
			{
				Log.Error("Error", e.Message);
			}
			return az_gain_6g;

		}
		public float Get_Ax_Offset_6G()
		{

			byte[] bufferRcvd;

			float ax_offset_6g;
			Get_Method((byte)Func.EVENT_EXT_GET_PARAM, (byte)ParamId.AX_OFFSET_6G_ID, (byte)ParamType.FLOAT32);
			bufferRcvd = ReceiveData();
			ax_offset_6g = (float)Compute_DataType(bufferRcvd);
			try
			{
				//Log.d(TAG, "hexadecimal " + getHexString(bufferRcvd)+" float "+ ax_offset_6g);
			}
			catch (Exception e)
			{
				Log.Error("Error", e.Message);
			}
			return ax_offset_6g;

		}
		public float Get_Ay_Offset_6G()
		{

			byte[] bufferRcvd;

			float ay_offset_6g;
			Get_Method((byte)Func.EVENT_EXT_GET_PARAM, (byte)ParamId.AY_OFFSET_6G_ID, (byte)ParamType.FLOAT32);
			bufferRcvd = ReceiveData();
			ay_offset_6g = (float)Compute_DataType(bufferRcvd);
			try
			{
				//Log.d(TAG, "hexadecimal " + getHexString(bufferRcvd)+" float "+ ay_offset_6g);
			}
			catch (Exception e)
			{
				Log.Error("Error", e.Message);
			}
			return ay_offset_6g;

		}
		public float Get_Az_Offset_6G()
		{

			byte[] bufferRcvd;

			float az_offset_6g;
			Get_Method((byte)Func.EVENT_EXT_GET_PARAM, (byte)ParamId.AZ_OFFSET_6G_ID, (byte)ParamType.FLOAT32);
			bufferRcvd = ReceiveData();
			az_offset_6g = (float)Compute_DataType(bufferRcvd);
			try
			{
				//Log.d(TAG, "hexadecimal " + getHexString(bufferRcvd)+" float "+ az_offset_6g);
			}
			catch (Exception e)
			{
				Log.Error("Error", e.Message);
			}
			return az_offset_6g;

		}
		public DateTime Get_Start_DT()
		{

			byte[] bufferRcvd;

			DateTime start_DT;

			Get_Method((byte)Func.EVENT_EXT_GET_PARAM, (byte)ParamId.DT_START_ID, (byte)ParamType.DT);
			bufferRcvd = ReceiveData();
			start_DT = (DateTime)Compute_DataType(bufferRcvd);
			try
			{
				//Log.d(TAG, "hexadecimal " + getHexString(bufferRcvd)+" float "+ az_offset_6g);
			}
			catch (Exception e)
			{
				Log.Error("Error", e.Message);
			}
			return start_DT;

		}
		private async void CheckAppPermissionsAsync(string SD_id)
		{
			/*	if ((int)Build.VERSION.SdkInt < 23)
				{
					return;
				}
				else
				{
					if (PackageManager.CheckPermission(Manifest.Permission.ReadExternalStorage, PackageName) != Permission.Granted
						&& PackageManager.CheckPermission(Manifest.Permission.WriteExternalStorage, PackageName) != Permission.Granted)
					{
						var permissions = new string[] { Manifest.Permission.ReadExternalStorage, Manifest.Permission.WriteExternalStorage };
						RequestPermissions(permissions, 1);
					}
				}*/

			PermissionStatus readstatus = await Permissions.CheckStatusAsync<Permissions.StorageRead>();
			PermissionStatus writestatus = await Permissions.CheckStatusAsync<Permissions.StorageWrite>();

			if (readstatus == PermissionStatus.Granted && writestatus == PermissionStatus.Granted)
			{
				CreateViewerFile1(SD_id);
			}
			else
			{
				var read = await Permissions.RequestAsync<Permissions.StorageRead>();
				var write = await Permissions.RequestAsync<Permissions.StorageWrite>();

				if (read == PermissionStatus.Granted && write == PermissionStatus.Granted)
				{
					CreateViewerFile1(SD_id);
				}
			}
		}
		#endregion Download
	}
}