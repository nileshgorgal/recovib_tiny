﻿using Android.App;
using Android.Content;
using Android.Hardware.Usb;
using Android.Util;
using Android.Widget;
using Java.Nio;
using RecovibTiny.Droid.Helper;
using RecovibTiny.Helper;
using RecovibTiny.Interface;
using System;
using System.Collections.Generic;
using System.Text;
[assembly: Xamarin.Forms.Dependency(typeof(ComClass))]
namespace RecovibTiny.Droid.Helper
{
	[IntentFilter(new[] { "android.settings.MANAGE_ALL_FILES_ACCESS_PERMISSION" })]
	public class ComClass : IArmSetup
	{
		private readonly string TAG = "ComClassError";
		private UsbDevice device;
		private UsbInterface usbInterface;
		private UsbEndpoint endPointWrite;
		private UsbManager mUsbManager;
		private UsbEndpoint endPointRead;
		private UsbDeviceConnection connection;
		private PendingIntent mPermissionIntent;
		private List<byte> outputbuff;
		private Context mContext;
		private int packetSize;
		private int VendorID = 8263;//0x2047
		private int ProductIDTiny = 2379;//0x094B

		#region SetArm
		private void Device_detected()
		{
			mContext = Application.Context;
			mUsbManager = (UsbManager)mContext.GetSystemService(Context.UsbService);

			// List<CharSequence> list = new LinkedList<>();
			foreach (UsbDevice usbDevice in mUsbManager.DeviceList.Values)
			{
				if ((usbDevice.VendorId == VendorID) && (usbDevice.ProductId == ProductIDTiny))
				{
					device = usbDevice;
					usbInterface = device.GetInterface(1);
				}
			}

			connection = mUsbManager.OpenDevice(device);
			if (null != connection)
			{
				connection.ClaimInterface(usbInterface, true);            //Device connected - claim ownership over the interface
			}
			if (device != null)
			{
				if (device.VendorId == VendorID && device.ProductId == ProductIDTiny)
				{
					GetPoint();
				}
			}
			else
			{
				return;
			}
		}
		private void GetPoint()
		{
			if (mUsbManager.HasPermission(device))
			{
				System.Console.WriteLine(" device connected.");
				try
				{
					//Direction of end point 1 - OUT - from host to device
					if (0 == usbInterface.GetEndpoint(1).Direction)
					{
						endPointWrite = usbInterface.GetEndpoint(1);
					}
				}
				catch (Exception e)
				{
					//Log.e(TAG, "Device have no endPointWrite", e);
				}
				try
				{
					//Direction of end point 0 - IN - from device to host
					if (0 != usbInterface.GetEndpoint(0).Direction)
					{
						endPointRead = usbInterface.GetEndpoint(0);
						packetSize = endPointRead.MaxPacketSize;
					}
				}
				catch (Exception e)
				{
					//Log.e(TAG, "Device have no endPointRead", e);
				}
			}
			else
			{
				//	handler_getPath.postDelayed(runnable_getPath, 1000);
				mUsbManager.RequestPermission(device, mPermissionIntent);
			}
		}
		public void InitArmSetup(byte pSensitivity, DateTime startAt, DateTime stopAt, DateTime durationNow)
		{
			try
			{
				outputbuff = new List<byte>();
				Device_detected();
				RunArmSetup(pSensitivity, startAt, stopAt, durationNow);
			}
			catch (Exception ex)
			{
				Log.Error(TAG, ex.Message);
			}
		}
		private void RunArmSetup(byte pSensitivity, DateTime startAt, DateTime stopAt, DateTime durationNow)
		{
			try
			{
				lock (this)
				{
					byte[] dateBuffer = new byte[14];
					byte[] bufferRcvd;

					SetSensitivity(pSensitivity);
					var startDate = new DateTimeOffset(startAt);
					var stopDate = new DateTimeOffset(stopAt);

					dateBuffer = ComputeDateTime(DateTime.Now);
					Set_Method((byte)Func.EVENT_EXT_SET_PARAM, ParamId.DT_ARM_ID, ParamType.DT, dateBuffer);
					dateBuffer = null;
					bufferRcvd = ReceiveData();
					bufferRcvd = null;

					dateBuffer = ComputeDateTime(startAt);
					Set_Method((byte)Func.EVENT_EXT_SET_PARAM, ParamId.DT_START_ID, ParamType.DT, dateBuffer);
					dateBuffer = null;
					bufferRcvd = ReceiveData();
					bufferRcvd = null;

					dateBuffer = ComputeDateTime(stopAt);
					Set_Method((byte)Func.EVENT_EXT_SET_PARAM, ParamId.DT_STOP_ID, ParamType.DT, dateBuffer);
					dateBuffer = null;
					bufferRcvd = ReceiveData();
					bufferRcvd = null;

					long ms_to_start = new DateTimeOffset(startAt.ToUniversalTime()).ToUnixTimeMilliseconds() - new DateTimeOffset(durationNow.ToUniversalTime()).ToUnixTimeMilliseconds();
					long ms_to_stop = new DateTimeOffset(stopAt.ToUniversalTime()).ToUnixTimeMilliseconds() - new DateTimeOffset(durationNow.ToUniversalTime()).ToUnixTimeMilliseconds();
					if (!(ms_to_start >= ms_to_stop) && !(ms_to_start <= 0) && !(ms_to_stop <= 0))
					{
						int ticks_to_start = (int)ms_to_start / 1000;
						int ticks_to_stop = (int)ms_to_stop / 1000;

						ticks_to_start *= 1024;
						ticks_to_stop *= 1024;

						SetArm(ticks_to_start, ticks_to_stop - 1);
						bufferRcvd = ReceiveData();
						bufferRcvd = null;
					}
					else
					{
						Toast.MakeText(mContext, "Bad timing configuration..!", ToastLength.Long).Show();
					}
				}
			}
			catch (Exception ex)
			{
				Log.Error(TAG, ex.Message);
			}
		}
		public void SetSensitivity(byte sensitivity)
		{

			byte[] data = new byte[1];
			data[0] = sensitivity;
			Set_Method((byte)Func.EVENT_EXT_SET_PARAM, (byte)ParamId.SENSITIVITY_ID, (byte)ParamType.INT8, data);

			byte[] bufferRcvd;
			bufferRcvd = ReceiveData();
			bufferRcvd = null;
		}
		public void SetArm(int start_tick, int stop_tick)
		{
			byte[] arrARM = new byte[12];
			arrARM[0] = Func.EVENT_EXT_ARM;
			arrARM[1] = 0x00;
			arrARM[2] = 0x11; // dummy
			arrARM[3] = 0x12; // dummy 

			byte[] startTicks = new byte[4];
			ByteBuffer startTicksBuffer = ByteBuffer.Allocate(4).Order(ByteOrder.LittleEndian);
			startTicksBuffer.PutInt(start_tick);
			if (startTicksBuffer.HasArray)
			{
				startTicks = Encoding.ASCII.GetBytes(startTicksBuffer.ToString());
			}

			byte[] stopTicks = new byte[4];
			ByteBuffer stopTicksBuffer = ByteBuffer.Allocate(4).Order(ByteOrder.LittleEndian);
			stopTicksBuffer.PutInt(stop_tick);
			stopTicks = Encoding.ASCII.GetBytes(stopTicksBuffer.ToString());

			Array.Copy(startTicks, 0, arrARM, 4, 4);
			Array.Copy(stopTicks, 0, arrARM, 8, 4);

			Set_Method_event(arrARM);
		}
		public void Set_Method_event(byte[] bufferSet)
		{
			List<byte> list = new List<byte>();
			outputbuff.Clear();
			foreach (byte b in bufferSet)
			{
				list.Add(b);
			}
			outputbuff = list;
			SendData(outputbuff);
		}
		public int SendData(List<byte> buffIn)
		{
			int status = 0;
			if (device != null && endPointWrite != null && mUsbManager.HasPermission(device))
			{
				outputbuff = new List<byte>(buffIn);
				byte[] report = new byte[64];
				report[0] = 0x3f;
				report[1] = (byte)outputbuff.Count;
				for (int i = 0; i < outputbuff.Count; i++)
				{
					report[i + 2] = outputbuff[i];
				}

				status = connection.BulkTransfer(endPointWrite, report, report.Length, 1024);
				//Log.d("MessageProcesss", "Status send : " + status);

			}
			return status;
		}
		public void Set_Method(byte Event, byte idFunction, byte typeFunction, byte[] bufferSet)
		{
			List<Byte> list = new List<byte>();
			outputbuff.Clear();
			outputbuff.Add(Event);
			outputbuff.Add(idFunction);
			outputbuff.Add(typeFunction);
			foreach (byte b in bufferSet)
			{
				list.Add(b);
			}

			foreach (Byte lst in list)
			{
				outputbuff.Add(lst);
			}

			SendData(outputbuff);
		}
		public byte[] ReceiveData()
		{
			// STATE_RECEIVE = 1;
			int size = 0;
			StringBuilder stringBuilder = new StringBuilder();        //Create new instances of the StringBuilder every time this method is called

			byte[] buffer = new byte[packetSize];

			if (connection != null && endPointRead != null)
			{    //Verify USB connection and data at end point
				int status = connection.BulkTransfer(endPointRead, buffer, packetSize, 1024);    //Read 64 bytes of data from end point 0 and store
																								 // Log.d("MessageProcesss", "Status rcvd : " + status);

			}

			byte[] frameCut = new byte[buffer.Length - 2];
			Array.Copy(buffer, 2, frameCut, 0, buffer.Length - 2);


			// STATE_RECEIVE = 0;
			return frameCut;
		}
		public byte[] ComputeDateTime(DateTime toCompute)
		{
			DateTime dt_compute = toCompute;

			int year, month, day, hour, minute, second, millisecond;

			year = dt_compute.Year;
			month = dt_compute.Month;
			day = dt_compute.Day;
			hour = dt_compute.Hour;
			minute = dt_compute.Minute;
			second = dt_compute.Second;
			millisecond = dt_compute.Millisecond;

			//convert the date
			byte[] data = new byte[14];
			data[0] = (byte)(year & 0xFF);
			data[1] = (byte)((year >> 8) & 0xFF);

			data[2] = (byte)(month & 0xFF);
			data[3] = (byte)((month >> 8) & 0xFF);

			data[4] = (byte)(day & 0xFF);
			data[5] = (byte)((day >> 8) & 0xFF);

			data[6] = (byte)(hour & 0xFF);
			data[7] = (byte)((hour >> 8) & 0xFF);

			data[8] = (byte)(minute & 0xFF);
			data[9] = (byte)((minute >> 8) & 0xFF);

			data[10] = (byte)(second & 0xFF);
			data[11] = (byte)((second >> 8) & 0xFF);

			data[12] = (byte)(millisecond & 0xFF);
			data[13] = (byte)((millisecond >> 8) & 0xFF);
			return data;
		}
		#endregion SetArm
	}
}