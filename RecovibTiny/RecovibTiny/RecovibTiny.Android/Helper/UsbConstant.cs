﻿namespace RecovibTiny.Droid.Helper
{
	public class UsbConstant : Java.Lang.Object
	{
		public const int UsbEndpointXferBulk = 2;
		public const int UsbDirOut = 0;
		public const int UsbDirIn = 128;
	}
}