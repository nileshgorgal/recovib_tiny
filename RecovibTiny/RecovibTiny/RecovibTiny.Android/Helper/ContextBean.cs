﻿using Android.Content;

namespace RecovibTiny.Droid.Helper
{
	public class ContextBean
	{
		private static Context localContext;
		public static Context GetLocalContext()
		{

			return localContext;
		}
		public static void SetLocalContext(Context localContext)
		{
			ContextBean.localContext = localContext;
		}
	}
}