﻿using Android.Content;
using Android.Hardware.Usb;
using Android.Util;
using Android.Views;
using Android.Widget;
using Java.IO;
using Java.Nio;
using Java.Text;
using Java.Util;
using Java.Util.Concurrent;
using RecovibTiny.Droid.NativeLargeArray;
using RecovibTiny.Droid.ScichartActivities;
using RecovibTiny.Helper;
using SciChart.Charting.Visuals.Axes;
using SciChart.Core.Model;
using SciChart.Data.Model;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace RecovibTiny.Droid.Helper
{
	public class AnalysisClass
	{
		private string TAG = "AnnalysisError";
		private UsbDevice device;
		private UsbEndpoint endPointWrite;
		private UsbManager mUsbManager;
		private UsbEndpoint endPointRead;
		private UsbDeviceConnection connection;
		private int packetSize;
		private List<Byte> outputbuff;
		private Context context;

		private float _ax_gain_2g;
		private float _ay_gain_2g;
		private float _az_gain_2g;
		private float _ax_offset_2g;
		private float _ay_offset_2g;
		private float _az_offset_2g;
		private float _ax_gain_6g;
		private float _ay_gain_6g;
		private float _az_gain_6g;
		private float _ax_offset_6g;
		private float _ay_offset_6g;
		private float _az_offset_6g;
		public void Setcontext(Context value)
		{
			this.context = value;
		}
		public AnalysisClass(UsbEndpoint endPointWrite, UsbEndpoint endPointRead, UsbDevice device, UsbManager mUsbManager, UsbDeviceConnection connection, int packetSize)
		{
			//this.context = context;
			this.connection = connection;
			this.endPointWrite = endPointWrite;
			this.endPointRead = endPointRead;
			this.device = device;
			this.mUsbManager = mUsbManager;
			this.packetSize = packetSize;
			outputbuff = new List<byte>();
		}
		#region getter/setter
		//Retrieve params from RecoVIB Feel
		public void Ext_Event_Get_Params()
		{
			try
			{
				_ax_gain_2g = Get_Ax_Gain_2G();
				_ay_gain_2g = Get_Ay_Gain_2G();
				_az_gain_2g = Get_Az_Gain_2G();
				_ax_offset_2g = Get_Ax_Offset_2G();
				_ay_offset_2g = Get_Ay_Offset_2G();
				_az_offset_2g = Get_Az_Offset_2G();
				_ax_gain_6g = Get_Ax_Gain_6G();
				_ay_gain_6g = Get_Ay_Gain_6G();
				_az_gain_6g = Get_Az_Gain_6G();
				_ax_offset_6g = Get_Ax_Offset_6G();
				_ay_offset_6g = Get_Ay_Offset_6G();
				_az_offset_6g = Get_Az_Offset_6G();

				_sn = GetSN();
			}
			catch (UnsupportedEncodingException e)
			{
			}

		}
		public byte Get_Sensitivity()
		{
			byte[] bufferRcvd;
			Get_Method((byte)Func.EVENT_EXT_GET_PARAM, (byte)ParamId.SENSITIVITY_ID, (byte)ParamType.INT8);
			bufferRcvd = ReceiveData();

			byte sensitivity = (byte)Compute_DataType(bufferRcvd);
			try
			{
				//Log.d(TAG, "hexadecimal " + getHexString(bufferRcvd)+" float "+ ax_gain_2g);
			}
			catch (Exception e)
			{
				Log.Error("Error", e.Message);
			}
			return sensitivity;

		}
		public float Get_Ax_Gain_2G()
		{

			byte[] bufferRcvd;

			float ax_gain_2g;
			Get_Method((byte)Func.EVENT_EXT_GET_PARAM, (byte)ParamId.AX_GAIN_2G_ID, (byte)ParamType.FLOAT32);
			bufferRcvd = ReceiveData();

			ax_gain_2g = (float)Compute_DataType(bufferRcvd);
			try
			{
				//Log.d(TAG, "hexadecimal " + getHexString(bufferRcvd)+" float "+ ax_gain_2g);
			}
			catch (Exception e)
			{
				Log.Error("Error", e.Message);
			}
			return ax_gain_2g;

		}
		public float Get_Ay_Gain_2G()
		{

			byte[] bufferRcvd;

			float ay_gain_2g;
			Get_Method((byte)Func.EVENT_EXT_GET_PARAM, (byte)ParamId.AY_GAIN_2G_ID, (byte)ParamType.FLOAT32);
			bufferRcvd = ReceiveData();

			ay_gain_2g = (float)Compute_DataType(bufferRcvd);
			try
			{
				//Log.d(TAG, "hexadecimal " + getHexString(bufferRcvd)+" float "+ ay_gain_2g);
			}
			catch (Exception e)
			{
				Log.Error("Error", e.Message);
			}
			return ay_gain_2g;

		}
		public float Get_Az_Gain_2G()
		{

			byte[] bufferRcvd;

			float az_gain_2g;
			Get_Method((byte)Func.EVENT_EXT_GET_PARAM, (byte)ParamId.AZ_GAIN_2G_ID, (byte)ParamType.FLOAT32);
			bufferRcvd = ReceiveData();

			az_gain_2g = (float)Compute_DataType(bufferRcvd);
			try
			{
				//Log.d(TAG, "hexadecimal " + getHexString(bufferRcvd)+" float "+ az_gain_2g);
			}
			catch (Exception e)
			{
				Log.Error("Error", e.Message);
			}
			return az_gain_2g;

		}
		public float Get_Ax_Offset_2G()
		{

			byte[] bufferRcvd;

			float ax_offset_2g;
			Get_Method((byte)Func.EVENT_EXT_GET_PARAM, (byte)ParamId.AX_OFFSET_2G_ID, (byte)ParamType.FLOAT32);
			bufferRcvd = ReceiveData();
			ax_offset_2g = (float)Compute_DataType(bufferRcvd);
			try
			{
				//Log.d(TAG, "hexadecimal " + getHexString(bufferRcvd)+" float "+ ax_offset_2g);
			}
			catch (Exception e)
			{
				Log.Error("Error", e.Message);
			}
			return ax_offset_2g;

		}
		public float Get_Ay_Offset_2G()
		{

			byte[] bufferRcvd;

			float ay_offset_2g;
			Get_Method((byte)Func.EVENT_EXT_GET_PARAM, (byte)ParamId.AY_OFFSET_2G_ID, (byte)ParamType.FLOAT32);
			bufferRcvd = ReceiveData();
			ay_offset_2g = (float)Compute_DataType(bufferRcvd);
			try
			{
				//Log.d(TAG, "hexadecimal " + getHexString(bufferRcvd)+" float "+ ay_offset_2g);
			}
			catch (Exception e)
			{
				Log.Error("Error", e.Message);
			}
			return ay_offset_2g;

		}
		public float Get_Az_Offset_2G()
		{
			byte[] bufferRcvd;
			float az_offset_2g;
			Get_Method((byte)Func.EVENT_EXT_GET_PARAM, (byte)ParamId.AZ_OFFSET_2G_ID, (byte)ParamType.FLOAT32);
			bufferRcvd = ReceiveData();
			az_offset_2g = (float)Compute_DataType(bufferRcvd);
			try
			{
				//Log.d(TAG, "hexadecimal " + getHexString(bufferRcvd)+" float "+ az_offset_2g);
			}
			catch (Exception e)
			{
				Log.Error("Error", e.Message);
			}
			return az_offset_2g;

		}
		public float Get_Ax_Gain_6G()
		{

			byte[] bufferRcvd;

			float ax_gain_6g;
			Get_Method((byte)Func.EVENT_EXT_GET_PARAM, (byte)ParamId.AX_GAIN_6G_ID, (byte)ParamType.FLOAT32);
			bufferRcvd = ReceiveData();

			ax_gain_6g = (float)Compute_DataType(bufferRcvd);
			try
			{
				//Log.d(TAG, "hexadecimal " + getHexString(bufferRcvd)+" float "+ ax_gain_6g);
			}
			catch (Exception e)
			{
				Log.Error("Error", e.Message);
			}
			return ax_gain_6g;

		}
		public float Get_Ay_Gain_6G()
		{

			byte[] bufferRcvd;

			float ay_gain_6g;
			Get_Method((byte)Func.EVENT_EXT_GET_PARAM, (byte)ParamId.AY_GAIN_6G_ID, (byte)ParamType.FLOAT32);
			bufferRcvd = ReceiveData();

			ay_gain_6g = (float)Compute_DataType(bufferRcvd);
			try
			{
				//Log.d(TAG, "hexadecimal " + getHexString(bufferRcvd)+" float "+ ay_gain_6g);
			}
			catch (Exception e)
			{
				Log.Error("Error", e.Message);
			}
			return ay_gain_6g;

		}
		public float Get_Az_Gain_6G()
		{

			byte[] bufferRcvd;

			float az_gain_6g;
			Get_Method((byte)Func.EVENT_EXT_GET_PARAM, (byte)ParamId.AZ_GAIN_6G_ID, (byte)ParamType.FLOAT32);
			bufferRcvd = ReceiveData();

			az_gain_6g = (float)Compute_DataType(bufferRcvd);
			try
			{
				//Log.d(TAG, "hexadecimal " + getHexString(bufferRcvd)+" float "+ az_gain_6g);
			}
			catch (Exception e)
			{
				Log.Error("Error", e.Message);
			}
			return az_gain_6g;

		}
		public float Get_Ax_Offset_6G()
		{

			byte[] bufferRcvd;

			float ax_offset_6g;
			Get_Method((byte)Func.EVENT_EXT_GET_PARAM, (byte)ParamId.AX_OFFSET_6G_ID, (byte)ParamType.FLOAT32);
			bufferRcvd = ReceiveData();
			ax_offset_6g = (float)Compute_DataType(bufferRcvd);
			try
			{
				//Log.d(TAG, "hexadecimal " + getHexString(bufferRcvd)+" float "+ ax_offset_6g);
			}
			catch (Exception e)
			{
				Log.Error("Error", e.Message);
			}
			return ax_offset_6g;

		}
		public float Get_Ay_Offset_6G()
		{

			byte[] bufferRcvd;

			float ay_offset_6g;
			Get_Method((byte)Func.EVENT_EXT_GET_PARAM, (byte)ParamId.AY_OFFSET_6G_ID, (byte)ParamType.FLOAT32);
			bufferRcvd = ReceiveData();
			ay_offset_6g = (float)Compute_DataType(bufferRcvd);
			try
			{
				//Log.d(TAG, "hexadecimal " + getHexString(bufferRcvd)+" float "+ ay_offset_6g);
			}
			catch (Exception e)
			{
				Log.Error("Error", e.Message);
			}
			return ay_offset_6g;

		}
		public float Get_Az_Offset_6G()
		{

			byte[] bufferRcvd;

			float az_offset_6g;
			Get_Method((byte)Func.EVENT_EXT_GET_PARAM, (byte)ParamId.AZ_OFFSET_6G_ID, (byte)ParamType.FLOAT32);
			bufferRcvd = ReceiveData();
			az_offset_6g = (float)Compute_DataType(bufferRcvd);
			try
			{
				//Log.d(TAG, "hexadecimal " + getHexString(bufferRcvd)+" float "+ az_offset_6g);
			}
			catch (Exception e)
			{
				Log.Error("Error", e.Message);
			}
			return az_offset_6g;

		}
		public DateTime Get_Start_DT()
		{

			byte[] bufferRcvd;

			DateTime start_DT;

			Get_Method((byte)Func.EVENT_EXT_GET_PARAM, (byte)ParamId.DT_START_ID, (byte)ParamType.DT);
			bufferRcvd = ReceiveData();
			start_DT = (DateTime)Compute_DataType(bufferRcvd);
			try
			{
				//Log.d(TAG, "hexadecimal " + getHexString(bufferRcvd)+" float "+ az_offset_6g);
			}
			catch (Exception e)
			{
				Log.Error("Error", e.Message);
			}
			return start_DT;

		}
		public void Get_Method(Byte EventType, Byte idFunction, Byte ParamType)
		{
			outputbuff.Clear();
			outputbuff.Add(EventType);
			outputbuff.Add(idFunction);
			outputbuff.Add(ParamType);
			SendData(outputbuff);
		}
		public int SendData(List<byte> buffIn)
		{

			int status = 0;
			if (device != null && endPointWrite != null && mUsbManager.HasPermission(device))
			{
				outputbuff = new List<byte>(buffIn);
				byte[] report = new byte[64];
				report[0] = 0x3f;
				report[1] = (byte)outputbuff.Count;
				for (int i = 0; i < outputbuff.Count; i++)
				{
					report[i + 2] = outputbuff[i];
				}

				status = connection.BulkTransfer(endPointWrite, report, report.Length, 1024);
				//Log.d("MessageProcesss", "Status send : " + status);

			}
			return status;
		}
		public byte[] ReceiveData()
		{
			// STATE_RECEIVE = 1;
			int size = 0;
			StringBuilder stringBuilder = new StringBuilder();        //Create new instances of the StringBuilder every time this method is called

			byte[] buffer = new byte[packetSize];

			if (connection != null && endPointRead != null)
			{    //Verify USB connection and data at end point
				int status = connection.BulkTransfer(endPointRead, buffer, packetSize, 1024);    //Read 64 bytes of data from end point 0 and store
																								 // Log.d("MessageProcesss", "Status rcvd : " + status);

			}

			byte[] frameCut = new byte[buffer.Length - 2];
			Array.Copy(buffer, 2, frameCut, 0, buffer.Length - 2);


			// STATE_RECEIVE = 0;
			return frameCut;
		}
		public object Compute_DataType(byte[] rcvd)
		{
			byte dataType;
			dataType = rcvd[2];
			byte[] frameCut = new byte[rcvd.Length - 3];
			Array.Copy(rcvd, 3, frameCut, 0, rcvd.Length - 3);
			if (dataType == ParamType.SN8)
			{
				byte[] dataRcvd = new byte[ParamType.SN8_SIZE];
				Array.Copy(frameCut, 0, dataRcvd, 0, ParamType.SN8_SIZE);

				Log.Error("MessageProcesss", "in the datatype");
				Java.Lang.String data;
				data = new Java.Lang.String(dataRcvd, "UTF-8");
				return (string)data;
			}

			if (dataType == ParamType.FLOAT32)
			{
				byte[] dataRcvd = new byte[ParamType.FLOAT32_SIZE];
				Array.Copy(frameCut, 0, dataRcvd, 0, ParamType.FLOAT32_SIZE);
				float data = ByteBuffer.Wrap(frameCut).Order(ByteOrder.LittleEndian).GetFloat(0);
				return data;
			}
			if (dataType == ParamType.DT)
			{
				byte[] byteDateBuffer = new byte[2];

				int year, month, day, hour, minute, second, millisecond;

				byteDateBuffer[0] = frameCut[0];
				byteDateBuffer[1] = frameCut[1];


				short year_s = ByteBuffer.Wrap(byteDateBuffer).Order(ByteOrder.LittleEndian).GetShort(0);
				year = year_s;


				byteDateBuffer[0] = frameCut[2];
				byteDateBuffer[1] = frameCut[3];

				short month_s = ByteBuffer.Wrap(byteDateBuffer).Order(ByteOrder.LittleEndian).GetShort(0);
				month = month_s;


				byteDateBuffer[0] = frameCut[4];
				byteDateBuffer[1] = frameCut[5];

				short day_s = ByteBuffer.Wrap(byteDateBuffer).Order(ByteOrder.LittleEndian).GetShort(0);
				day = day_s;


				byteDateBuffer[0] = frameCut[6];
				byteDateBuffer[1] = frameCut[7];

				short hour_s = ByteBuffer.Wrap(byteDateBuffer).Order(ByteOrder.LittleEndian).GetShort(0);

				hour = hour_s;


				byteDateBuffer[0] = frameCut[8];
				byteDateBuffer[1] = frameCut[9];

				short minute_s = ByteBuffer.Wrap(byteDateBuffer).Order(ByteOrder.LittleEndian).GetShort(0);
				minute = minute_s;


				byteDateBuffer[0] = frameCut[10];
				byteDateBuffer[1] = frameCut[11];

				short second_s = ByteBuffer.Wrap(byteDateBuffer).Order(ByteOrder.LittleEndian).GetShort(0);
				second = second_s;


				byteDateBuffer[0] = frameCut[12];
				byteDateBuffer[1] = frameCut[13];

				short millisecond_s = ByteBuffer.Wrap(byteDateBuffer).Order(ByteOrder.LittleEndian).GetShort(0);
				millisecond = millisecond_s;


				DateTime data = new DateTime(year, month, day, hour, minute, second, millisecond);

				return data;
			}

			if (dataType == ParamType.UINT16)
			{
				byte[] dataRcvd = new byte[ParamType.UINT16_SIZE];
				Array.Copy(frameCut, 0, dataRcvd, 0, ParamType.UINT16_SIZE);
				short data = ByteBuffer.Wrap(frameCut).Order(ByteOrder.LittleEndian).GetShort(0);
				return data;
			}

			if (dataType == ParamType.INT8)
			{
				return frameCut[0];
			}

			Log.Error("MessageProcesss", "null dataType");
			return null;
		}
		public string GetSN()
		{


			byte[] bufferRcvd;

			/* --GET THE SERIAL NUMBER --*/

			String SerialNumber;
			Get_Method((byte)Func.EVENT_EXT_GET_PARAM, (byte)ParamId.SN_ID, (byte)ParamType.SN8);
			bufferRcvd = ReceiveData();
			try
			{
				Log.Error("RCVD ", "hexadecimal" + GetHexString(bufferRcvd));
			}
			catch (Exception e)
			{
				e.StackTrace.ToString();
			}
			SerialNumber = (string)Compute_DataType(bufferRcvd);
			_sn = SerialNumber;


			return _sn;

		}
		public static string GetHexString(byte[] b)
		{
			String result = "";
			for (int i = 0; i < b.Length; i++)
			{
				result += ((b[i] & 0xff) + 0x100, 16).ToString().Substring(1);
			}
			return result;
		}
		#endregion getter/setter

		#region enum values
		//Filter Possibilities
		public enum FILTER_CHOICE
		{
			HP, LP, HP_LP, NO_FILTER
		}
		//Input Possibilities
		public enum INPUT_CHOICE
		{
			REAL_VALUES, RECORDED_VALUES
		}
		//Viewer Device Possibilities
		public enum MODE_CHOICE
		{
			ACCELERATION_MODE, VELOCITY_MODE, DISPLACEMENT_MODE
		}
		public enum WINDOW_CHOICE
		{
			RECTANGULAR_WINDOW, HANNING_WINDOW, FLAT_TOP_WINDOW
		}
		public enum FREQUENTIAL_CHOICE
		{
			AMPLITUDE_SPECTRUM_PEAK, AMPLITUDE_SPECTRUM_RMS, POWER_SPECTRUM, POWER_SPECTRAL_DENSITY, AMPLITUDE_SPECTRAL_DENSITY
		}

		#endregion enum values
		#region getset		
		//Recording flags
		private bool recordTemp;
		public void SetRecordTemp(bool value)
		{
			recordTemp = value;
		}
		//Recorded values reader speed multiplier
		private int speedMult;
		public void SetSpeedMult(int value)
		{
			speedMult = value;
		}
		public int GetSpeedMult()
		{
			return speedMult;
		}
		private float rms_tau;
		private double rms_m;
		private long rms_block_size;
		public void SetRms_tau(float value)
		{
			rms_tau = value;
			rms_m = (double)(1 - Java.Lang.Math.Exp((double)(-1 / (rms_tau * Sampling_Freq))));
			rms_block_size = (long)Java.Lang.Math.Floor(rms_tau * Sampling_Freq / 2.0);
		}
		private int Sampling_Freq;
		public int GetSampling_Freq()
		{
			return Sampling_Freq;
		}
		public void SetSampling_Freq(int value)
		{
			Sampling_Freq = value;
		}
		private int skipNextValue;
		public void SetSkipNextValue(int value)
		{
			skipNextValue = value;
		}
		public int GetSkipNextValue()
		{
			return skipNextValue;
		}
		private int nPointsToShow;
		public void SetnPointsToShow(int value)
		{
			nPointsToShow = value;
		}
		private int nPointsToKeep;
		public void SetnPointsToKeep(int value)
		{
			nPointsToKeep = value;
		}
		//Cutoff frequencies
		private double hp_filter_fc;
		public void Set_hp_filter_fc(double value)
		{
			hp_filter_fc = value;
		}
		public double Get_hp_filter_fc()
		{
			return hp_filter_fc;
		}

		private double lp_filter_fc;
		public void Set_lp_filter_fc(double value)
		{
			lp_filter_fc = value;
		}
		public double Get_lp_filter_fc()
		{
			return lp_filter_fc;
		}

		private double vel_filter_fc;
		public void Set_vel_filter_fc(double value)
		{
			vel_filter_fc = value;
		}
		public double Get_vel_filter_fc()
		{
			return vel_filter_fc;
		}

		private double disp_filter_fc;
		public void Set_disp_filter_fc(double value)
		{
			disp_filter_fc = value;
		}
		public double Get_disp_filter_fc()
		{
			return disp_filter_fc;
		}

		private FREQUENTIAL_CHOICE _freq_Choice_Update;
		public void Set_freq_Choice_Update(FREQUENTIAL_CHOICE value) { _freq_Choice_Update = value; }
		public FREQUENTIAL_CHOICE Get_freq_Choice_Update() { return _freq_Choice_Update; }
		private WINDOW_CHOICE _window_Choice;
		public void Set_window_Choice(WINDOW_CHOICE value) { _window_Choice = value; }
		public WINDOW_CHOICE Get_window_Choice() { return _window_Choice; }

		private float window_npbw;
		public void Set_window_npbw(float value) { window_npbw = value; }
		public float Get_window_npbw() { return window_npbw; }
		private MODE_CHOICE _mode_Choice;
		public void Set_mode_Choice(MODE_CHOICE value)
		{
			_mode_Choice = value;
		}
		public MODE_CHOICE Get_mode_Choice()
		{
			return _mode_Choice;
		}
		private FILTER_CHOICE _filter_Choice;
		public void Set_filter_Choice(FILTER_CHOICE value) { _filter_Choice = value; }
		public FILTER_CHOICE Get_filter_Choice() { return _filter_Choice; }

		private INPUT_CHOICE _input_choice;
		public void Set_input_Choice(INPUT_CHOICE value) { _input_choice = value; }
		public INPUT_CHOICE Get_input_Choice() { return _input_choice; }

		private bool yAutoScale;
		public void SetyAutoScale(bool value)
		{
			yAutoScale = value;
			DisplayActivity.yAxisT.AutoRange = yAutoScale ? AutoRange.Always : AutoRange.Never;
			DisplayActivity.yAxisF.AutoRange = yAutoScale ? AutoRange.Always : AutoRange.Never;
		}

		//True if the values are to be displayed in g or in m/s²
		private bool gUnits;
		public void SetgUnits(bool value)
		{
			gUnits = value;
		}
		public bool GetgUnits()
		{
			return gUnits;
		}

		//To distinguish 2g, 15g and 200g, and 6g during the conversion step
		private bool _2g_sensitivity;
		public void Set_2g_sensitivity(bool value) { _2g_sensitivity = value; }
		public bool Get_2g_sensitivity() { return _2g_sensitivity; }
		private int fft_Window_Size_Update;

		public void SetFft_Window_Size_Update(int value)
		{
			fft_Window_Size_Update = value;
		}
		public int GetFftWindowSizeUpdate()
		{
			return fft_Window_Size_Update;
		}

		private int averageDepth_update;
		public int GetAverageDepthUpdate()
		{
			return averageDepth_update;
		}
		public void SetAverageDepthUpdate(int value)
		{
			averageDepth_update = value;
		}

		private long startTimeMillis = 0;
		public void SetStartTimeMillis()
		{
			startTimeMillis = new DateTimeOffset(DateTime.Now.ToUniversalTime()).ToUnixTimeSeconds();
		}
		private long counterMillis = 0;
		public void SetCounterMillis(long value)
		{
			counterMillis = value;
		}

		//Recorded values reader skip next
		private bool skipNext;
		public void SetSkipNext(bool value)
		{
			skipNext = value;
		}
		public bool GetSkipNext()
		{
			return skipNext;
		}

		private string _sn;
		public void Set_sn(string value)
		{
			_sn = value;
		}
		public string Sn()
		{
			return _sn;
		}
		public void Set_ax_gain(float value)
		{
			if (_2g_sensitivity)
			{
				_ax_gain_2g = value;
			}
			else
			{
				_ax_gain_6g = value;
			}
		}
		public void Set_ay_gain(float value)
		{
			if (_2g_sensitivity)
			{
				_ay_gain_2g = value;
			}
			else
			{
				_ay_gain_6g = value;
			}
		}
		public void Set_az_gain(float value)
		{
			if (_2g_sensitivity)
			{
				_az_gain_2g = value;
			}
			else
			{
				_az_gain_6g = value;
			}
		}
		public void Set_ax_offset(float value)
		{
			if (_2g_sensitivity)
			{
				_ax_offset_2g = value;
			}
			else
			{
				_ax_offset_6g = value;
			}
		}
		public void Set_ay_offset(float value)
		{
			if (_2g_sensitivity)
			{
				_ay_offset_2g = value;
			}
			else
			{
				_ay_offset_6g = value;
			}
		}
		public void Set_az_offset(float value)
		{
			if (_2g_sensitivity)
			{
				_az_offset_2g = value;
			}
			else
			{
				_az_offset_6g = value;
			}
		}
		public float Get_ax_gain()
		{
			if (_2g_sensitivity)
			{
				return _ax_gain_2g;
			}
			else
			{
				return _ax_gain_6g;
			}
		}
		public float Get_ay_gain()
		{
			if (_2g_sensitivity)
			{
				return _ay_gain_2g;
			}
			else
			{
				return _ay_gain_6g;
			}
		}
		public float Get_az_gain()
		{
			if (_2g_sensitivity)
			{
				return _az_gain_2g;
			}
			else
			{
				return _az_gain_6g;
			}
		}
		public float Get_ax_offset()
		{
			if (_2g_sensitivity)
			{
				return _ax_offset_2g;
			}
			else
			{
				return _ax_offset_6g;
			}
		}
		public float Get_ay_offset()
		{
			if (_2g_sensitivity)
			{
				return _ay_offset_2g;
			}
			else
			{
				return _ay_offset_6g;
			}
		}
		public float Get_az_offset()
		{
			if (_2g_sensitivity)
			{
				return _az_offset_2g;
			}
			else
			{
				return _az_offset_6g;
			}
		}

		private bool closeFOS = false;
		public void SetCloseFOS(bool value)
		{
			closeFOS = value;
		}
		//Stop flag
		private bool _stop_Signal;
		public void Set_stop_Signal(bool value) { _stop_Signal = value; }
		//Pause flag (only in viewer mode)
		private bool _pause_Signal;
		public void Set_pause_Signal(bool value) { _pause_Signal = value; }
		public bool Get_pause_Signal() { return _pause_Signal; }
		#endregion getset

		#region GetSET
		//Worst situation use 1 HP and 2 LP filters at the same time
		private BiquadFilter HP_FILTER;
		private BiquadFilter LP_FILTER;
		private BiquadFilter LP_FILTER_2;
		private int averageDepth;
		private int fft_Window_Size;
		private FREQUENTIAL_CHOICE _freq_Choice;
		private FFT fft_1D;
		private Semaphore fifosem = new Semaphore(0);
		private Semaphore freqsem = new Semaphore(0);
		private Semaphore tempsem = new Semaphore(0);
		private Semaphore tempcopysem = new Semaphore(1);
		private Semaphore freqcopysem = new Semaphore(1);
		//the number of point to add to the chart a a time
		private int display_Block_Size;
		//Exponential RMS
		private float E_RMS_ax_converted_value, E_RMS_ay_converted_value, E_RMS_az_converted_value;
		private Java.Lang.Thread acq_thread;
		private Java.Lang.Thread treat_thread;
		private Java.Lang.Thread temp_thread;
		private Java.Lang.Thread freq_thread;
		private long recordingTimeLimit;
		public void Set_recordingTimeLimit(long value)
		{
			recordingTimeLimit = value * 60 * 1000;
		}
		public long Get_recordingTimeLimit()
		{
			return recordingTimeLimit;
		}
		private bool rtlRunnableSent = false;
		private bool fullFifoRunnableSent = false;
		public int STATE_RECEIVE = 0;
		//the depth of the FIFO of the Recovib Feel
		private int ACQ_FIFO_DEPTH = 500;
		private int _ticks_value;
		private long _ticks_converted_value;
		private float final_tick = -1.0f;
		private int _ax_value;
		private int _ay_value;
		private int _az_value;
		private float _ax_converted_value;
		private float _ay_converted_value;
		private float _az_converted_value;
		private Object fifo_lock = new Object();
		private Object list_lock = new Object();
		private bool clearAllBuffersFlag = false;
		const float GRAVITY = 9.80665f;
		//Moving visible range limits (xAxis) 
		double minRange;
		double maxRange;
		private FloatLargeArray fftBufferAx;
		private FloatLargeArray fftBufferAy;
		private FloatLargeArray fftBufferAz;
		#endregion GetSET
		#region Methods

		public void Ext_Event_Stop()
		{
			Get_Event_Method((byte)0x20);//EVENT_EXT_STOP signal sent
		}
		//To send an event to the RecoVIB Feel through HID
		public void Get_Event_Method(Byte Event)
		{
			outputbuff.Clear();
			outputbuff.Add(Event);
			SendData(outputbuff);
		}
		//Unsigned int doesn't exist in java. Ticks are sent as C unsigned int so a conversion is needed
		public long SignedIntToUnsignedLong(int mySignedInt)
		{
			return (long)(mySignedInt & 0xffffffffL);
		}
		public void Ext_Event_Go_On()
		{
			Get_Event_Method((byte)0x21);//EVENT_EXT_GO_ON signal sent
		}
		public int Read1Line(FileInputStream fileInputStream, byte[] ticks, byte[] ax, byte[] ay, byte[] az)
		{
			int res = 0;
			int bytesread = 0;
			byte[] buf = new byte[11];
			int i;
			try
			{
				while (((i = fileInputStream.Read()) != -1))
				{
					buf[bytesread] = (byte)i;
					//Log.i(TAG,"byte : " + (byte) i);
					bytesread++;
					if (bytesread >= 11)
					{
						break;
					}
				}
				if (i == -1)
				{
					res = -1;
				}
				else
				{
					Array.Copy(buf, 0, ticks, 0, 4); // copy 4 bytes into ticks pos 0 from buf pos 0
					Array.Copy(buf, 5, ax, 0, 2); // copy 2 bytes into ax pos 0 from buf pos 5
					Array.Copy(buf, 7, ay, 0, 2); // copy 2 bytes into ay pos 0 from buf pos 7
					Array.Copy(buf, 9, az, 0, 2); // copy 2 bytes into az pos 0 from buf pos 9
					res = 0;
				}
			}
			catch (IOException e)
			{
				e.PrintStackTrace();
			}
			return res;
		}

		public void SkipxLines(FileInputStream inputStream, long x)
		{
			//skip(n) method may skip less than x bytes
			long bytesSkipped = 0;
			while (bytesSkipped < x)
			{
				try
				{
					bytesSkipped = inputStream.Skip(x - bytesSkipped);
				}
				catch (IOException e)
				{
					e.PrintStackTrace();
				}
			}
		}
		public void VariableTempo(int readblock_size)
		{
			long currentTimeMillis = new DateTimeOffset(DateTime.Now.ToUniversalTime()).ToUnixTimeMilliseconds();
			switch (speedMult)
			{
				case 1:
					while (true)
					{
						if (currentTimeMillis >= Math.Round(counterMillis * readblock_size / 1.024) + startTimeMillis)
						{
							counterMillis++;
							break;
						}
					}
					break;
				case 2:
					while (true)
					{
						if (currentTimeMillis >= Math.Round(counterMillis * readblock_size / (2 * 1.024)) + startTimeMillis)
						{
							counterMillis++;
							break;
						}
					}
					break;
				case 3:
					while (true)
					{
						if (currentTimeMillis >= Math.Round(counterMillis * readblock_size / (4 * 1.024)) + startTimeMillis)
						{
							counterMillis++;
							break;
						}
					}
					break;
				case 4:
					while (true)
					{
						if (currentTimeMillis >= Math.Round(counterMillis * readblock_size / (8 * 1.024)) + startTimeMillis)
						{
							counterMillis++;
							break;
						}
					}
					break;
				case 5:
					while (true)
					{
						if (currentTimeMillis >= Math.Round(counterMillis * readblock_size / (16 * 1.024)) + startTimeMillis)
						{
							counterMillis++;
							break;
						}
					}
					break;
				case 6:
					while (true)
					{
						if (currentTimeMillis >= Math.Round(counterMillis * readblock_size / (32 * 1.024)) + startTimeMillis)
						{
							counterMillis++;
							break;
						}
					}
					break;
				case 7:
					while (true)
					{
						if (currentTimeMillis >= Math.Round(counterMillis * readblock_size / (64 * 1.024)) + startTimeMillis)
						{
							counterMillis++;
							break;
						}
					}
					break;
				default:
					break;
			}
		}
		public void Ext_Event_Start()
		{
			#region Done
			if (context == null)
			{
				context = ContextBean.GetLocalContext();
			}
			switch (_mode_Choice)
			{
				case MODE_CHOICE.ACCELERATION_MODE:
					HP_FILTER = new BiquadFilter(1, 1, 6, 2, BiquadFilter.FILTER_TYPE.HIGHPASS, 0, hp_filter_fc, Sampling_Freq);
					LP_FILTER = new BiquadFilter(1, 1, 6, 2, BiquadFilter.FILTER_TYPE.LOWPASS, 0, lp_filter_fc, Sampling_Freq);
					break;
				case MODE_CHOICE.VELOCITY_MODE:
					HP_FILTER = new BiquadFilter(1, 1, 6, 2, BiquadFilter.FILTER_TYPE.HIGHPASS, 0, vel_filter_fc, Sampling_Freq);
					LP_FILTER = new BiquadFilter(1, 1, 6, 2, BiquadFilter.FILTER_TYPE.INTEGRATOR, 1, vel_filter_fc, Sampling_Freq);
					break;
				case MODE_CHOICE.DISPLACEMENT_MODE:
					HP_FILTER = new BiquadFilter(1, 1, 6, 2, BiquadFilter.FILTER_TYPE.HIGHPASS, 0, disp_filter_fc, Sampling_Freq);
					LP_FILTER = new BiquadFilter(1, 1, 6, 2, BiquadFilter.FILTER_TYPE.INTEGRATOR, 1, disp_filter_fc, Sampling_Freq);
					LP_FILTER_2 = new BiquadFilter(1, 1, 6, 2, BiquadFilter.FILTER_TYPE.INTEGRATOR, 1, disp_filter_fc, Sampling_Freq);
					break;
				default:
					break;
			}
			DisplayActivity.TitleRMS.Visibility = ViewStates.Visible;
			switch (_mode_Choice)
			{
				case MODE_CHOICE.ACCELERATION_MODE:
					if (!gUnits)
					{
						DisplayActivity.TitleRMS.Text = context.GetString(Resource.String.title_rms_ms2);
					}
					else
					{
						DisplayActivity.TitleRMS.Text = context.GetString(Resource.String.title_rms_g);
					}
					break;
				case MODE_CHOICE.VELOCITY_MODE:
					DisplayActivity.TitleRMS.Text = context.GetString(Resource.String.title_rms_ms);
					break;
				case MODE_CHOICE.DISPLACEMENT_MODE:
					DisplayActivity.TitleRMS.Text = context.GetString(Resource.String.title_rms_m);
					break;
				default:
					break;
			}

			averageDepth = averageDepth_update;
			fft_Window_Size = fft_Window_Size_Update;
			_freq_Choice = _freq_Choice_Update;

			//define the size of the block to be sent
			display_Block_Size = 32;

			//fft_1D instance
			//fft_1D = new FloatFFT_1D(fft_Window_Size);

			fifosem = new Semaphore(0);
			freqsem = new Semaphore(0);
			tempsem = new Semaphore(0);
			tempcopysem = new Semaphore(1);
			freqcopysem = new Semaphore(1);

			//Clear all the buffers
			DisplayActivity.FIFOticksValues.Clear();
			DisplayActivity.FIFOazValues.Clear();
			DisplayActivity.FIFOayValues.Clear();
			DisplayActivity.FIFOaxValues.Clear();
			//Clear buffers after a stop
			DisplayActivity.ticksValues.Clear();
			DisplayActivity.azValues.Clear();
			DisplayActivity.ayValues.Clear();
			DisplayActivity.axValues.Clear();

			DisplayActivity.ticksValuesCopy.Clear();
			DisplayActivity.azValuesCopy.Clear();
			DisplayActivity.ayValuesCopy.Clear();
			DisplayActivity.axValuesCopy.Clear();

			DisplayActivity.fftax.Clear();
			DisplayActivity.fftay.Clear();
			DisplayActivity.fftaz.Clear();
			DisplayActivity.fftFreq.Clear();

			DisplayActivity.fftaxCopy.Clear();
			DisplayActivity.fftayCopy.Clear();
			DisplayActivity.fftazCopy.Clear();
			DisplayActivity.fftFreqCopy.Clear();

			DisplayActivity.RMSazValues.Clear();
			DisplayActivity.RMSayValues.Clear();
			DisplayActivity.RMSaxValues.Clear();

			E_RMS_ax_converted_value = 0;
			E_RMS_ay_converted_value = 0;
			E_RMS_az_converted_value = 0;

			DisplayActivity.dataSeriesAx.Clear();
			DisplayActivity.dataSeriesAy.Clear();
			DisplayActivity.dataSeriesAz.Clear();

			DisplayActivity.dataSeriesFFTAx.Clear();
			DisplayActivity.dataSeriesFFTAy.Clear();
			DisplayActivity.dataSeriesFFTAz.Clear();

			if (acq_thread != null)
				acq_thread.Interrupt();
			if (treat_thread != null)
				treat_thread.Interrupt();
			if (temp_thread != null)
				temp_thread.Interrupt();
			if (freq_thread != null)
				freq_thread.Interrupt();

			Java.Lang.Runnable runnable_recording_time_limit = new Java.Lang.Runnable(() =>
			{
				DisplayActivity.recordButton.CallOnClick();
				int timeLimit = (int)recordingTimeLimit / (60 * 1000);
				Toast.MakeText(context, DisplayActivity.toastString_rtl + " " + timeLimit + " " + DisplayActivity.toastString_rtlm,
						ToastLength.Short).Show();
				rtlRunnableSent = false;
			});
			Java.Lang.Runnable runnable_full_fifo = new Java.Lang.Runnable(() =>
			{

				Toast.MakeText(context, DisplayActivity.toastString_fifo, ToastLength.Short).Show();
				fullFifoRunnableSent = false;
			});

			Java.Lang.Runnable runnable_end_of_file = new Java.Lang.Runnable(() =>
			{
				DisplayActivity.stopButton.SetImageDrawable(DisplayActivity.replayDrawable);
				DisplayActivity.startButton.SetImageDrawable(DisplayActivity.startDrawable);
				DisplayActivity.DisableImButton(DisplayActivity.ffButton);
				DisplayActivity.DisableImButton(DisplayActivity.skipNextButton);
				DisplayActivity.replayFlag = true;
				Toast.MakeText(context, DisplayActivity.toastString_eof, ToastLength.Short).Show();
			});


			//Thread dedicated to the acquisition and conversion of the values
			acq_thread = new Java.Lang.Thread(new Java.Lang.Runnable(() =>
			{
				//Read from file if viewer mode selected
				//Read from USB-hid if streamer mode selected
				switch (_input_choice)
				{
					case INPUT_CHOICE.REAL_VALUES:
						{
							byte[] ticks_buffer = new byte[4];
							byte[] ax_buffer = new byte[2];
							byte[] ay_buffer = new byte[2];
							byte[] az_buffer = new byte[2];
							byte[] count_buffer = new byte[2];
							byte[] count2_buffer = new byte[2];
							byte[] rcvd;
							//total number of message sent on the current hid message (max 64 bytes)
							int total_n_message;
							int total_n_message_fifo;
							//ticks info sent on the current hid message (1 4-bytes ticks info / message)
							long message_ticks;
							Get_Event_Method(0x1F);//EVENT_EXT_START signal sent
							rcvd = ReceiveData();
							while (!_stop_Signal)
							{
								Array.Copy(rcvd, 2, count_buffer, 0, 2); // copy 2 bytes into count_buffer from rcvd pos 2
								total_n_message = ByteBuffer.Wrap(count_buffer).Order(ByteOrder.LittleEndian).Short;// get short from count_buffer
								/*TEST*/
								Array.Copy(rcvd, 0, count2_buffer, 0, 2); // copy 2 bytes into count_buffer from rcvd pos 2
								total_n_message_fifo = ByteBuffer.Wrap(count2_buffer).Order(ByteOrder.LittleEndian).Short;// get short from count_buffer
								if ((total_n_message_fifo >= ACQ_FIFO_DEPTH) && !fullFifoRunnableSent)
								{
									Device.BeginInvokeOnMainThread(() =>
									{
										runnable_full_fifo.Run();
									});
									fullFifoRunnableSent = true;
								}
								if (total_n_message != 0)
								{
									Array.Copy(rcvd, 4, ticks_buffer, 0, 4); // copy 4 bytes into ticks_buffer from rcvd pos 4
									_ticks_value = ByteBuffer.Wrap(ticks_buffer).Order(ByteOrder.LittleEndian).Int;// get int from ticks_buffer
									message_ticks = SignedIntToUnsignedLong(_ticks_value);

									for (int i = 0; i < total_n_message; i++)
									{
										Array.Copy(rcvd, 8 + 6 * i, ax_buffer, 0, 2); // copy 2 bytes into ax_buffer from rcvd pos 8
										_ax_value = ByteBuffer.Wrap(ax_buffer).Order(ByteOrder.LittleEndian).Short;// get short from ax_buffer
										Array.Copy(rcvd, 10 + 6 * i, ay_buffer, 0, 2); // copy 2 bytes into ay_buffer from rcvd pos 10
										_ay_value = ByteBuffer.Wrap(ay_buffer).Order(ByteOrder.LittleEndian).Short;// get short from ay_buffer
										Array.Copy(rcvd, 12 + 6 * i, az_buffer, 0, 2); // copy 2 bytes into az_buffer from rcvd pos 12
										_az_value = ByteBuffer.Wrap(az_buffer).Order(ByteOrder.LittleEndian).Short;// get short from az_buffer
																														// One 4-bytes ticks info / hid message
										_ticks_converted_value = message_ticks + i;

										if (recordTemp)
										{
											byte[] bytesTicks = Encoding.ASCII.GetBytes(ByteBuffer.Allocate(4).Order(ByteOrder.LittleEndian).PutInt((int)_ticks_converted_value).ToString());
											byte bytesEc = 0x00;
											byte[] bytesAx = Encoding.ASCII.GetBytes(ByteBuffer.Allocate(2).Order(ByteOrder.LittleEndian).PutShort((short)_ax_value).ToString());
											byte[] bytesAy = Encoding.ASCII.GetBytes(ByteBuffer.Allocate(2).Order(ByteOrder.LittleEndian).PutShort((short)_ay_value).ToString());
											byte[] bytesAz = Encoding.ASCII.GetBytes(ByteBuffer.Allocate(2).Order(ByteOrder.LittleEndian).PutShort((short)_az_value).ToString());
											try
											{
												DisplayActivity.fosout.Write(bytesTicks);
												DisplayActivity.fosout.Write(bytesEc);
												DisplayActivity.fosout.Write(bytesAx);
												DisplayActivity.fosout.Write(bytesAy);
												DisplayActivity.fosout.Write(bytesAz);
											}
											catch (IOException e)
											{
												Log.Error("ERROR", "", e.Message);
											}
										}

										//Converts the acceleration values according to the sensitivity chosen
										if (_2g_sensitivity)
										{
											_ax_converted_value = ((float)_ax_value + _ax_offset_2g) * _ax_gain_2g;
											_ay_converted_value = ((float)_ay_value + _ay_offset_2g) * _ay_gain_2g;
											_az_converted_value = ((float)_az_value + _az_offset_2g) * _az_gain_2g;
										}
										else
										{
											_ax_converted_value = ((float)_ax_value + _ax_offset_6g) * _ax_gain_6g;
											_ay_converted_value = ((float)_ay_value + _ay_offset_6g) * _ay_gain_6g;
											_az_converted_value = ((float)_az_value + _az_offset_6g) * _az_gain_6g;
										}

										lock (fifo_lock)
										{
											DisplayActivity.FIFOaxValues.Add(_ax_converted_value);
											DisplayActivity.FIFOayValues.Add(_ay_converted_value);
											DisplayActivity.FIFOazValues.Add(_az_converted_value);
											DisplayActivity.FIFOticksValues.Add(_ticks_converted_value);
										}
										fifosem.Release();
									}
									if (recordTemp)
									{
										Date now = new Date();
										long duration = now.Time - DisplayActivity.DT_start.Time; //Time duration in ms
										if (duration >= recordingTimeLimit && !rtlRunnableSent)
										{
											recordTemp = false;
											Device.BeginInvokeOnMainThread(() =>
											{
												runnable_recording_time_limit.Run();
											});
											rtlRunnableSent = true;
										}
									}
									if (closeFOS)
									{
										closeFOS = false;
										try
										{
											DisplayActivity.fosout.Close();
										}
										catch (IOException e)
										{
											e.PrintStackTrace();
										}
									}
								}
								else
								{
								}
								Ext_Event_Go_On(); //EVENT_EXT_GO_ON signal sent
								rcvd = ReceiveData();
							}
						}
						break;
					case INPUT_CHOICE.RECORDED_VALUES:
						{
							byte[] ticks_buf = new byte[4];
							byte[] ax_buf = new byte[2];
							byte[] ay_buf = new byte[2];
							byte[] az_buf = new byte[2];
							final_tick = -1.0f;
							long n_meas_in_file = DisplayActivity.bin_file.Length() / 11;
							int val_Sent = 0;
							int readblock_size = 20;
							startTimeMillis = new DateTimeOffset(DateTime.Now.ToUniversalTime()).ToUnixTimeMilliseconds();
							counterMillis = 0;
							while (Read1Line(DisplayActivity.istToRead, ticks_buf, ax_buf, ay_buf, az_buf) != -1)
							{
								if (_stop_Signal)
								{
									break;
								}
								if (_pause_Signal)
								{
									//Allow zooming when pause
									DisplayActivity.xAxisT.AutoRange = AutoRange.Never;
									DisplayActivity.yAxisT.AutoRange = AutoRange.Never;
									DisplayActivity.xAxisF.AutoRange = AutoRange.Never;
									DisplayActivity.yAxisF.AutoRange = AutoRange.Never;
									while (true)
									{
										if (!_pause_Signal)
										{
											startTimeMillis = new DateTimeOffset(DateTime.Now.ToUniversalTime()).ToUnixTimeMilliseconds();
											counterMillis = 0;
											//if scaling was enabled before pause button was pressed - restore it !
											if (yAutoScale)
											{
												DisplayActivity.yAxisT.AutoRange = AutoRange.Always;
												DisplayActivity.yAxisF.AutoRange = AutoRange.Always;
											}
											break;
										}
										if (_stop_Signal)
										{
											if (yAutoScale)
											{
												DisplayActivity.yAxisT.AutoRange = AutoRange.Always;
												DisplayActivity.yAxisF.AutoRange = AutoRange.Always;
											}
											_pause_Signal = false;
											break;
										}
									}
								}
								/*  Skip Next management    */
								if (skipNext)
								{
									clearAllBuffersFlag = true;
									SkipxLines(DisplayActivity.istToRead, skipNextValue * Sampling_Freq * 11);
									skipNext = false;
									if (Read1Line(DisplayActivity.istToRead, ticks_buf, ax_buf, ay_buf, az_buf) == -1)
									{
										break;
									}
								}
								//Interpret bytes as integers
								_ticks_value = ByteBuffer.Wrap(ticks_buf).Order(ByteOrder.LittleEndian).Int;// get int from ticks_buf
								_ax_value = ByteBuffer.Wrap(ax_buf).Order(ByteOrder.LittleEndian).Short;// get short from ax_buf
								_ay_value = ByteBuffer.Wrap(ay_buf).Order(ByteOrder.LittleEndian).Short;// get short from ay_buf
								_az_value = ByteBuffer.Wrap(az_buf).Order(ByteOrder.LittleEndian).Short;// get short from az_buf
																											 //Convert values
								_ticks_converted_value = SignedIntToUnsignedLong(_ticks_value);
								if (final_tick < 0)
								{
									final_tick = (_ticks_converted_value + n_meas_in_file - 1) / (float)Sampling_Freq;
								}
								//Converts the acceleration values according to the sensitivity chosen
								if (_2g_sensitivity)
								{
									_ax_converted_value = (_ax_value + _ax_offset_2g) * _ax_gain_2g;
									_ay_converted_value = (_ay_value + _ay_offset_2g) * _ay_gain_2g;
									_az_converted_value = (_az_value + _az_offset_2g) * _az_gain_2g;
								}
								else
								{
									_ax_converted_value = (_ax_value + _ax_offset_6g) * _ax_gain_6g;
									_ay_converted_value = (_ay_value + _ay_offset_6g) * _ay_gain_6g;
									_az_converted_value = (_az_value + _az_offset_6g) * _az_gain_6g;
								}
								lock (fifo_lock)
								{
									DisplayActivity.FIFOaxValues.Add(_ax_converted_value);
									DisplayActivity.FIFOayValues.Add(_ay_converted_value);
									DisplayActivity.FIFOazValues.Add(_az_converted_value);
									DisplayActivity.FIFOticksValues.Add(_ticks_converted_value);
								}
								fifosem.Release();
								val_Sent++;
								//Depending on the reading speed we want, wait during x secondes between each values
								if (val_Sent == readblock_size)
								{
									val_Sent = 0;
									VariableTempo(readblock_size);
								}
								//Log.i(TAG, "20 values block going to "+_ticks_converted_value);
							}
							try
							{
								DisplayActivity.istToRead.Close();
							}
							catch (IOException e)
							{
								Log.Error(TAG, "IOException" + e);
							}
							if (!_stop_Signal)
							{
								Device.BeginInvokeOnMainThread(() =>
								{
									runnable_end_of_file.Run();
								});
							}
						}
						break;

				}

			}));

			Java.Lang.Runnable runnable_rms = new Java.Lang.Runnable(() =>
			{
				DecimalFormat df = new DecimalFormat("0.#####E0");
				df.MinimumFractionDigits = 5;
				DisplayActivity.ERMSx.Text = "x = " + df.Format(E_RMS_ax_converted_value);
				DisplayActivity.ERMSy.Text = "y = " + df.Format(E_RMS_ay_converted_value);
				DisplayActivity.ERMSz.Text = "z = " + df.Format(E_RMS_az_converted_value);
			});

			treat_thread = new Java.Lang.Thread(new Java.Lang.Runnable(() =>
			{
				float filtered_val_x = 0;
				float filtered_val_y = 0;
				float filtered_val_z = 0;
				float val_tick = 0;
				while (!_stop_Signal)
				{
					try
					{
						fifosem.Acquire(1);

						switch (_mode_Choice)
						{
							case MODE_CHOICE.ACCELERATION_MODE:
								switch (_filter_Choice)
								{
									case FILTER_CHOICE.NO_FILTER:
										lock (fifo_lock)
										{
											filtered_val_x = DisplayActivity.FIFOaxValues.Get(0);
											filtered_val_y = DisplayActivity.FIFOayValues.Get(0);
											filtered_val_z = DisplayActivity.FIFOazValues.Get(0);
										}
										break;
									case FILTER_CHOICE.HP:
										//HP
										lock (fifo_lock)
										{
											filtered_val_x = (float)(HP_FILTER.biquad_filter(DisplayActivity.FIFOaxValues.Get(0), 0) * HP_FILTER.getIir_biquads_g());
											filtered_val_y = (float)(HP_FILTER.biquad_filter(DisplayActivity.FIFOayValues.Get(0), 1) * HP_FILTER.getIir_biquads_g());
											filtered_val_z = (float)(HP_FILTER.biquad_filter(DisplayActivity.FIFOazValues.Get(0), 2) * HP_FILTER.getIir_biquads_g());
										}
										break;
									case FILTER_CHOICE.LP:
										//LP
										lock (fifo_lock)
										{
											filtered_val_x = (float)(LP_FILTER.biquad_filter(DisplayActivity.FIFOaxValues.Get(0), 0) * LP_FILTER.getIir_biquads_g());
											filtered_val_y = (float)(LP_FILTER.biquad_filter(DisplayActivity.FIFOayValues.Get(0), 1) * LP_FILTER.getIir_biquads_g());
											filtered_val_z = (float)(LP_FILTER.biquad_filter(DisplayActivity.FIFOazValues.Get(0), 2) * LP_FILTER.getIir_biquads_g());
										}
										break;
									case FILTER_CHOICE.HP_LP:
										//HP
										lock (fifo_lock)
										{
											filtered_val_x = (float)(HP_FILTER.biquad_filter(DisplayActivity.FIFOaxValues.Get(0), 0) * HP_FILTER.getIir_biquads_g());
											filtered_val_y = (float)(HP_FILTER.biquad_filter(DisplayActivity.FIFOayValues.Get(0), 1) * HP_FILTER.getIir_biquads_g());
											filtered_val_z = (float)(HP_FILTER.biquad_filter(DisplayActivity.FIFOazValues.Get(0), 2) * HP_FILTER.getIir_biquads_g());
										}
										//LP
										filtered_val_x = (float)(LP_FILTER.biquad_filter(filtered_val_x, 0) * LP_FILTER.getIir_biquads_g());
										filtered_val_y = (float)(LP_FILTER.biquad_filter(filtered_val_y, 1) * LP_FILTER.getIir_biquads_g());
										filtered_val_z = (float)(LP_FILTER.biquad_filter(filtered_val_z, 2) * LP_FILTER.getIir_biquads_g());
										break;
									default:
										break;
								}
								//Convert it depending on the units chosen
								if (gUnits)
								{
									filtered_val_x /= GRAVITY;
									filtered_val_y /= GRAVITY;
									filtered_val_z /= GRAVITY;
								}
								break;
							case MODE_CHOICE.VELOCITY_MODE:
								//HP
								filtered_val_x = (float)(HP_FILTER.biquad_filter(DisplayActivity.FIFOaxValues.Get(0), 0) * HP_FILTER.getIir_biquads_g());
								filtered_val_y = (float)(HP_FILTER.biquad_filter(DisplayActivity.FIFOayValues.Get(0), 1) * HP_FILTER.getIir_biquads_g());
								filtered_val_z = (float)(HP_FILTER.biquad_filter(DisplayActivity.FIFOazValues.Get(0), 2) * HP_FILTER.getIir_biquads_g());
								//LP Integrator
								filtered_val_x = (float)(LP_FILTER.biquad_filter(filtered_val_x, 0) * LP_FILTER.getIir_biquads_g());
								filtered_val_y = (float)(LP_FILTER.biquad_filter(filtered_val_y, 1) * LP_FILTER.getIir_biquads_g());
								filtered_val_z = (float)(LP_FILTER.biquad_filter(filtered_val_z, 2) * LP_FILTER.getIir_biquads_g());

								//[mm/s]
								filtered_val_x *= 1000;
								filtered_val_y *= 1000;
								filtered_val_z *= 1000;
								break;
							case MODE_CHOICE.DISPLACEMENT_MODE:
								//HP
								filtered_val_x = (float)(HP_FILTER.biquad_filter(DisplayActivity.FIFOaxValues.Get(0), 0) * HP_FILTER.getIir_biquads_g());
								filtered_val_y = (float)(HP_FILTER.biquad_filter(DisplayActivity.FIFOayValues.Get(0), 1) * HP_FILTER.getIir_biquads_g());
								filtered_val_z = (float)(HP_FILTER.biquad_filter(DisplayActivity.FIFOazValues.Get(0), 2) * HP_FILTER.getIir_biquads_g());
								//LP Integrator 1
								filtered_val_x = (float)(LP_FILTER.biquad_filter(filtered_val_x, 0) * LP_FILTER.getIir_biquads_g());
								filtered_val_y = (float)(LP_FILTER.biquad_filter(filtered_val_y, 1) * LP_FILTER.getIir_biquads_g());
								filtered_val_z = (float)(LP_FILTER.biquad_filter(filtered_val_z, 2) * LP_FILTER.getIir_biquads_g());

								//LP Integrator 2
								filtered_val_x = (float)(LP_FILTER_2.biquad_filter(filtered_val_x, 0) * LP_FILTER_2.getIir_biquads_g());
								filtered_val_y = (float)(LP_FILTER_2.biquad_filter(filtered_val_y, 1) * LP_FILTER_2.getIir_biquads_g());
								filtered_val_z = (float)(LP_FILTER_2.biquad_filter(filtered_val_z, 2) * LP_FILTER_2.getIir_biquads_g());

								//[mm]
								filtered_val_x *= 1000;
								filtered_val_y *= 1000;
								filtered_val_z *= 1000;
								break;
							default:
								break;
						}
						lock (fifo_lock)

						{
							val_tick = (float)(DisplayActivity.FIFOticksValues.Get(0) / (float)Sampling_Freq);
							DisplayActivity.FIFOticksValues.Remove(0);
							DisplayActivity.FIFOaxValues.Remove(0);
							DisplayActivity.FIFOayValues.Remove(0);
							DisplayActivity.FIFOazValues.Remove(0);
						}
						lock (list_lock)

						{
							DisplayActivity.RMSaxValues.Add(filtered_val_x);
							DisplayActivity.RMSayValues.Add(filtered_val_y);
							DisplayActivity.RMSazValues.Add(filtered_val_z);

							DisplayActivity.axValues.Add(filtered_val_x);
							DisplayActivity.ayValues.Add(filtered_val_y);
							DisplayActivity.azValues.Add(filtered_val_z);

							DisplayActivity.ticksValues.Add(val_tick);
							tempsem.Release();

							DisplayActivity.faxValues.Add(filtered_val_x);
							DisplayActivity.fayValues.Add(filtered_val_y);
							DisplayActivity.fazValues.Add(filtered_val_z);

							freqsem.Release();
						}
						int size = DisplayActivity.RMSaxValues.Size();
						if (size >= rms_block_size)
						{
							for (int k = 0; k < rms_block_size; k++)
							{
								E_RMS_ax_converted_value = (float)Math.Sqrt(rms_m * ((DisplayActivity.RMSaxValues.Get(k) * DisplayActivity.RMSaxValues.Get(k)) - (E_RMS_ax_converted_value * E_RMS_ax_converted_value)) + (E_RMS_ax_converted_value * E_RMS_ax_converted_value));
								E_RMS_ay_converted_value = (float)Math.Sqrt(rms_m * ((DisplayActivity.RMSayValues.Get(k) * DisplayActivity.RMSayValues.Get(k)) - (E_RMS_ay_converted_value * E_RMS_ay_converted_value)) + (E_RMS_ay_converted_value * E_RMS_ay_converted_value));
								E_RMS_az_converted_value = (float)Math.Sqrt(rms_m * ((DisplayActivity.RMSazValues.Get(k) * DisplayActivity.RMSazValues.Get(k)) - (E_RMS_az_converted_value * E_RMS_az_converted_value)) + (E_RMS_az_converted_value * E_RMS_az_converted_value));
							}
							lock (list_lock)
							{
								for (int k = 0; k < rms_block_size; k++)
								{
									DisplayActivity.RMSaxValues.Remove(0);
									DisplayActivity.RMSayValues.Remove(0);
									DisplayActivity.RMSazValues.Remove(0);
								}
							}
							Device.BeginInvokeOnMainThread(() =>
							{
								runnable_rms.Run();
							});
						}
					}
					catch (Java.Lang.InterruptedException e) 
					{ 
						e.PrintStackTrace(); 
						break; 
					}
				}//not stopped loop
			}));

			Java.Lang.Runnable runnable_temp = new Java.Lang.Runnable(() =>
			{
				DisplayActivity.xAxisT.VisibleRange = new DoubleRange(minRange, maxRange);

				DisplayActivity.dataSeriesAx.Append(DisplayActivity.ticksValuesCopy, DisplayActivity.axValuesCopy);
				DisplayActivity.dataSeriesAy.Append(DisplayActivity.ticksValuesCopy, DisplayActivity.ayValuesCopy);
				DisplayActivity.dataSeriesAz.Append(DisplayActivity.ticksValuesCopy, DisplayActivity.azValuesCopy);

				DisplayActivity.ticksValuesCopy.Clear();
				DisplayActivity.axValuesCopy.Clear();
				DisplayActivity.ayValuesCopy.Clear();
				DisplayActivity.azValuesCopy.Clear();

				tempcopysem.Release();
			});

			temp_thread = new Java.Lang.Thread(new Java.Lang.Runnable(() =>
			{
				while (true)
				{
					try
					{
						int display_block_size_local = display_Block_Size;
						//Wait till copy buffers cleared
						tempcopysem.Acquire(1);
						//Wait till a new value is added to tempsem
						tempsem.Acquire(display_block_size_local);

						if (DisplayActivity.ticksValues.Size() > 0)
						{
							//So that all points are plotted
							//if final_tick is in tempticks 
							if (DisplayActivity.ticksValues.Get(DisplayActivity.ticksValues.Size() - 1) == final_tick)
							{
								display_block_size_local = (int)DisplayActivity.ticksValues.Size();
								Log.Debug(TAG, "final tick detected");
							}
							if (DisplayActivity.ticksValues.Get(display_block_size_local - 1) * Sampling_Freq - nPointsToShow < 0)
							{
								minRange = (double)0;
								maxRange = (double)0 + (double)nPointsToShow / Sampling_Freq + 0.2 * nPointsToShow / Sampling_Freq;
							}
							else
							{
								minRange = DisplayActivity.ticksValues.Get(display_block_size_local - 1) - (double)nPointsToShow / Sampling_Freq;
								maxRange = (double)DisplayActivity.ticksValues.Get(display_block_size_local - 1) + 0.2 * nPointsToShow / Sampling_Freq;
							}

							//Copy the (display_Block_Size) first values from buffers into copy ones
							//*buffers can contain more than (display_Block_Size) values
							//*buffers_copy can only contain (display_Block_Size) values.
							lock (list_lock)
							{
								for (int j = 0; j < display_block_size_local; j++)
								{
									DisplayActivity.ticksValuesCopy.Add(DisplayActivity.ticksValues.Get(j));
									DisplayActivity.axValuesCopy.Add(DisplayActivity.axValues.Get(j));
									DisplayActivity.ayValuesCopy.Add(DisplayActivity.ayValues.Get(j));
									DisplayActivity.azValuesCopy.Add(DisplayActivity.azValues.Get(j));
								}
								//Delete the (display_Block_Size) first values from buffers
								for (int j = 0; j < display_block_size_local; j++)
								{
									DisplayActivity.ticksValues.Remove(0);
									DisplayActivity.axValues.Remove(0);
									DisplayActivity.ayValues.Remove(0);
									DisplayActivity.azValues.Remove(0);
								}
							}
							//display_data()
							Device.BeginInvokeOnMainThread(() =>
							{
								runnable_temp.Run();
							});
						}
						else
						{
							Log.Debug(TAG, "error detected");
						}

					}
					catch (Java.Lang.InterruptedException e)
					{ 
						e.PrintStackTrace();
						break; 
					}
				}
			}));

			#endregion Done

			Java.Lang.Runnable runnable_fft = new Java.Lang.Runnable(() =>
			{
				DoubleRange xRange = new DoubleRange(0.0, Sampling_Freq / 2.0d);
				DisplayActivity.xAxisF.VisibleRangeLimit = xRange;

				DisplayActivity.dataSeriesFFTAx.Clear();
				DisplayActivity.dataSeriesFFTAy.Clear();
				DisplayActivity.dataSeriesFFTAz.Clear();

				DisplayActivity.dataSeriesFFTAx.Append(DisplayActivity.fftFreqCopy, DisplayActivity.fftaxCopy);
				DisplayActivity.dataSeriesFFTAy.Append(DisplayActivity.fftFreqCopy, DisplayActivity.fftayCopy);
				DisplayActivity.dataSeriesFFTAz.Append(DisplayActivity.fftFreqCopy, DisplayActivity.fftazCopy);

				DisplayActivity.fftFreqCopy.Clear();
				DisplayActivity.fftaxCopy.Clear();
				DisplayActivity.fftayCopy.Clear();
				DisplayActivity.fftazCopy.Clear();

				freqcopysem.Release();
			});

			freq_thread = new Java.Lang.Thread(new Java.Lang.Runnable(() =>
			{
				while (true)
				{
					try
					{
						if ((fft_Window_Size != fft_Window_Size_Update) || (averageDepth != averageDepth_update) || (_freq_Choice != _freq_Choice_Update))
						{
							fft_Window_Size = fft_Window_Size_Update;
							averageDepth = averageDepth_update;
							_freq_Choice = _freq_Choice_Update;
							//fft_1D instance
							//fft_1D = new FloatFFT_1D(fft_Window_Size);      ***********On hold

							DisplayActivity.fftax.Clear();
							DisplayActivity.fftay.Clear();
							DisplayActivity.fftaz.Clear();
							DisplayActivity.fftFreq.Clear();

							DisplayActivity.fftaxCopy.Clear();
							DisplayActivity.fftayCopy.Clear();
							DisplayActivity.fftazCopy.Clear();
							DisplayActivity.fftFreqCopy.Clear();

							freqsem = new Semaphore(0);
						}

						//Wait till copy buffers cleared
						freqcopysem.Acquire(1);
						//Wait till a new value is added to tempsem
						freqsem.Acquire(fft_Window_Size);

						switch (_window_Choice)
						{
							case WINDOW_CHOICE.HANNING_WINDOW:
								lock (list_lock)
								{
									Hann_window(DisplayActivity.faxValues, DisplayActivity.fayValues, DisplayActivity.fazValues, fft_Window_Size);
								}
								break;
							case WINDOW_CHOICE.FLAT_TOP_WINDOW:
								lock (list_lock)
								{
									Flat_top_window(DisplayActivity.faxValues, DisplayActivity.fayValues, DisplayActivity.fazValues, fft_Window_Size);
								}
								break;
							default:
								break;
						}
						#region Comment
						/*//Retrieve the (fft_Window_Size) first values of fa*Values buffers and prepare a FloatLargeArray object to send in realForward()
						fftBufferAx = new FloatLargeArray(Arrays.CopyOfRange(DisplayActivity.faxValues.GetItemsArray(), 0, fft_Window_Size));
						fftBufferAy = new FloatLargeArray(Arrays.CopyOfRange(DisplayActivity.fayValues.GetItemsArray(), 0, fft_Window_Size));
						fftBufferAz = new FloatLargeArray(Arrays.CopyOfRange(DisplayActivity.fazValues.GetItemsArray(), 0, fft_Window_Size));

						//Fft coefficients in buffers
						fft_1D.Initialize(fftBufferAx);
						fft_1D.Initialize(fftBufferAy);
						fft_1D.Initialize(fftBufferAz);

						//Prepare the set of magnitudes based on the coeff returned in buffer by realForward()
						DisplayActivity.SetFftVal(fft_Window_Size, window_npbw, fftBufferAx, DisplayActivity.fftax, _freq_Choice);
						DisplayActivity.SetFftVal(fft_Window_Size, window_npbw, fftBufferAy, DisplayActivity.fftay, _freq_Choice);
						DisplayActivity.SetFftVal(fft_Window_Size, window_npbw, fftBufferAz, DisplayActivity.fftaz, _freq_Choice);
						//Prepare the set of harmonic frequencies
						DisplayActivity.SetFftFreq(fft_Window_Size, Sampling_Freq, DisplayActivity.fftFreqCopy);
						//Averaging FftValues according to the (averageDepth) chosen and put the result in ffta*Copy buffers
						DisplayActivity.AverageFftValues(DisplayActivity.fftax, DisplayActivity.fftaxCopy, fft_Window_Size, averageDepth);
						DisplayActivity.AverageFftValues(DisplayActivity.fftay, DisplayActivity.fftayCopy, fft_Window_Size, averageDepth);
						DisplayActivity.AverageFftValues(DisplayActivity.fftaz, DisplayActivity.fftazCopy, fft_Window_Size, averageDepth);*/
						#endregion Comment
						lock (list_lock)
						{
							for (int j = 0; j < fft_Window_Size; j++)
							{
								try
								{
									DisplayActivity.faxValues.Remove(0);
									DisplayActivity.fayValues.Remove(0);
									DisplayActivity.fazValues.Remove(0);
								}
								catch (Java.Lang.ArrayIndexOutOfBoundsException e)
								{
									Log.Error(TAG, "ArrayIndexOutOfBoundsException" + e);
								}
							}
						}

						Device.BeginInvokeOnMainThread(() =>
						{
							runnable_fft.Run();
						});
					}
					catch (Java.Lang.InterruptedException e)
					{
						e.PrintStackTrace();
						break;
					}
				}
			}));
			acq_thread.Start();
			treat_thread.Start();
			temp_thread.Start();
			freq_thread.Start();
		}

		private void Hann_window(FloatValues inputx, FloatValues inputy, FloatValues inputz, int N)
		{
			for (int i = 0; i < N; i++)
			{
				float cos = (float)(Math.Cos(2.0f * Math.PI * (float)i / (float)(N - 1.0f)));
				inputx.Set(i, inputx.Get(i) * (1.0f - cos));
				inputy.Set(i, inputy.Get(i) * (1.0f - cos));
				inputz.Set(i, inputz.Get(i) * (1.0f - cos));
			}
		}
		private void Flat_top_window(FloatValues inputx, FloatValues inputy, FloatValues inputz, int N)
		{
			float a0 = 1f;
			float a1 = 1.93f;
			float a2 = 1.29f;
			float a3 = 0.388f;
			float a4 = 0.028f;
			for (int i = 0; i < N; i++)
			{
				float cos1 = (float)Math.Cos(2.0f * Math.PI * (float)i / (float)(N - 1.0f));
				float cos2 = (float)Math.Cos(4.0f * Math.PI * (float)i / (float)(N - 1.0f));
				float cos3 = (float)Math.Cos(6.0f * Math.PI * (float)i / (float)(N - 1.0f));
				float cos4 = (float)Math.Cos(8.0f * Math.PI * (float)i / (float)(N - 1.0f));
				inputx.Set(i, inputx.Get(i) * (a0 - a1 * cos1 + a2 * cos2 - a3 * cos3 + a4 * cos4));
				inputy.Set(i, inputy.Get(i) * (a0 - a1 * cos1 + a2 * cos2 - a3 * cos3 + a4 * cos4));
				inputz.Set(i, inputz.Get(i) * (a0 - a1 * cos1 + a2 * cos2 - a3 * cos3 + a4 * cos4));
			}
		}

		public void SetSensitivity(byte sensitivity)
		{
			byte[] data = new byte[1];
			data[0] = sensitivity;
			Set_Method(Func.EVENT_EXT_SET_PARAM, ParamId.SENSITIVITY_ID, ParamType.INT8, data);

			byte[] bufferRcvd;
			bufferRcvd = ReceiveData();
			bufferRcvd = null;
		}

		//To set params in the RecoVIB Feel flash
		public void Set_Method(byte Event, byte idFunction, byte typeFunction, byte[] bufferSet)
		{
			List<byte> list = new List<byte>();
			outputbuff.Clear();
			outputbuff.Add(Event);
			outputbuff.Add(idFunction);
			outputbuff.Add(typeFunction);
			foreach (byte b in bufferSet)
			{
				list.Add(b);
			}
			foreach (byte lst in list)
			{
				outputbuff.Add(lst);
			}
			SendData(outputbuff);

		}
		#endregion Methods
	}
}