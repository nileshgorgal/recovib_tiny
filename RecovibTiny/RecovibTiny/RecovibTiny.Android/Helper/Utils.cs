﻿namespace RecovibTiny.Droid.Helper
{
	public class Utils
	{
		public enum INPUT_CHOICE
		{
			REAL_VALUES, RECORDED_VALUES
		}

		public INPUT_CHOICE _input_choice;
		public void Set_input_Choice(INPUT_CHOICE value) { _input_choice = value; }
		public INPUT_CHOICE Get_input_Choice() { return _input_choice; }
	}


}