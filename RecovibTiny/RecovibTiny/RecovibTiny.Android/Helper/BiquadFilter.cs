﻿using System;

namespace RecovibTiny.Droid.Helper
{
	public class BiquadFilter
	{
		public enum FILTER_TYPE
		{
			INTEGRATOR,
			LOWPASS,
			HIGHPASS
		}
		private int BIQUAD_NZ;
		private int BIQUAD_NCOEFF;
		private int BIQUAD_SIZE;
		private double iir_biquads_g;
		private double[][] z_x;
		private double[][] z_y;
		private double[][] z_z;
		private double[][] coeffs;

		private double[] sCoef;
		private double[] zCoef;

		public BiquadFilter(float biquads_g, int biquad_size, int biquad_ncoeff, int biquad_nz, FILTER_TYPE type, double K, double fc, double fs)
		{
			BIQUAD_SIZE = biquad_size;
			BIQUAD_NCOEFF = biquad_ncoeff;
			BIQUAD_NZ = biquad_nz;
			iir_biquads_g = biquads_g;
			sCoef = new double[biquad_ncoeff];
			zCoef = new double[biquad_ncoeff];
			sCoef[0] = 1.0;
			//Fill sCoef and zCoef
			switch (type)
			{
				case FILTER_TYPE.INTEGRATOR:
					Integrator(K, fc);
					Tustin1st(sCoef, fs);
					break;
				case FILTER_TYPE.LOWPASS:
					LP2nd(0.707, fc);
					Tustin2nd(sCoef, fs);
					break;
				case FILTER_TYPE.HIGHPASS:
					HP2nd(0.707, fc);
					Tustin2nd(sCoef, fs);
					break;
			}
			/*this.coeffs = new double[5][];*//* {{zCoef[0],     // B0 0
                zCoef[1],     // B1 1
                zCoef[2],     // B2  2
                zCoef[3],     // A0  3
                zCoef[4],     // A1  4
                zCoef[5]}};*/
			coeffs = new double[][] { new double[6] { zCoef[0], zCoef[1], zCoef[2], zCoef[3], zCoef[4], zCoef[5] } };
			z_x = new double[BIQUAD_SIZE][];
			z_y = new double[BIQUAD_SIZE][];
			z_z = new double[BIQUAD_SIZE][];
			//biquad_clear();
		}
		public void biquad_clear()
		{
			int i, j;
			for (i = 0; i < BIQUAD_SIZE; i++)
			{
				/*for (j = 0; j < BIQUAD_NZ; j++)
				{*/
				z_x[i][0] = (double)0.0;
				z_y[i][0] = (double)0.0;
				z_z[i][0] = (double)0.0;
				/* z_x[i][j] = (double)0.0;
				 z_y[i][j] = (double)0.0;
				 z_z[i][j] = (double)0.0;*/
				//}
			}
		}
		public double biquad_filter(double x, int xyz)
		{
			int i;
			switch (xyz)
			{
				//x
				case 0:
					for (i = 0; i < this.BIQUAD_SIZE; i++)
					{
						x = filter(x, this.coeffs[i], this.z_x[i]);
					}
					break;
				//y
				case 1:
					for (i = 0; i < this.BIQUAD_SIZE; i++)
					{
						x = filter(x, this.coeffs[i], this.z_y[i]);
					}
					break;
				//z
				case 2:
					for (i = 0; i < this.BIQUAD_SIZE; i++)
					{
						x = filter(x, this.coeffs[i], this.z_z[i]);
					}
					break;
			}
			return x;
		}
		/*  Digital Biquad Filter Direct Form II :
        *
        * y[n] = b0 w[n] + b1 w[n-1] + b2 w[n-2]
        * w[n] = x[n] - a1 w[n-1] - a2 w[n-2]
        *
        * */
		private double filter(double x, double[] coeffs, double[] z)
		{
			double w, y;

			w = (float)(-z[0] * coeffs[5]);
			y = (float)(z[0] * coeffs[2]);
			w -= z[1] * coeffs[4];
			w += x;
			y += z[1] * coeffs[1];
			y += w * coeffs[0];
			z[0] = z[1];
			z[1] = w;

			return y;
		}
		public double getIir_biquads_g()
		{
			return this.iir_biquads_g;
		}
		/* TODO check if a second pole is not generated */
		/// <summary>
		/// Integrator
		///
		///     K
		/// ---------
		/// Omega + s
		///
		/// </summary>
		/// <param name="sCoef">Time Domain Coefficients</param>
		/// <param name="K"></param>
		/// <param name="f"></param>
		private void Integrator(double K, double f)
		{
			double Omega = 2 * Math.PI * f;
			this.sCoef[0] = 0.0;
			this.sCoef[1] = 0.0;
			this.sCoef[2] = K;
			this.sCoef[3] = 0.0;
			this.sCoef[4] = 1.0;
			this.sCoef[5] = Omega;
		}
		/// <summary>
		/// 2nd Order Low Pass Filter
		///
		///           Omega²
		/// ------------------------
		/// s² + 2*Xi*Omega + Omega²
		///
		/// </summary>
		/// <param name="sCoef">Time Domain Coefficients</param>
		/// <param name="Xi"></param>
		/// <param name="f"></param>
		private void LP2nd(double Xi, double f)
		{
			double Omega = 2.0 * Math.PI * f;
			this.sCoef[0] = 0.0;
			this.sCoef[1] = 0.0;
			this.sCoef[2] = Omega * Omega;
			this.sCoef[3] = 1.0;
			this.sCoef[4] = 2.0 * Xi * Omega;
			this.sCoef[5] = Omega * Omega;
		}
		/// <summary>
		/// 2nd Order High Pass Filter
		///
		///           s²
		/// ------------------------
		/// s² + 2*Xi*Omega + Omega²
		///
		/// </summary>
		/// <param name="sCoef">Time Domain Coefficients</param>
		/// <param name="Xi"></param>
		/// <param name="f"></param>
		private void HP2nd(double Xi, double f)
		{
			double Omega = 2.0 * Math.PI * f;
			this.sCoef[0] = 1.0;
			this.sCoef[1] = 0.0;
			this.sCoef[2] = 0.0;
			this.sCoef[3] = 1.0;
			this.sCoef[4] = 2.0 * Xi * Omega;
			this.sCoef[5] = Omega * Omega;
		}
		/// <summary>
		/// Bilinear Transform with Normalization for 1st order
		/// transfer functions implemented as biquad
		/// </summary>
		/// <param name="s">Time Domain Coefficients - b0s, b1s, b2s, a0s, a1s, a2s</param>
		/// <param name="z">reference to Z Domain Coefficients - b0z, b1z, b2z, a0z, a1z, a2z</param>
		/// <param name="fs">sampling frequency</param>
		private void Tustin1st(double[] s, double fs)
		{
			double K = 2.0 * fs;
			double d = s[4] * K + s[5];

			this.zCoef[0] = (s[1] * K + s[2]) / d;   // b0z
			this.zCoef[1] = (-s[1] * K + s[2]) / d;  // b1z
			this.zCoef[2] = 0.0;                     // b2z - 0.0 for 1st order transfer functions
			this.zCoef[3] = 1.0;                     // a0z - not used in normalized biquad
			this.zCoef[4] = (-s[4] * K + s[5]) / d;  // a1z
			this.zCoef[5] = 0.0;                     // a2z - 0.0 for 1st order transfer functions
		}

		/// <summary>
		/// Bilinear Transform with Normalization for 2nd order
		/// transfer functions implemented as biquad
		/// </summary>
		/// <param name="s">Time Domain Coefficients - b0s, b1s, b2s, a0s, a1s, a2s</param>
		/// <param name="z">reference to Z Domain Coefficients - b0z, b1z, b2z, a0z, a1z, a2z</param>
		/// <param name="fs">sampling frequency</param>
		private void Tustin2nd(double[] s, double fs)
		{
			double K = 2.0 * fs;
			double K2 = K * K;
			double d = s[3] * K2 + s[4] * K + s[5];

			this.zCoef[0] = (s[0] * K2 + s[1] * K + s[2]) / d;   // b0z
			this.zCoef[1] = (2.0 * s[2] - 2.0 * s[0] * K2) / d;  // b1z
			this.zCoef[2] = (s[0] * K2 - s[1] * K + s[2]) / d;   // b2z
			this.zCoef[3] = 1.0;                                 // a0z - not used in normalized biquad
			this.zCoef[4] = (2.0 * s[5] - 2.0 * s[3] * K2) / d;  // a1z
			this.zCoef[5] = (s[3] * K2 - s[4] * K + s[5]) / d;   // a2z
		}
	}
}