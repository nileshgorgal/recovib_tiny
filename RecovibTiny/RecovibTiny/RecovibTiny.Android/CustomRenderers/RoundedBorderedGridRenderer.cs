﻿using Android.Content;
using Android.Graphics.Drawables;
using RecovibTiny.CustomControl;
using RecovibTiny.Droid.CustomRenderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
[assembly: ExportRenderer(typeof(RoundedBorderedGrid), typeof(RoundedBorderedGridRenderer))]
namespace RecovibTiny.Droid.CustomRenderers
{
	public class RoundedBorderedGridRenderer : ViewRenderer
	{
		public RoundedBorderedGridRenderer(Context context) : base(context)
		{

		}
		protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.View> e)
		{
			base.OnElementChanged(e);
			try
			{
				var ctrl = e.NewElement as RoundedBorderedGrid;
				GradientDrawable gd = new GradientDrawable();
				gd.SetColor(ctrl.BackgroundColor.ToAndroid());
				gd.SetStroke(ctrl.BorderWidth, ctrl.BorderColor.ToAndroid());
				gd.SetCornerRadius(ctrl.CornerRadius);
				this.Background = gd;
			}
			catch
			{
				//ignore if exception occurs will not set the background

			}
		}
	}
}