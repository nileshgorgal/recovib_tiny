﻿using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Hardware.Usb;
using Android.OS;
using Android.Runtime;
using AndroidX.DocumentFile.Provider;
using RecovibTiny.Droid.Dependency;
using RecovibTiny.Droid.Reciever;
using RecovibTiny.Helper;
using System;
using Xamarin.Forms;

namespace RecovibTiny.Droid
{
	[Activity(Label = "RECOVIB Tiny", Icon = "@mipmap/ic_launcher_tiny", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
	[IntentFilter(new[] { "android.hardware.usb.action.USB_DEVICE_ATTACHED" })]
	[MetaData("android.hardware.usb.action.USB_DEVICE_ATTACHED", Resource = "@drawable/device_filter")]
	public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
	{
		private static String ACTION_USB_PERMISSION = "com.micromega_dynamics.recovibtinyapp.USB_PERMISSION";
		protected override void OnCreate(Bundle savedInstanceState)
		{
			TabLayoutResource = Resource.Layout.Tabbar;
			ToolbarResource = Resource.Layout.Toolbar;

			base.OnCreate(savedInstanceState);
			Rg.Plugins.Popup.Popup.Init(this);
			SciChart.Charting.Visuals.SciChartSurface.SetRuntimeLicenseKey("5bLyRu1upaa2B37mcU7zh1FePQ9YT5pHeQLV14BgmlNPN+9nDdmiLz7rOWqgrK3lLMu2i53ChzywXb6DGNp5rz6ajkkd3LuPvXca/lTfGiOenRBXVblziV3Z2VM+9UGLiMDIdju1si6q+KVe6ZRem4IYmfoo3jv4pWDnEoyHDoJu9Ds0vuC8ejyzQJENQ2kbLFWs5Ghg3yelVPxttTMX+lYnzjWSCyy5A9JHuAI2bTFF+1Y+1+o3ir0YEenkz2wrHd/0+wY8bLhZeNiXuc3e6aMI/qBl4kp8mKpXj2m/HN9ALMkOxctwtM0HSQ2psaZc7n4l3x5Jvc+2WgBOIA0iHAwhZ9ZT3AAjg9y4R+s8hYo/2fL9+WUgrqFzlt83ZIBL/HalDeXo8nz9UHhcL4OuiZtWVA/tL+hIEYcDTQjpkwxBJBbwSBkdlVquiXdHvibqXX5GhDV1WB8s2/g7QV2Nf2JxjneywKpkCIauQtR8eIdBhrjeZvI21XnODFniPtamQLwdfyyI/MttZ7C7UlreFZi707GYh8tR202NbQN6Lrjqo9yoGG1W1Cl08pABv3tJQqSfNvHAsOcNYFr/jjp5QQBjJNVGQu18G/t2hbubjGi1BANCGEt6BmoWMg3YzjSPh4YTSuvsJAxSZ9ICMCey2TFYYk5aBfHUHO1Wg2D1tDK9C6gInlXawfXAyBWufO3GuvvFm5L5");
			Xamarin.Essentials.Platform.Init(this, savedInstanceState);
			global::Xamarin.Forms.Forms.Init(this, savedInstanceState);

			IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
			filter.AddAction(UsbManager.ActionUsbDeviceAttached);
			filter.AddAction(UsbManager.ActionUsbDeviceDetached);
			this.RegisterReceiver(new UsbBroadcastReceiver(), filter);

			LoadApplication(new App());
			//GetPermission();
		}
		public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
		{
			Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

			base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
		}

		protected override void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent data)
		{
			base.OnActivityResult(requestCode, resultCode, data);
			if (resultCode == Result.Ok)
			{
				if (data != null)
				{
					if (requestCode == 9)
					{
						Android.Net.Uri uriTree = data.Data;
						DocumentFile documentFile = DocumentFile.FromTreeUri(this, uriTree);
						int fileCount = 0;
						if (documentFile != null && documentFile.IsDirectory)
						{
							foreach (DocumentFile file in documentFile.ListFiles())
							{
								fileCount++;
								if (file.Name.Equals("MEASURE.BIN"))
								{
									//Toast.MakeText(this, "OTG device storage access." + file.Uri, ToastLength.Long).Show();
									GlobalPreferences.Android11_Path = file.Uri.ToString();
									DependencyService.Get<IDirectoryInfoHelper>(DependencyFetchTarget.GlobalInstance).CopyMeasureBinFile();
									break;
								}
							}
						}
					}
				}
			}
		}
	}
}